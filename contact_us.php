<?php
require_once ('./library/application_top.php');
include_once ('./views/header.php');
?>
<!--=== Breadcrumbs ===-->
<div class="breadcrumbs">
<div class="container">
<h1 class="pull-left">Contact Us</h1>
<ul class="pull-right breadcrumb">
<li><a href="index.html">Home</a></li>
<li><a href="">Pages</a></li>
<li class="active">contact_us</li>
</ul>
</div>
</div><!--/breadcrumbs-->
<!--=== End Breadcrumbs ===-->

<div class="body-container">

<!--=== Content Part ===-->
<div class="container content">
<div class="row">
<div class="col-md-12">
<!-- General Questions -->
<div class = "first-line">
<h1>Need a little more help?</h1>
<p>
We've replaced our email service with an enhanced Live Chat to be quicker and more efficient in our responses.
It provide support from one of our Customer Care specialists right through our website Quick easy, and right on your computer, our Live Chat can handle almost anything
If Live Chat isn't suitable, our Specialists continue to be available over the phone. Click below for contact number and hours of operation.

</p>
</div>

<div class = 'main-area'>
<ul class = 'contact-chat'>
<li class = 'chat-button'>
<a href = '#'>Live Chat</a>
</li>
<li class = 'call-us'>
<a href = '#'>Call Us</a>
</li>
</ul>
</div>

<div class = 'callus-area'>
<div class = 'address-line'>
<div class = 'col-md-6'>
<div class = 'col-md-3'>

</div>
<div class = 'col-md-6'>
<p>In Toronto</p>
88888889999
</div>
<div class = 'col-md-3'></div>
</div>
</div>
</div>
</div><!--/col-md-12-->

</div><!--/row-->
</div><!--/container-->
<!--=== End Content Part ===-->

</div>

<?php include './views/footer.php'; ?>
<!--=== End Footer Version 1 ===-->
</div><!--/wrapper-->

<!--=== Style Switcher ===-->
<i class="style-switcher-btn fa fa-cogs hidden-xs"></i>
<div class="style-switcher animated fadeInRight">
<div class="style-swticher-header">
<div class="style-switcher-heading">Style Switcher</div>
<div class="theme-close"><i class="icon-close"></i></div>
</div>

<div class="style-swticher-body">
<!-- Theme Colors -->
<div class="style-switcher-heading">Theme Colors</div>
<ul class="list-unstyled">
<li class="theme-default theme-active" data-style="default" data-header="light"></li>
<li class="theme-blue" data-style="blue" data-header="light"></li>
<li class="theme-orange" data-style="orange" data-header="light"></li>
<li class="theme-red" data-style="red" data-header="light"></li>
<li class="theme-light" data-style="light" data-header="light"></li>
<li class="theme-purple last" data-style="purple" data-header="light"></li>
<li class="theme-aqua" data-style="aqua" data-header="light"></li>
<li class="theme-brown" data-style="brown" data-header="light"></li>
<li class="theme-dark-blue" data-style="dark-blue" data-header="light"></li>
<li class="theme-light-green" data-style="light-green" data-header="light"></li>
<li class="theme-dark-red" data-style="dark-red" data-header="light"></li>
<li class="theme-teal last" data-style="teal" data-header="light"></li>
</ul>

<!-- Theme Skins -->
<div class="style-switcher-heading">Theme Skins</div>
<div class="row no-col-space margin-bottom-20 skins-section">
<div class="col-xs-6">
<button data-skins="default" class="btn-u btn-u-xs btn-u-dark btn-block active-switcher-btn handle-skins-btn">Light</button>
</div>
<div class="col-xs-6">
<button data-skins="dark" class="btn-u btn-u-xs btn-u-dark btn-block skins-btn">Dark</button>
</div>
</div>

<hr>

<!-- Layout Styles -->
<div class="style-switcher-heading">Layout Styles</div>
<div class="row no-col-space margin-bottom-20">
<div class="col-xs-6">
<a href="javascript:void(0);" class="btn-u btn-u-xs btn-u-dark btn-block active-switcher-btn wide-layout-btn">Wide</a>
</div>
<div class="col-xs-6">
<a href="javascript:void(0);" class="btn-u btn-u-xs btn-u-dark btn-block boxed-layout-btn">Boxed</a>
</div>
</div>

<hr>

<!-- Theme Type -->
<div class="style-switcher-heading">Theme Types and Versions</div>
<div class="row no-col-space margin-bottom-10">
<div class="col-xs-6">
<a href="E-Commerce/index.html" class="btn-u btn-u-xs btn-u-dark btn-block">Shop UI <small class="dp-block">Template</small></a>
</div>
<div class="col-xs-6">
<a href="One-Pages/Classic/index.html" class="btn-u btn-u-xs btn-u-dark btn-block">One Page <small class="dp-block">Template</small></a>
</div>
</div>

<div class="row no-col-space">
<div class="col-xs-6">
<a href="Blog-Magazine/index.html" class="btn-u btn-u-xs btn-u-dark btn-block">Blog <small class="dp-block">Template</small></a>
</div>
<div class="col-xs-6">
<a href="RTL/index.html" class="btn-u btn-u-xs btn-u-dark btn-block">RTL <small class="dp-block">Version</small></a>
</div>
</div>
</div>
</div><!--/style-switcher-->
<!--=== End Style Switcher ===-->

<!-- JS Global Compulsory -->
<script type="text/javascript" src="assets/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript" src="assets/plugins/jquery/jquery-migrate.min.js"></script>
<script type="text/javascript" src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<!-- JS Implementing Plugins -->
<script type="text/javascript" src="assets/plugins/back-to-top.js"></script>
<script type="text/javascript" src="assets/plugins/smoothScroll.js"></script>
<!-- JS Customization -->
<script type="text/javascript" src="assets/js/custom.js"></script>
<!-- JS Page Level -->
<script type="text/javascript" src="assets/js/app.js"></script>
<script type="text/javascript" src="assets/js/plugins/style-switcher.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() {
App.init();
StyleSwitcher.initStyleSwitcher();
});
</script>
<!--[if lt IE 9]>
<script src="assets/plugins/respond.js"></script>
<script src="assets/plugins/html5shiv.js"></script>
<script src="assets/plugins/placeholder-IE-fixes.js"></script>
<![endif]-->
</body>
</html>

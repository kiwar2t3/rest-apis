<?php
require_once ('./library/application_top.php');
include_once ('./views/header.php');
?>
		<!--=== Breadcrumbs ===-->
		<div class="breadcrumbs">
			<div class="container">
				<h1 class="pull-left">Privacy Policy</h1>
				<ul class="pull-right breadcrumb">
					<li><a href="index.html">Home</a></li>
					<li><a href="">Pages</a></li>
					<li class="active">Privacy Policy</li>
				</ul>
			</div>
		</div><!--/breadcrumbs-->
		<!--=== End Breadcrumbs ===-->
		
		
		<div class="body-container">
		<!--=== Content Part ===-->
		<div class="container content">
			<div class="row-fluid privacy">
				<p class = 'effective-date'>Effective Date: July 1, 2016</p>
				<p class = 'lead'>Your Privacy Rights<br></p>
				<p>This Privacy Policy describes your privacy rights regarding our collection, use, storage, sharing and protection of your personal information. It applies to the FoodApp website and all related sites, applications, services and tools regardless of how you access or use them.</p>
				<br><br>
				<div class = 'privacy-explain'>
					<h4>Privacy Rights explained:</h4>
					<div class = 'each-explain'>
						<p class = 'head-text'><i class = "head-icon fa fa-plus"></i>&nbsp;HeaderText1</p>
						<div class = 'body-text' id = 'body-text1'>BodyText1</div>
					</div>
					<div class = 'each-explain'>
						<p class = 'head-text'><i class = "head-icon fa fa-plus"></i>&nbsp;HeaderText2</p>
						<div class = 'body-text' id = 'body-text2'>BodyText2</div>
					</div>
					<div class = 'each-explain'>
						<p class = 'head-text'><i class = "head-icon fa fa-plus"></i>&nbsp;HeaderText3</p>
						<div class = 'body-text'>BodyText3</div>
					</div>
				</div>
				
			</div><!--/row-fluid-->
		</div><!--/container-->
		<!--=== End Content Part ===-->
		</div>
		
		<!--=== Footer Version 1 ===-->
		<?php include './views/footer.php'; ?>		
	</div><!--/wrapper-->


	<!--=== End Style Switcher ===-->

	<!-- JS Global Compulsory -->
	<script type="text/javascript" src="assets/plugins/jquery/jquery.min.js"></script>
	<script type="text/javascript" src="assets/plugins/jquery/jquery-migrate.min.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
	<!-- JS Implementing Plugins -->
	<script type="text/javascript" src="assets/plugins/back-to-top.js"></script>
	<script type="text/javascript" src="assets/plugins/smoothScroll.js"></script>
	<!-- JS Customization -->
	<script type="text/javascript" src="assets/js/custom.js"></script>
	<!-- JS Page Level -->
	<script type="text/javascript" src="assets/js/app.js"></script>
	<script type="text/javascript" src="assets/js/plugins/style-switcher.js"></script>
	<script type="text/javascript">
		jQuery(document).ready(function() {
			App.init();
			StyleSwitcher.initStyleSwitcher();
			var i = 1;
			$('.head-icon').each(function () {
				$(this).click(function () {
			 		$(this).toggleClass('fa-plus fa-minus');
			 		$(this).parent().next('.body-text').slideToggle(100);
				});
				i ++;
			});
		});
	</script>
</body>
</html>

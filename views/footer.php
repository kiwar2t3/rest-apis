<?php
	
?>
<footer class = "global-footer" role = "contentinfo">
	<div class = "container">
		<ul class = 'footer-main secondaryLink'>
			<li>
				<a href = '#' class = "footer-help">HELP & CONTACT</a>
			</li>
			<li>
				<a href = '#' class = "footer-security">SECURITY</a>
			</li>
		</ul>
		<hr>
		<ul class = 'footer-secondary copyright-section sencondaryLink'>
			<li id = 'footer-copyright' class = 'footer-copyright'>
				&copy;1999-2016 <b>FoodApp</b> All rights reserved &nbsp;&nbsp;&nbsp;&nbsp;|
			</li>
			<li id = 'footer-privacy' class = 'footer-privacy'>
				<a href = '#'>Privacy</a>
			</li>
			<li id = 'footer-legal' class = 'footer-legal'>
				<a href = '#'>Legal</a>
			</li>
			<li id = 'sitefeedback' class = 'sitefeedback'>
				<a href = '#'>Feedback</a>
			</li>
		</ul>
	</div>
</footer>
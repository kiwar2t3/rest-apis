<?php
include 'application_top.php';
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<head>
<title>FoodApp Website</title>

<!-- Meta -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">

<!-- Favicon -->
<link rel="shortcut icon" href="favicon.ico">

<!-- Web Fonts -->
<!-- <link rel='stylesheet' type='text/css' href='//fonts.googleapis.com/css?family=Open+Sans:400,300,600&amp;subset=cyrillic,latin'> -->

<!-- CSS Global Compulsory -->
<link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="assets/css/style.css">

<!-- CSS Header and Footer -->
<link rel="stylesheet" href="assets/css/headers/header-default.css">
<link rel="stylesheet" href="assets/css/footers/footer-new.css">
<!-- <link rel="stylesheet" href="assets/css/footers/footer-v1.css"> -->

<!-- CSS Body -->
<link rel="stylesheet" href="assets/css/pages/page_privacy.css">

<!-- CSS Implementing Plugins -->
<link rel="stylesheet" href="assets/plugins/animate.css">
<link rel="stylesheet" href="assets/plugins/line-icons/line-icons.css">
<link rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="assets/plugins/brand-buttons/brand-buttons-inversed.css">

<!-- CSS Theme -->
<link rel="stylesheet" href="assets/css/theme-colors/default.css" id="style_color">
<link rel="stylesheet" href="assets/css/theme-skins/dark.css">
<link rel="stylesheet" href="assets/css/pages/page_log_reg_v3.css">

<!-- CSS Customization -->
<link rel="stylesheet" href="assets/css/custom.css">
</head>
<body>
<div class="wrapper">
<!--=== Header ===-->
<div class="header">
<div class="container">
<!-- Logo -->
<a class="logo" href="index.php">
<img src="assets/img/logo1-white.png" alt="Logo">
<!-- <img src="assets/img/images.jpg" alt="Logo"> -->
</a>
<!-- End Logo -->

<!-- Topbar -->
<div class="topbar">
<ul class="loginbar pull-right">
<li class="hoverSelector">
<i class="fa fa-globe"></i>
<a>Languages</a>
<ul class="languages hoverSelectorBlock">
<li class="active">
<a href="#">English <i class="fa fa-check"></i></a>
</li>
<li><a href="#">Spanish</a></li>
<li><a href="#">Russian</a></li>
<li><a href="#">German</a></li>
</ul>
</li>
<li class="topbar-devider"></li>
<li><a href="page_faq.html">Help</a></li>
<li class="topbar-devider"></li>
<li><a href="login.php">Login</a></li>
</ul>
</div>
<!-- End Topbar -->

<!-- Toggle get grouped for better mobile display -->
<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
<span class="sr-only">Toggle navigation</span>
<span class="fa fa-bars"></span>
</button>
<!-- End Toggle -->
</div><!--/end container-->

<!-- Collect the nav links, forms, and other content for toggling -->
<div class="collapse navbar-collapse mega-menu navbar-responsive-collapse">
<div class="container">
<ul class="nav navbar-nav">
<!-- Home -->
<li class="">
<a href="index.php" class="dropdown-toggle">
Dashboard
</a>
</li>
<li class="">
<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
Recipe
</a>
</li>
<li class="">
<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
List(s)
</a>
</li>
<li class="">
<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
Calendar
</a>
</li>
<li class="">
<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
Friends
</a>
</li>
</ul>
<div class = "pull-right">
<div class = "header-buttons">
<a href = '#' class="btn btn-small btn-secondary">Log Out</a>
<!-- <a href = 'signup.php' class = "btn btn-small btn-signup">Sign Up</a> -->
</div>
</div>
</div><!--/end container-->
</div><!--/navbar-collapse-->
</div>
<!--=== End Header ===-->
<?php
require_once './library/application_top.php';

$restApi = new FoodCopia_RestApi("login");
if (isset($_POST['email']) && $_POST['email']) {
	$restApi -> call_api(array(
		"user_email" => tep_get_value_post("email"),
		"user_password" => tep_get_value_post("password")
	));
	if ($restApi -> has_successed()) {
		${SESSION_USER_ID} = $restApi -> result -> user_id;
		${SESSION_AUTH_TOKEN} = $restApi -> result -> user_authtoken;

		tep_session_register(SESSION_USER_ID);
		tep_session_register(SESSION_AUTH_TOKEN);

		tep_redirect("index.php");
	}
}
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
	<!--<![endif]-->
	<head>
		<title>Login 2 | Unify - Responsive Website Template</title>

		<!-- Meta -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="">
		<meta name="author" content="">

		<!-- Favicon -->
		<link rel="shortcut icon" href="favicon.ico">

		<!-- Web Fonts -->
		<!-- <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans:400,300,700&amp;subset=cyrillic,latin"> -->

		<!-- CSS Global Compulsory -->
		<link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="assets/css/style.css">
		<link rel="stylesheet" href="assets/css/footers/footer-new.css">
		<!-- CSS Implementing Plugins -->
		<link rel="stylesheet" href="assets/plugins/animate.css">
		<link rel="stylesheet" href="assets/plugins/line-icons/line-icons.css">
		<link rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="assets/plugins/brand-buttons/brand-buttons.css">

		<!-- CSS Page Style -->
		<link rel="stylesheet" href="assets/css/pages/page_log_reg_v4.css">

		<!-- CSS Theme -->
		<link rel="stylesheet" href="assets/css/theme-colors/blue.css">

		<!-- CSS Customization -->
		<link rel="stylesheet" href="assets/css/custom.css">

	</head>

	<body>
		<div class="body-container">
			<!--=== Content Part ===-->
			<div class="container content">
				<div class = 'col-md-2 col-sm-2'></div>
				<div class="col-md-6 col-sm-6 form-block equal-height-column">
					<div class="login-block">
						<a href="<?php echo get_page_link("index.php"); ?>"> <img src="assets/img/themes/logo1-blue.png" alt=""> </a>

						<?php
						if ($restApi -> has_failed()) {
							show_error_messages($restApi -> result);
						}
						?>

						<form id = 'login-form' method="post">
							<div class = 'form-group margin-bottom-20'>
								<div class="input-group">
									<span class="input-group-addon rounded-left"><i class="icon-user color-blue"></i></span>
									<input type="text" name = 'email' id = 'email' class="form-control rounded-right" placeholder="Email" value="<?php echo tep_get_value_post("email"); ?>">
								</div>
							</div>
							<div class = 'form-group margin-bottom-30'>
								<div class="input-group">
									<span class="input-group-addon rounded-left"><i class="icon-lock color-blue"></i></span>
									<input type="password" name = 'password' id = 'password' class="form-control rounded-right" placeholder="Password" value="">
								</div>
							</div>

							<div class = 'form-group'>
								<div class="row margin-bottom-10">
									<div class="col-md-12">
										<button type="submit" id = 'login-button' class="btn-u btn-u-blue btn-block rounded">
											Log In
										</button>
									</div>
								</div>
							</div>
							<div class = 'forgot margin-bottom-70'>
								<div class = 'col-md-3'></div>
								<a href = '#'>Having trouble logging in?</a>
							</div>

							<div class="social-login text-center">
								<div class="or rounded-x">
									Or
								</div>
								<ul class="list-inline margin-bottom-20">
									<li>
										<button class="btn rounded btn-lg btn-facebook">
											<i class="fa fa-facebook"></i> Facebook
										</button>
									</li>
									<li>
										<button class="btn rounded btn-lg btn-twitter">
											<i class="fa fa-google-plus"></i> Google
										</button>
									</li>
								</ul>
								<p>
									Don't have an account? <a href="signup.php">Create New</a>
								</p>
							</div>

						</form>
					</div>
					<!-- </div> -->
				</div>
			</div><!--/container-->
		</div>
		<!--=== End Content Part ===-->

		<?php
		include './views/footer.php';
		?>

		<!-- JS Global Compulsory -->
		<script src="assets/plugins/jquery/jquery.min.js"></script>
		<script src="assets/plugins/jquery/jquery-migrate.min.js"></script>
		<script src="assets/plugins/jquery/jquery_validate.js"></script>
		<script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>

		<!-- JS Implementing Plugins -->
		<script src="assets/plugins/back-to-top.js"></script>
		<script src="assets/plugins/backstretch/jquery.backstretch.min.js"></script>

		<!-- JS Customization -->
		<script src="assets/js/custom.js"></script>

		<!-- JS Page Level -->
		<script src="assets/js/app.js"></script>

		<!-- Custom JS -->
		<script src="assets/js/pages/login.js?v=1"></script>

		<script>
			jQuery(document).ready(function() {
				App.init();
			});
		</script>
		<script>
$(".image-block").backstretch([
"assets/img/bg/img11.jpg",
"assets/img/bg/img5.jpg",
], {
fade: 1000,
duration: 7000
});
		</script>
		<!--[if lt IE 9]>
		<script src="assets/plugins/respond.js"></script>
		<script src="assets/plugins/html5shiv.js"></script>
		<script src="assets/plugins/placeholder-IE-fixes.js"></script>
		<![endif]-->
	</body>
</html>
<?php
require_once ('./library/application_top.php');
include_once ('./views/header.php');
?>

<!--=== Breadcrumbs ===-->
<div class="breadcrumbs">
	<div class="container">
		<?php if (tep_session_is_registered(SESSION_USER_ID)) :
		?>
		<div class = 'col-md-6 pull-left'>
			<div class = 'col-md-2'>
				<a href = 'profile.php'>
					<img src = <?php 
									echo $logined_user_info -> user_avatar; 
								?> 
					/>
				</a>
			</div>
			<div class = 'col-md-10'>
				<div>Hi Again, <?php 
									if ( !is_null($logined_user_info->user_name) ) 
										echo explode("@",$logined_user_info->user_email)[0]; 
									else echo "Friend"; 
								?>
			    </div>
				<div class = 'sub-letter'>Welcom to your FOOD APP</div>
			</div>
		</div>
		<div class = 'pull-right btn add-list-btn'>
			<a href='#'>Add New List <i class = "fa fa-arrow-circle-up"></i></a>
		</div>
		<?php else: ?>

		<?php endif; ?>
	</div>
</div>
<!--/breadcrumbs-->
<div class = 'body-container'>
	<div class = 'container'>
	<?php if (tep_session_is_registered(SESSION_USER_ID)) : ?>
		<div class = 'col-md-4 left-area'>
			<div class = 'sections stats-section'>
				<div class = 'left-lines'>
					Stats
					<i class = 'pull-right fa fa-chevron-right'></i>
				</div>
				<div class = 'item-money'>
					<div class = 'item-phurchased'>
						<div>ITEMS PHURCHASED.</div>
						<div>
							<span>ZERO.</span>
						</div>
					</div>
					<div class = 'money-spent'>
						<div>MONEY SPENT.</div>
						<div>
							<span>$0.00</span>
						</div>
					</div>
				</div>
			</div>
			
			<div class = 'sections card-coupons-section'>
				<div class = 'left-lines'>
					Cards & Coupons
					<i class = 'pull-right fa fa-chevron-right'></i>
				</div>
				<div class = 'card-coupon'>
					<div>Wal-Mart For Mc</div>
					<div>Neutrogena.</div>
				</div>
			</div>
			
			<div class = 'sections manage-sharing-section'>
				<div class = 'left-lines'>
					Manage Sharing
					<i class = 'pull-right fa fa-chevron-right'></i>
				</div>
				<div class = 'manage-sharing'>
					<div>Shared With Me.</div>
					<div>My Sharing.</div>
				</div>
			</div>
		</div>
		<div class = 'col-md-8 right-area'>
			<div class = 'list-section'>
				List(s)
				<i class = 'pull-right fa fa-chevron-right'></i>
			</div>
			<div class = 'sort-section'>
				<div class = 'col-md-9'>
					<u>SORT</u>
					<i class = 'fa fa-angle-down'></i>
				</div>
				<div class = 'col-md-3'>
					<u>ITEM COUNT</u>
				</div>
			</div>
			<div class = 'lists-section'>
				<?php
					$restApi = new FoodCopia_RestApi("mylists");
					$restApi -> call_api(array(
						'user_id' => $logined_user_info -> user_id,
						'position' => '0'
					));
					$lists = $restApi -> result;
					foreach ( $lists as $list ) {
				?>
				<div class = 'col-md-12 list-row'>
					<!--  <input type = 'hidden' id = 'user_authtoken' value = <?php echo $list->user_authtoken; ?>/> ---->
					<input type = 'hidden' id = 'list_id' value = <?php echo $list->id; ?>/>
					<div class = 'col-md-9 left-section'>
						<div class = 'col-md-2'>
							<div class = 'first-line'>
								<a href = 'profile.php'><img src = <?php echo $list -> user_avatar; ?>></a>
							</div>
							<div class = 'second-line'>
								<?php 
									$time = strtotime($list -> created_date);
									$newformat = date('M d',$time);
									echo $newformat;
								?>
							</div>
						</div>
						<div class = 'col-md-10'>
							<div class = 'first-line'>
								<?php echo $list -> list_name ?>
							</div>
							<div class = 'second-line'>
							
							</div>
						</div>
					</div>
					<div class = 'col-md-3 right-section'>
						<div class = 'item-count'><?php echo $list -> items_count; ?></div>
					</div>
				</div>
				<div class = 'col-md-12 icon-area'>
					<div class = 'button-line'>
						<button type="button" class="close icon-area-close" data-dismiss="icon-area" aria-hidden="true">×</button>
					</div>
					<div class = 'icon-line'>
						<div class = 'col-md-4'>
							<a href = '#'><i class = 'fa fa-heart-o'></i>Favorite</a>
						</div>
						<div class = 'col-md-4'>
							<a href = '#'><i class = 'fa fa-comment'></i>Comment</a>
						</div>
						<div class = 'col-md-4'>
							<a href = '#'><i class = 'fa fa-trash'></i>Delete</a>
						</div>
					</div>
					<div class = 'icon-line'>
						<div class = 'col-md-4'>
							<a href = '#'><i class = 'fa fa-check-square-o'></i>Mark Phurchased</a>
						</div>
						<div class = 'col-md-4'>
							<a href = '#'><i class = 'fa fa-files-o'></i>Make a Copy</a>
						</div>
						<div class = 'col-md-4'>
							<a href = '#'><i class = 'fa fa-shopping-cart'></i>Cart Item(s)</a>
						</div>
					</div>
					<div class = 'icon-line'>
						<div class = 'col-md-4'>
							<a href = '#'><i class = 'fa fa-info-circle'></i>Details</a>
						</div>
						<div class = 'col-md-4'>
							<a href = '#'><i class = 'fa fa-share-alt'></i>Share</a>
						</div>
						<div class = 'col-md-4'>
							<a href = '#'><i class = 'fa fa-print'></i>Print List</a>
						</div>
					</div>
				</div>
				<?php } ?>
			</div>
			<div class = 'col-md-12 view-more'>
				View More
			</div>
			<div class = 'col-md-12' id = 'loading-lists' style = 'display:none'>
				<img class = 'img-responsive center-block' src='assets/img/loading.gif' width="30" height="30" />
			</div>
		</div>
	<?php endif; ?>
	</div>
</div>
<?php
	include './views/footer.php';
?>
<!--=== End Team v4 ===-->

<!-- JS Global Compulsory -->
<script type="text/javascript" src="assets/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript" src="assets/plugins/jquery/jquery-migrate.min.js"></script>
<script type="text/javascript" src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<!-- JS Implementing Plugins -->
<script type="text/javascript" src="assets/plugins/back-to-top.js"></script>
<script type="text/javascript" src="assets/plugins/smoothScroll.js"></script>
<script type="text/javascript" src="assets/plugins/jquery-appear.js"></script>
<script type="text/javascript" src="assets/plugins/fancybox/source/jquery.fancybox.pack.js"></script>
<script type="text/javascript" src="assets/plugins/owl-carousel/owl-carousel/owl.carousel.js"></script>
<!-- JS Customization -->
<script type="text/javascript" src="assets/js/custom.js"></script>
<!-- JS Page Level -->
<script type="text/javascript" src="assets/js/app.js"></script>
<script type="text/javascript" src="assets/js/pages/business_home.js"></script>
<script type="text/javascript" src="assets/js/plugins/fancy-box.js"></script>
<script type="text/javascript" src="assets/js/plugins/progress-bar.js"></script>
<script type="text/javascript" src="assets/js/plugins/owl-carousel.js"></script>
<script type="text/javascript" src="assets/js/plugins/style-switcher.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function() {
		App.init();
		FancyBox.initFancybox();
		OwlCarousel.initOwlCarousel();
		StyleSwitcher.initStyleSwitcher();
		ProgressBar.initProgressBarHorizontal();
	}); 
</script>
<!--[if lt IE 9]>
<script src="assets/plugins/respond.js"></script>
<script src="assets/plugins/html5shiv.js"></script>
<script src="assets/plugins/placeholder-IE-fixes.js"></script>
<![endif]-->
</body>
</html>

<?php
require_once './library/application_top.php';

$restApi = new FoodCopia_RestApi("registration");
if (isset($_POST['user_email'])) {
	if (isset($_FILES['user_avatar']) && isset($_FILES['user_avatar']['tmp_name']) && $_FILES['user_avatar']['tmp_name']) {
		$restApi -> call_api($_POST, array("user_avatar" => $_FILES['user_avatar']));
	} else {
		$restApi -> call_api($_POST);
	}
	if ($restApi -> has_successed()) {
		${SESSION_USER_ID} = $restApi -> result -> user_id;
		${SESSION_AUTH_TOKEN} = $restApi -> result -> user_authtoken;

		tep_session_register(SESSION_USER_ID);
		tep_session_register(SESSION_AUTH_TOKEN);

		tep_redirect("index.php");
	}
}
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
	<!--<![endif]-->
	<head>
		<title>Regsitration | <?php echo SITE_TITLE; ?></title>

		<!-- Meta -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="">
		<meta name="author" content="">

		<!-- Favicon -->
		<link rel="shortcut icon" href="favicon.ico">

		<!-- Web Fonts -->
		<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans:400,300,700&amp;subset=cyrillic,latin">

		<!-- CSS Global Compulsory -->
		<link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="assets/css/style.css">
		<link rel="stylesheet" href="assets/css/footers/footer-new.css">

		<!-- CSS Implementing Plugins -->
		<link rel="stylesheet" href="assets/plugins/animate.css">
		<link rel="stylesheet" href="assets/plugins/line-icons/line-icons.css">
		<link rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome.min.css">

		<!-- CSS Page Style -->
		<link rel="stylesheet" href="assets/css/pages/page_log_reg_v4.css">

		<!-- CSS Customization -->
		<link rel="stylesheet" href="assets/css/custom.css?v=1.1">
	</head>

	<body>
		<!--=== Content Part ===-->
		<div class="container content">
			<div class="row equal-height-columns">
				<div class = 'col-md-2 col-sm-2'></div>
				<div class="col-md-6 col-sm-6 form-block equal-height-column">
					<div class="reg-block">
						<a href="<?php echo get_page_link("index.php"); ?>"> <img src="assets/img/themes/logo1-blue.png" alt=""> </a>
						<h4 class="margin-bottom-30">Sign up for FoodApp, It's free</h4>
						
						<?php
						if ($restApi -> has_failed()) {
							show_error_messages($restApi -> result);
						}
						?>
						
						<form id = 'signup-form' method="post" enctype="multipart/form-data">
							<!--div class="margin-bottom-20">
								<div class="input-group">
									<span class="input-group-addon rounded-left"><i class="icon-user color-green"></i></span>
									<input type="text" class="form-control rounded-right" name = 'user_name' id = 'user_name' placeholder="Username">
								</div>
							</div-->

							<div class="margin-bottom-20">
								<div class="input-group">
									<span class="input-group-addon rounded-left"><i class="icon-envelope color-green"></i></span>
									<input type="email" class="form-control rounded-right" name = 'user_email' id = 'user_email' placeholder="Your email" value="<?php echo tep_get_value_post("user_email"); ?>">
								</div>
							</div>

							<div class="margin-bottom-20">
								<div class="input-group">
									<span class="input-group-addon rounded-left"><i class="icon-lock color-green"></i></span>
									<input type="password" class="form-control rounded-right" name = 'user_password' id = 'user_password' placeholder="Create your Password" value="<?php echo tep_get_value_post("user_password"); ?>">
								</div>
							</div>

							<div class="margin-bottom-20">
								<div class="input-group">
									<span class="input-group-addon rounded-left"><i class="icon-lock color-green"></i></span>
									<input type="password" class="form-control rounded-right" name = 'confirm_password' id = 'confirm_password' placeholder="Confirm your password" value="<?php echo tep_get_value_post("confirm_password"); ?>">
								</div>
							</div>

							<div class="margin-bottom-20">
								<div class="input-group">
									<span class="input-group-addon rounded-left"></span>
									<input type="text" class="form-control rounded-right" name='user_postal' id = 'user_postal' placeholder="Postal Code" value="<?php echo tep_get_value_post("user_postal"); ?>">
								</div>
							</div>

							<div class="margin-bottom-20">
								<div class="input-group">
									<span class="input-group-addon rounded-left"></span>
									<input type="text" class="form-control rounded-right" id='user_address'name="user_address" placeholder="Address" value="<?php echo tep_get_value_post("user_address"); ?>">
								</div>
							</div>

							<div class="margin-bottom-20">
								<div class="input-group">
									<span class="input-group-addon rounded-left"></span>
									<input type="text" class="form-control rounded-right" id = 'user_phone' name = 'user_phone' placeholder="Phone" value="<?php echo tep_get_value_post("user_phone"); ?>">
								</div>
							</div>
							
							<div class = 'margin-bottom-20'>
								<div class="input-group file-input-group" data-file-types='jpg|png|jpeg'>
									<span class="input-group-addon rounded-left"><i class="icon-cloud-upload color-green"></i></span>
									<input type="text" class="form-control rounded-right" placeholder="Choose photo" />
									<input type="file" name = 'user_avatar' style="display: none;" id='user_avatar' />
								</div>
							</div>
							<button type="submit" class="btn-u btn-block rounded">
								Continue
							</button>
						</form>
					</div>
				</div>
			</div>
		</div><!--/container-->
		<!--=== End Content Part ===-->

		<?php include './views/footer.php' ?>
		<!-- JS Global Compulsory -->
		<script src="assets/plugins/jquery/jquery.min.js"></script>
		<script src="assets/plugins/jquery/jquery-migrate.min.js"></script>
		<script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>

		<!-- JS Implementing Plugins -->
		<script src="assets/plugins/back-to-top.js"></script>
		<script src="assets/plugins/backstretch/jquery.backstretch.min.js"></script>
		<script src="assets/plugins/jquery/jquery_validate.js"></script>

		<!-- JS Customization -->
		<script src="assets/js/custom.js"></script>

		<!-- JS Page Level -->
		<script src="assets/js/app.js"></script>
		<script src="assets/js/pages/signup.js?v=1.1"></script>
		<script>
			jQuery(document).ready(function() {
				App.init();
			});
		</script>
		<script>
$(".image-block").backstretch([
"assets/img/bg/img6.jpg",
"assets/img/bg/img5.jpg",
], {
fade: 1000,
duration: 7000
});
		</script>
		<!--[if lt IE 9]>
		<script src="assets/plugins/respond.js"></script>
		<script src="assets/plugins/html5shiv.js"></script>
		<script src="assets/plugins/placeholder-IE-fixes.js"></script>
		<![endif]-->
	</body>
</html>

<?php
require_once ('./library/application_top.php');
include_once ('./views/header.php');
?>
<!--=== Breadcrumbs ===-->
<div class="breadcrumbs">
<div class="container">
<h1 class="pull-left">FAQs</h1>
<ul class="pull-right breadcrumb">
<li><a href="index.html">Home</a></li>
<li><a href="">Pages</a></li>
<li class="active">FAQ</li>
</ul>
</div>
</div><!--/breadcrumbs-->
<!--=== End Breadcrumbs ===-->

<div class="body-container">

<!--=== Content Part ===-->
<div class="container content">
<div class="row">
<div class="col-md-12">
<!-- General Questions -->
<div class = "search-line">
<div class = 'col-md-2'></div>
<div class = 'col-md-8'>
<div class = 'search-area'>
<input type = 'text' class = 'search-input'>
<input type = 'button' class = 'search-box' value = 'Search'>
</div>
</div>
<div class = 'col-md-2'></div>
</div>
<div class="headline"><h2>HELP TOPICS</h2></div>
<div class="panel-group acc-v1 margin-bottom-40" id="accordion">
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title">
<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
1. Put a bird on it squid single-origin coffee nulla?
</a>
</h4>
</div>
<div id="collapseOne" class="panel-collapse collapse in">
<div class="panel-body">
Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et.
</div>
</div>
</div>

<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title">
<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
2. Oliva pariatur cliche reprehenderit high life accusamus?
</a>
</h4>
</div>
<div id="collapseTwo" class="panel-collapse collapse">
<div class="panel-body">
<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.</p>
<ul class="list-unstyled">
<li><i class="fa fa-check color-green"></i> Donec id elit non mi porta gravida at eget metus..</li>
<li><i class="fa fa-check color-green"></i> Fusce dapibus, tellus ac cursus comodo egetine..</li>
<li><i class="fa fa-check color-green"></i> Food truck quinoa nesciunt laborum eiusmod runch..</li>
<li><i class="fa fa-check color-green"></i> Donec id elit non mi porta gravida at eget metus..</li>
</ul>
</div>
</div>
</div>

<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title">
<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
3. Enim eiusmod high life accusamus terry richardson?
</a>
</h4>
</div>
<div id="collapseThree" class="panel-collapse collapse">
<div class="panel-body">
<p>Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Food truck quinoa nesciunt laborum eiusmodolf moon tempor, sunt aliqua put a bird.</p>
<ul class="list-unstyled">
<li><i class="fa fa-check color-green"></i> Donec id elit non mi porta gravida at eget metus..</li>
<li><i class="fa fa-check color-green"></i> Fusce dapibus, tellus ac cursus comodo egetine..</li>
</ul>
</div>
</div>
</div>

<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title">
<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
4. Livil anim keffiyeh helvetica craft beer labore wesde brunch?
</a>
</h4>
</div>
<div id="collapseFour" class="panel-collapse collapse">
<div class="panel-body">
Olif moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et.
</div>
</div>
</div>

<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title">
<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
5. Leggings occaecat craft beer farmto tableraw denim?
</a>
</h4>
</div>
<div id="collapseFive" class="panel-collapse collapse">
<div class="panel-body">
<p>Keffiyeh anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et.</p>
<ul class="list-unstyled">
<li><i class="fa fa-check color-green"></i> Donec id elit non mi porta gravida at eget metus..</li>
<li><i class="fa fa-check color-green"></i> Fusce dapibus, tellus ac cursus comodo egetine..</li>
<li><i class="fa fa-check color-green"></i> Food truck quinoa nesciunt laborum eiusmod runch..</li>
<li><i class="fa fa-check color-green"></i> Donec id elit non mi porta gravida at eget metus..</li>
<li><i class="fa fa-check color-green"></i> Fusce dapibus, tellus ac cursus comodo egetine..</li>
<li><i class="fa fa-check color-green"></i> Food truck quinoa nesciunt laborum eiusmod runch..</li>
</ul>
</div>
</div>
</div>

<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title">
<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseSix">
6. Keffiyeh anim keffiyeh helvetica craft beer labore wesse?
</a>
</h4>
</div>
<div id="collapseSix" class="panel-collapse collapse">
<div class="panel-body">
Helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Brunch sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et.
</div>
</div>
</div>

<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title">
<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven">
7. Helvetica craft beer labore wes anderson cred nesciu ntlife richardson?
</a>
</h4>
</div>
<div id="collapseSeven" class="panel-collapse collapse">
<div class="panel-body">
Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.
</div>
</div>
</div>
</div><!--/acc-v1-->
<!-- End General Questions -->
</div><!--/col-md-12-->
</div><!--/row-->
</div><!--/container-->
<!--=== End Content Part ===-->

</div>

<?php include './views/footer.php'; ?>
<!--=== End Footer Version 1 ===-->
</div><!--/wrapper-->

<!--=== Style Switcher ===-->
<i class="style-switcher-btn fa fa-cogs hidden-xs"></i>
<div class="style-switcher animated fadeInRight">
<div class="style-swticher-header">
<div class="style-switcher-heading">Style Switcher</div>
<div class="theme-close"><i class="icon-close"></i></div>
</div>

<div class="style-swticher-body">
<!-- Theme Colors -->
<div class="style-switcher-heading">Theme Colors</div>
<ul class="list-unstyled">
<li class="theme-default theme-active" data-style="default" data-header="light"></li>
<li class="theme-blue" data-style="blue" data-header="light"></li>
<li class="theme-orange" data-style="orange" data-header="light"></li>
<li class="theme-red" data-style="red" data-header="light"></li>
<li class="theme-light" data-style="light" data-header="light"></li>
<li class="theme-purple last" data-style="purple" data-header="light"></li>
<li class="theme-aqua" data-style="aqua" data-header="light"></li>
<li class="theme-brown" data-style="brown" data-header="light"></li>
<li class="theme-dark-blue" data-style="dark-blue" data-header="light"></li>
<li class="theme-light-green" data-style="light-green" data-header="light"></li>
<li class="theme-dark-red" data-style="dark-red" data-header="light"></li>
<li class="theme-teal last" data-style="teal" data-header="light"></li>
</ul>

<!-- Theme Skins -->
<div class="style-switcher-heading">Theme Skins</div>
<div class="row no-col-space margin-bottom-20 skins-section">
<div class="col-xs-6">
<button data-skins="default" class="btn-u btn-u-xs btn-u-dark btn-block active-switcher-btn handle-skins-btn">Light</button>
</div>
<div class="col-xs-6">
<button data-skins="dark" class="btn-u btn-u-xs btn-u-dark btn-block skins-btn">Dark</button>
</div>
</div>

<hr>

<!-- Layout Styles -->
<div class="style-switcher-heading">Layout Styles</div>
<div class="row no-col-space margin-bottom-20">
<div class="col-xs-6">
<a href="javascript:void(0);" class="btn-u btn-u-xs btn-u-dark btn-block active-switcher-btn wide-layout-btn">Wide</a>
</div>
<div class="col-xs-6">
<a href="javascript:void(0);" class="btn-u btn-u-xs btn-u-dark btn-block boxed-layout-btn">Boxed</a>
</div>
</div>

<hr>

<!-- Theme Type -->
<div class="style-switcher-heading">Theme Types and Versions</div>
<div class="row no-col-space margin-bottom-10">
<div class="col-xs-6">
<a href="E-Commerce/index.html" class="btn-u btn-u-xs btn-u-dark btn-block">Shop UI <small class="dp-block">Template</small></a>
</div>
<div class="col-xs-6">
<a href="One-Pages/Classic/index.html" class="btn-u btn-u-xs btn-u-dark btn-block">One Page <small class="dp-block">Template</small></a>
</div>
</div>

<div class="row no-col-space">
<div class="col-xs-6">
<a href="Blog-Magazine/index.html" class="btn-u btn-u-xs btn-u-dark btn-block">Blog <small class="dp-block">Template</small></a>
</div>
<div class="col-xs-6">
<a href="RTL/index.html" class="btn-u btn-u-xs btn-u-dark btn-block">RTL <small class="dp-block">Version</small></a>
</div>
</div>
</div>
</div><!--/style-switcher-->
<!--=== End Style Switcher ===-->

<!-- JS Global Compulsory -->
<script type="text/javascript" src="assets/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript" src="assets/plugins/jquery/jquery-migrate.min.js"></script>
<script type="text/javascript" src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<!-- JS Implementing Plugins -->
<script type="text/javascript" src="assets/plugins/back-to-top.js"></script>
<script type="text/javascript" src="assets/plugins/smoothScroll.js"></script>
<!-- JS Customization -->
<script type="text/javascript" src="assets/js/custom.js"></script>
<!-- JS Page Level -->
<script type="text/javascript" src="assets/js/app.js"></script>
<script type="text/javascript" src="assets/js/plugins/style-switcher.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() {
App.init();
StyleSwitcher.initStyleSwitcher();
});
</script>
<!--[if lt IE 9]>
<script src="assets/plugins/respond.js"></script>
<script src="assets/plugins/html5shiv.js"></script>
<script src="assets/plugins/placeholder-IE-fixes.js"></script>
<![endif]-->
</body>
</html>

/* Write here your custom javascript codes */
$(function() {
	$("form .file-input-group").each(function() {
		$parent = $(this).parent();
		$icon = $(this).find("span.input-group-addon");
		$file_name = $(this).find("input[type=text]");
		$file_input = $(this).find("input[type=file]");
		
		$file_name.attr("readonly", "readonly");
		
		$allow_filetypes = [];
		if ($(this).attr("data-file-types")) {
			$allow_filetypes = $(this).attr("data-file-types").split('|');
		}
		
		$file_name.click(function() {
			$file_input.trigger("click");	
		});
		
		$file_input.change(function() {
			var filepath = $(this).val();
			
			if (filepath) {
				
			} else {
				$file_name.val("");
				return;
			}
			
			var fileName = "";
			if (filepath.lastIndexOf("/") > 0) {
				fileName = filepath.split('/').pop();
			} else {
				fileName = filepath.split('\\').pop();
			}
			
			if ($allow_filetypes.length == 0) {
				$file_name.val(fileName);
				return;
			}
			
			$temp = fileName.split(".");
			if ($temp.length < 2) {
				$file_name.val("");
				return;
			}
			$file_ext = $temp[$temp.length - 1].toLowerCase();
			
			if ($.inArray($file_ext, $allow_filetypes) < 0) {
				$file_input.val('');
				$file_name.val("");
				$parent.append('<label class="error">Valid file types are ' + $allow_filetypes.join(",") + '</label>');
				return;
			} else {
				$parent.find('label.error').remove();
				$file_name.val(fileName);
				return;
			}
		});
	});
})

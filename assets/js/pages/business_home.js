//Masonry js functions
$(document).ready(function() {
	$('.view-more').click(function () {
		var position = $('.list-row').length;
		var param = {
			action: "viewlist",
			position: position
		}
		$('.view-more').hide();
		$('#loading-lists').show();
		$.post('http://138.68.96.163/RestApi/mobile_service.php', param, function(returnData) {
			returnData = JSON.parse(returnData);
			if ( returnData.status == 'success' ) {
				var lists = returnData.result;
				var html = "";
				$('#loading-lists').hide();
				if ( lists.length == 10 ) {
					$('.view-more').show();
				}
				var month = new Array();
				month[0] = "Jan";
				month[1] = "Feb";
				month[2] = "Mar";
				month[3] = "Apr";
				month[4] = "May";
				month[5] = "Jun";
				month[6] = "Jul";
				month[7] = "Augt";
				month[8] = "Sep";
				month[9] = "Oct";
				month[10] = "Nov";
				month[11] = "Dec";
				for ( var i = 0; i < lists.length; i ++ ) {
					html += "<div class = 'col-md-12 list-row'>";
					html +=	"<input type = 'hidden' id = 'user_authtoken' value = '" + lists[i].user_authtoken + "'/>";
					html += "<input type = 'hidden' id = 'list_id' value = '" + lists[i].id + "'/>";
					html +=	"<div class = 'col-md-9 left-section'>";
					html += "<div class = 'col-md-2'>";
					html +=  "<div class = 'first-line'>";
					html += "<a href = '#'><img src = '" + lists[i].user_avatar + "'></a></div>";
					html += "<div class = 'second-line'>";
					
					var date = new Date(lists[i].created_date);
					var mon = month[date.getMonth()];
					var day = date.getDate();
					var created_date = mon + ' ' + day;
					html += created_date + "</div></div>";
					
					html += "<div class = 'col-md-10'>";
					html += "<div class = 'first-line'>";
					html += lists[i].list_name;
					html += "</div>";
					html += "<div class = 'second-line'></div></div></div>";
					html += "<div class = 'col-md-3 right-section'>";
					html += "<div class = 'item-count'>" + lists[i].items_count + "</div></div></div>";
					html += "<div class = 'col-md-12 icon-area'>";
					html += "<div class = 'button-line'>";
					html += "<button type='button' class='close icon-area-close' data-dismiss='icon-area' aria-hidden='true'>×</button>";
					html += "</div>";
					html += "<div class = 'icon-line'>";
					html += "<div class = 'col-md-4'>";
					html += "<a href = '#'><i class = 'fa fa-heart-o'></i>Favorite</a>";
					html += "</div>";
					html += "<div class = 'col-md-4'>";
					html += "<a href = '#'><i class = 'fa fa-comment'></i>Comment</a>";
					html += "</div>";
					html += "<div class = 'col-md-4'>";
					html += "<a href = '#'><i class = 'fa fa-trash'></i>Delete</a>";
					html += "</div>";
					html += "</div>";
					html += "<div class = 'icon-line'>";
					html += "<div class = 'col-md-4'>";
					html += "<a href = '#'><i class = 'fa fa-check-square-o'></i>Mark Phurchased</a>";
					html += "</div>";
					html += "<div class = 'col-md-4'>";
					html += "<a href = '#'><i class = 'fa fa-files-o'></i>Make a Copy</a>";
					html += "</div>";
					html += "<div class = 'col-md-4'>";
					html += "<a href = '#'><i class = 'fa fa-shopping-cart'></i>Cart Item(s)</a>";
					html += "</div>";
					html += "</div>";
					html += "<div class = 'icon-line'>";
					html += "<div class = 'col-md-4'>";
					html += "<a href = '#'><i class = 'fa fa-info-circle'></i>Details</a>";
					html += "</div>";
					html += "<div class = 'col-md-4'>";
					html += "<a href = '#'><i class = 'fa fa-share-alt'></i>Share</a>";
					html += "</div>";
					html += "<div class = 'col-md-4'>";
					html += "<a href = '#'><i class = 'fa fa-print'></i>Print List</a>";
					html += "</div>";
					html += "</div>";
					html += "</div>";
					// console.log(html);
				}
				
				$('.lists-section').append(html);
				
				$('.list-row').click(function () {
					$(this).addClass('clicked');
					var index = $('.list-row').index(this);
					$('.icon-area').eq(index).show();
				});
				$('.icon-area-close').click(function () {
					var index = $('.icon-area-close').index(this);
					$('.icon-area').eq(index).hide();
					$('.list-row').eq(index).removeClass('clicked');
				});
			}
		});
	});
	
	$('.list-row').click(function () {
		$(this).addClass('clicked');
		var index = $('.list-row').index(this);
		$('.icon-area').eq(index).show();
	});
	$('.icon-area-close').click(function () {
		var index = $('.icon-area-close').index(this);
		$('.icon-area').eq(index).hide();
		$('.list-row').eq(index).removeClass('clicked');
	});
});

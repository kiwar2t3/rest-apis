//Masonry js functions
$(document).ready(function() {
	$("#signup-form").validate({
		rules : {
			user_name : {
				required : true
			},
			user_email : {
				required : true,
				email : true
			},
			user_password : {
				required : true
			},
			confirm_password : {
				equalTo : "#user_password"
			}
		},
		messages : {
			user_password : {
				required : "Please provide a password",
				minlength : "Password at least have 8 characters"
			},
			user_email : "Please enter a valid email address"
		},

		// Do not change code below
		errorPlacement : function(error, element) {
			error.insertAfter(element.parent());
		}
	});
});

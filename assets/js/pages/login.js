//Masonry js functions
$(document).ready(function() {
	$('document').ready(function() {
		$("#login-form").validate({
			rules : {
				email : {
					required : true,
					email : true
				},
				password : {
					required : true,
				},
			},
			messages : {
				password : {
					required : "Please provide a password",
					minlength : "Password at least have 8 characters"
				},
				email : "Please enter a valid email address"
			},
			// Do not change code below
			errorPlacement : function(error, element) {
				error.insertAfter(element.parent());
			}
		});

	});
});

<?php
require_once ('./library/application_top.php');
include_once ('./views/header.php');
?>
<!--=== Breadcrumbs ===-->
<div class="breadcrumbs">
<div class="container">
<h1 class="pull-left">About Us</h1>
<ul class="pull-right breadcrumb">
<li><a href="index.html">Home</a></li>
<li><a href="">Pages</a></li>
<li class="active">About Us</li>
</ul>
</div><!--/container-->
</div><!--/breadcrumbs-->
<!--=== End Breadcrumbs ===-->

<!--=== Container Part ===-->
<div class="container content-sm">
<div class="headline-center margin-bottom-60">
<h2>Who We Are</h2>
</div>
</div>
<!--=== End Container Part ===-->

<!--=== Service Block v5 ===-->
<div class="service-block-v5">
<div class="container">
<div class="row equal-height-columns">
<div class="col-md-6 service-inner equal-height-column">
<span>Place Holder1</span>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's</p>
</div>
<div class="col-md-6 service-inner equal-height-column service-border">
<span>Place Holder1</span>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's</p>
</div>
</div><!--/end row-->
</div><!--/end container-->
</div>
<!--=== End Service Block v5 ===-->

<!--=== Container Part ===-->
<div class="company-info">
<div class="container">
<div class="row">
<div class = 'col-md-4'>
<div class = 'col-md-2'></div>
<div class="col-md-10">
<span><h4>Company's Founding Date</h4></span>
<p>December 1998</p>
<span><h4>Website Address</h4></span>
<p><a href='#'>www.foodapp.com</a></p>
</div>

</div>
<div class = 'col-md-4'>
<div class = 'col-md-2'></div>
<div class="col-md-10">
<span><h4>Corporate Headquarters</h4></span>
<p>1511 North First Street</p>
<p>British Columbia, Ca</p>
<span><h4>Wordwide Operations</h4></span>
</div>
</div>
<div class = 'col-md-4'>
<div class = 'col-md-2'></div>
<div class="col-md-10">
<span><h4>Media Resources</h4></span>
<p><a href='#'>More Information</a></p>
<span><h4>Media Inquiries</h4></span>
<p><a href='#'>Contact Us</a></p>
</div>
</div>
</div><!--/end row-->
</div><!--/end container-->
</div>
<!--=== End Container Part ===-->

<!--=== Team v4 ===-->
<div class="container content-sm">
<div class="headline-center margin-bottom-60">
<h2>Our Team Members</h2>
</div>
<div class = 'row team-v4'>
<div class = 'col-md-4'></div>
<div class="col-md-4 col-sm-6 md-margin-bottom-50">
<img class="img-responsive" src="assets/img/team/img15-md.jpg" alt="">
<span>Name</span>
<small>- President and CEO -</small>
</div>
<div class = 'col-md-4'></div>
</div>
<div class="row team-v4">

<div class="col-md-4 col-sm-6 md-margin-bottom-50">
<img class="img-responsive" src="assets/img/team/img31-md.jpg" alt="">
<span>Name</span>
<small>- EVP, Chief Strategy & Growth Officer -</small>
</div>
<div class="col-md-4 col-sm-6 sm-margin-bottom-50">
<img class="img-responsive" src="assets/img/team/img18-md.jpg" alt="">
<span>Name</span>
<small>- EVP. Cheif Commercial Officer -</small>
</div>
<div class="col-md-4 col-sm-6">
<img class="img-responsive" src="assets/img/team/img37-md.jpg" alt="">
<span>Name</span>
<small>- EVP. Chief Operating Officer -</small>
</div>
<div class="col-md-4 col-sm-6">
<img class="img-responsive" src="assets/img/team/img37-md.jpg" alt="">
<span>Name</span>
<small>- EVP. Chief Risk and Data Officer -</small>
</div>
<div class="col-md-4 col-sm-6">
<img class="img-responsive" src="assets/img/team/img37-md.jpg" alt="">
<span>Name</span>
<small>- EVP. Chief Financian Officer -</small>
</div>
<div class="col-md-4 col-sm-6">
<img class="img-responsive" src="assets/img/team/img37-md.jpg" alt="">
<span>Name</span>
<small>- EVP. Chief Business Affairs & Legal Officer -</small>
</div>
</div><!--/end row-->
</div>
<?php include './views/footer.php'; ?>
<!--=== End Team v4 ===-->

<!-- JS Global Compulsory -->
<script type="text/javascript" src="assets/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript" src="assets/plugins/jquery/jquery-migrate.min.js"></script>
<script type="text/javascript" src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<!-- JS Implementing Plugins -->
<script type="text/javascript" src="assets/plugins/back-to-top.js"></script>
<script type="text/javascript" src="assets/plugins/smoothScroll.js"></script>
<script type="text/javascript" src="assets/plugins/jquery-appear.js"></script>
<script type="text/javascript" src="assets/plugins/fancybox/source/jquery.fancybox.pack.js"></script>
<script type="text/javascript" src="assets/plugins/owl-carousel/owl-carousel/owl.carousel.js"></script>
<!-- JS Customization -->
<script type="text/javascript" src="assets/js/custom.js"></script>
<!-- JS Page Level -->
<script type="text/javascript" src="assets/js/app.js"></script>
<script type="text/javascript" src="assets/js/plugins/fancy-box.js"></script>
<script type="text/javascript" src="assets/js/plugins/progress-bar.js"></script>
<script type="text/javascript" src="assets/js/plugins/owl-carousel.js"></script>
<script type="text/javascript" src="assets/js/plugins/style-switcher.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() {
App.init();
FancyBox.initFancybox();
OwlCarousel.initOwlCarousel();
StyleSwitcher.initStyleSwitcher();
ProgressBar.initProgressBarHorizontal();
});
</script>
<!--[if lt IE 9]>
<script src="assets/plugins/respond.js"></script>
<script src="assets/plugins/html5shiv.js"></script>
<script src="assets/plugins/placeholder-IE-fixes.js"></script>
<![endif]-->
</body>
</html>

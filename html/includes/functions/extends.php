<?php

function tep_validate_password($plain, $encrypted) {
  if (tep_not_null($plain) && tep_not_null($encrypted)) {
    // split apart the hash / salt
    $stack = explode(':', $encrypted);

    if (sizeof($stack) != 2)
      return false;

    if (hash_hmac("sha256", utf8_encode($plain), utf8_encode($stack[1]), false) == $stack[0]) {
      return true;
    }
  }

}

function tep_encrypt_password($plain) {
  $password = '';

  for ($i = 0; $i < 10; $i++) {
    $password .= tep_rand();
  }

  $salt = substr(md5($password), 0, 4);

  $password = hash_hmac("sha256", utf8_encode($plain), utf8_encode($salt), false); //md5($salt . $plain) . ':' . $salt;

  return $password . ":" . $salt;
}

function get_user_status_color($status) {
  switch ($status) {
    case 'checked_in': return 'green';
    case 'looking': return 'yellow';
    case 'busy': return 'red';
    case 'designated_driver': return 'orange';
  }

  return "white";
}

function get_address($address, $key = "") {
  $result = "";
  $result .= $address[$key . 'address_street_1'] . "";
  if ($address[$key . 'address_street_2'] != '') {
    $result .= " " . $address[$key . 'address_street_2'];
  }
  $result .= ", ";
  $result .= $address[$key . 'address_city'] . ", ";
  $result .= $address[$key . 'address_state'] . " ";
  $result .= $address[$key . 'address_zip'] . ", ";
  $result .= $address[$key . 'address_country'];

  return $result;
}

function get_url_title($title) {
  $title = urlencode($title);

  $avalibal_charactors = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789.!@+-&";

  $result = "";

  for ($i = 0; $i < strlen($title); $i ++) {
    $charac = substr($title, $i, 1);

    if (strpos($avalibal_charactors, $charac) === false) {
      continue;
    }
    $result.= $charac;
  }

  return $result;
}

function upload_file($title, $file_name, $require = true, $width = 0, $height = 0, $crop = false) {
  global $message_cls, $upload_img_path;
  if (!isset($_FILES[$file_name]) || $_FILES[$file_name]['tmp_name'] == '') {
    if ($require) {
      $message_cls->set_error($file_name, "Empty file");
    }

    return "";
  }
  $file = $_FILES[$file_name];

  $year = date('Y');
  $month = date('m');
  $day = date('d');

  $upload_dir = $year;
  @mkdir(DIR_WS_UPLOAD . $upload_dir);
  
  $upload_dir.= "/" . $month;
  @mkdir(DIR_WS_UPLOAD . $upload_dir);
  
  $upload_dir.= $day;
  @mkdir(DIR_WS_UPLOAD . $upload_dir);
  
  $upload_dir.= "/";
  
  /*chmod(DIR_WS_UPLOAD, 0777);
  if (!is_dir(DIR_WS_UPLOAD . $upload_dir)) {
    if (mkdir(DIR_WS_UPLOAD . $upload_dir, 0777, true)) {
      chmod(DIR_WS_UPLOAD . $upload_dir, 0777);
    }
  }*/

  $info = pathinfo($file['name']);
  $ext = $info['extension'];

  $new_image_file = $upload_dir . get_url_title($title) . "." . $ext;

  while (file_exists(DIR_WS_UPLOAD . $new_image_file)) {
    $new_image_file = $upload_dir . urlencode($title) . "_" . rand(1, 99) . "." . $ext;
  }

  if (move_uploaded_file($file["tmp_name"], DIR_WS_UPLOAD . $new_image_file)) {
    $upload_img_path = DIR_WS_UPLOAD . $new_image_file;
    if ($width != 0 && $height != 0) {
      $resized_image = image_resize(DIR_WS_UPLOAD . $new_image_file, $width, $height, $crop);
      @unlink(DIR_WS_UPLOAD . $upload_img_path);
      @rename($resized_image, DIR_WS_UPLOAD . $new_image_file);
    }
    return HTTP_WS_UPLOAD . $new_image_file;
  } else {
    $message_cls->set_error($file_name, "Error upload file.");
  }

  return "";
}

function download_file($title, $file_url, $require = true, $width = 0, $height = 0, $crop = false) {
  global $message_cls, $upload_img_path;

  $year = date('Y');
  $month = date('m');
  $day = date('d');
  /*$upload_dir = $year . "/" . $month . "/" . $day . "/";
  chmod(DIR_WS_UPLOAD, 0777);
  if (!is_dir(DIR_WS_UPLOAD . $upload_dir)) {
    if (mkdir(DIR_WS_UPLOAD . $upload_dir, 0777, true)) {
      chmod(DIR_WS_UPLOAD . $upload_dir, 0777);
    }
  }*/
  
  $upload_dir = $year;
  @mkdir(DIR_WS_UPLOAD . $upload_dir);
  
  $upload_dir.= "/" . $month;
  @mkdir(DIR_WS_UPLOAD . $upload_dir);
  
  $upload_dir.= $day;
  @mkdir(DIR_WS_UPLOAD . $upload_dir);
  
  $upload_dir.= "/";
  
  if (strrpos($file_url, '?'))
    $ext = substr($file_url, strrpos($file_url, '.') + 1, strrpos($file_url, '?') - strrpos($file_url, '.') - 1);
  else
    $ext = substr($file_url, strrpos($file_url, '.') + 1);
  $new_image_file = $upload_dir . get_url_title($title) . "." . $ext;
  while (file_exists(DIR_WS_UPLOAD . $new_image_file)) {
    $new_image_file = $upload_dir . urlencode($title) . "_" . rand(1, 99) . "." . $ext;
  }

  $img_file = file_get_contents($file_url);
  if ($img_file == false) {
    if ($require) {
      $message_cls->set_error($file_name, "Invalid URL");
    }
    return false;
  }

  $file_loc = DIR_WS_UPLOAD . $new_image_file;

  $file_handler = fopen($file_loc, 'w');

  if (fwrite($file_handler, $img_file) == false) {
    return false;
  }

  fclose($file_handler);

  $upload_img_path = DIR_WS_UPLOAD . $new_image_file;

  if ($width != 0 && $height != 0) {
    $resized_image = image_resize(DIR_WS_UPLOAD . $new_image_file, $width, $height, $crop);
    @unlink(DIR_WS_UPLOAD . $upload_img_path);
    @rename($resized_image, DIR_WS_UPLOAD . $new_image_file);
  }

  return HTTP_WS_UPLOAD . $new_image_file;
}

function formated_image($original_img_url, $original_img_path, $width, $height, $crop = false) {
  $formatted_img = image_resize($original_img_path, $width, $height, $crop);

  $url_info = pathinfo($original_img_url);
  $img_info = pathinfo($formatted_img);

  return $url_info['dirname'] . "/" . $img_info['basename'];
}

function formatted_mobile_image($original_img_url, $original_img_path) {
  $formatted_img = image_resize($original_img_path, MOBILE_IMAGE_WIDTH, MOBILE_IMAGE_HEIGHT, true);

  $url_info = pathinfo($original_img_url);
  $img_info = pathinfo($formatted_img);

  return $url_info['dirname'] . "/" . $img_info['basename'];
}

function thumb_mobile_image($original_img_url, $original_img_path) {
  $formatted_img = image_resize($original_img_path, AVATAR_IMAGE_WIDTH, AVATAR_IMAGE_HEIGHT, true);

  $url_info = pathinfo($original_img_url);
  $img_info = pathinfo($formatted_img);

  return $url_info['dirname'] . "/" . $img_info['basename'];
}

function upload_avatar($user_id, $user_name, $file_name) {
  global $message_cls;

  if (!isset($_FILES[$file_name]) || $_FILES[$file_name]['tmp_name'] == '') {
    return "";
  }

  $file = $_FILES[$file_name];

  $avatar_dir = "avatar/";
  $avatar_dir.= ($user_id - ($user_id % 10000)) . "/";
  
  //echo substr(sprintf('%o', fileperms(DIR_WS_UPLOAD)), -4);
  
  //@chmod(DIR_WS_UPLOAD, 755);
  if (!is_dir(DIR_WS_UPLOAD . $avatar_dir)) {
    if (mkdir(DIR_WS_UPLOAD . $avatar_dir)) {
      //chmod(DIR_WS_UPLOAD . $avatar_dir, 755, true);
    }
  }

  $ext = pathinfo($file['name'], PATHINFO_EXTENSION);
  $new_image_file = $user_name . "." . $ext;
  @unlink(DIR_WS_UPLOAD . $avatar_dir . $new_image_file);

  if (move_uploaded_file($file["tmp_name"], DIR_WS_UPLOAD . $avatar_dir . $new_image_file)) {
    $avartar_image = image_resize(DIR_WS_UPLOAD . $avatar_dir . $new_image_file, AVATAR_IMAGE_WIDTH, AVATAR_IMAGE_HEIGHT, true);
    @unlink(DIR_WS_UPLOAD . $avatar_dir . $new_image_file);
    @rename($avartar_image, DIR_WS_UPLOAD . $avatar_dir . $new_image_file);

    return HTTP_WS_UPLOAD . $avatar_dir . $new_image_file;
  } else {
    
  }

  return "";
}

function upload_profile_file($profile_id, $user_name, $file_name) {
global $message_cls;
if (!isset($_FILES[$file_name]) || $_FILES[$file_name]['tmp_name'] == '') {
return "";
    }

$file = $_FILES[$file_name];
$file_dir = 'file/profile';
$file_dir .= ($profile_id - ($profile_id % 10000)) . "/";
//chmod(DIR_WS_UPLOAD, "0777");
if (!is_dir(DIR_WS_UPLOAD . $file_dir)) {
if (mkdir(DIR_WS_UPLOAD . $file_dir)) {
//if (mkdir(DIR_WS_UPLOAD . $file_dir, "0777", true)) {
//chmod(DIR_WS_UPLOAD . $file_dir, "0777");
}
}
$ext = pathinfo($file['name'], PATHINFO_EXTENSION);
$new_file = $user_name . "." . $ext;
@unlink(DIR_WS_UPLOAD . $file_dir . $new_file);
if (move_uploaded_file($file["tmp_name"], DIR_WS_UPLOAD . $file_dir . $new_file)) {
$return = array();
$return['file_url'] = HTTP_WS_UPLOAD . $file_dir . $new_file;
$return['type'] = $ext;
return $return;
} else {

}

return "";
}

function upload_product_image($product_id, $product_name, $file_name) {
global $message_cls;
if (!isset($_FILES[$file_name]) || $_FILES[$file_name]['tmp_name'] == '') {
return "";
}

$file = $_FILES[$file_name];
$image_dir = "product/";
/*$image_dir.= date('Y') . '/' . date('m') . '/';
chmod(DIR_WS_UPLOAD, 0777);
if (!is_dir(DIR_WS_UPLOAD . $image_dir)) {
if (mkdir(DIR_WS_UPLOAD . $image_dir, 0777, true)) {
  chmod(DIR_WS_UPLOAD . $image_dir, 0777);
}
}*/
$image_dir.= date('Y');
@mkdir($image_dir);

$image_dir.= '/' . date('m');
@mkdir($image_dir);

$image_dir.= '/';

$ext = pathinfo($file['name'], PATHINFO_EXTENSION);
$new_image_file = $product_id . "." . $ext;
@unlink(DIR_WS_UPLOAD . $image_dir . $new_image_file);
$original_image = DIR_WS_UPLOAD . $image_dir . $new_image_file;
$small_image = DIR_WS_UPLOAD . $image_dir . $product_id . '-small.' . $ext;
$large_image = DIR_WS_UPLOAD . $image_dir . $product_id . '-large.' . $ext;
$return = array();
$return['original'] = HTTP_WS_UPLOAD . $image_dir . $new_image_file;
$return['small'] = HTTP_WS_UPLOAD . $image_dir . $product_id . '-small.' . $ext;
$return['large'] = HTTP_WS_UPLOAD . $image_dir . $product_id . '-large.' . $ext;
for ( $i = 0; $i < 3; $i ++ ) {
if ( $i == 0 ) {
$newFileName = $original_image;
}
else if ( $i == 1 ) {
$newFileName = $small_image;
$width = PRODUCT_SMALLIMAGE_WIDTH;
$height = PRODUCT_SMALLIMAGE_HEIGHT;
}
else if ( $i == 2 ) {
$newFileName = $large_image;
$width = PRODUCT_LARGEIMAGE_WIDTH;
$height = PRODUCT_LARGEIMAGE_HEIGHT;
}
if (copy($file["tmp_name"], $newFileName)) {
if ( $i != 0 ) {
$product_image = image_resize($newFileName, $width, $height, true);
@rename($product_image, $newFileName);
}

// return HTTP_WS_UPLOAD . $image_dir . $new_image_file;
} else {

}
}
// if (move_uploaded_file($file["tmp_name"], DIR_WS_UPLOAD . $image_dir . $new_image_file)) {
// var_dump(getimagesize(DIR_WS_UPLOAD . $image_dir . $new_image_file));exit;
// $product_image = image_resize(DIR_WS_UPLOAD . $image_dir . $new_image_file, PRODUCT_SMALLIMAGE_WIDTH, PRODUCT_SMALLIMAGE_HEIGHT, true);
// @unlink(DIR_WS_UPLOAD . $image_dir . $new_image_file);
// @rename($product_image, DIR_WS_UPLOAD . $image_dir . $new_image_file);

// return HTTP_WS_UPLOAD . $image_dir . $new_image_file;
// } else {

// }
return $return;
}

function upload_discount_image($discount_id, $file_name) {
global $message_cls;
if (!isset($_FILES[$file_name]) || $_FILES[$file_name]['tmp_name'] == '') {
return "";
}

$file = $_FILES[$file_name];
$image_dir = "discount/";
/*$image_dir.= date('Y') . '/' . date('m') . '/';
chmod(DIR_WS_UPLOAD, 0777);
if (!is_dir(DIR_WS_UPLOAD . $image_dir)) {
if (mkdir(DIR_WS_UPLOAD . $image_dir, 0777, true)) {
  chmod(DIR_WS_UPLOAD . $image_dir, 0777);
}
}*/
@mkdir($image_dir);

$image_dir.= '/' . date('Y');
@mkdir($image_dir);

$image_dir.= '/' . date('m');
@mkdir($image_dir);

$image_dir.= '/';

$ext = pathinfo($file['name'], PATHINFO_EXTENSION);
$new_image_file = $discount_id . "." . $ext;
@unlink(DIR_WS_UPLOAD . $image_dir . $new_image_file);
$original_image = DIR_WS_UPLOAD . $image_dir . $new_image_file;
$small_image = DIR_WS_UPLOAD . $image_dir . $discount_id . '-small.' . $ext;
$large_image = DIR_WS_UPLOAD . $image_dir . $discount_id . '-large.' . $ext;
$return = array();
$return['original'] = $image_dir . $new_image_file;
$return['small'] = $image_dir . $discount_id . '-small.' . $ext;
$return['large'] = $image_dir . $discount_id . '-large.' . $ext;
for ( $i = 0; $i < 3; $i ++ ) {
if ( $i == 0 ) {
$newFileName = $original_image;
}
else if ( $i == 1 ) {
$newFileName = $small_image;
$width = PRODUCT_SMALLIMAGE_WIDTH;
$height = PRODUCT_SMALLIMAGE_HEIGHT;
}
else if ( $i == 2 ) {
$newFileName = $large_image;
$width = PRODUCT_LARGEIMAGE_WIDTH;
$height = PRODUCT_LARGEIMAGE_HEIGHT;
}
if (copy($file["tmp_name"], $newFileName)) {
if ( $i != 0 ) {
$product_image = image_resize($newFileName, $width, $height, true);
@rename($product_image, $newFileName);
}

// return HTTP_WS_UPLOAD . $image_dir . $new_image_file;
} else {

}
}
return HTTP_WS_UPLOAD . $return['original'];
}

function export_table_csv($table_name, $export_file_name) {
  export_query_csv("select * from " . $table_name);
}

function export_query_csv($query, $export_file_name) {
  $result = tep_db_query($query);

  if (!$result) {
    echo '<script lanuage="javascript">alert("No export data.")</script>';
  }

  $filed_count = mysql_num_fields($result);
  $headers = array();
  for ($i = 0; $i < $filed_count; $i ++) {
    $headers[] = mysql_field_name($result, $i);
  }

  $fp = fopen("php://output", "w");
  if ($fp) {
    header("Content-type: text/csv");
    header("Content-Disposition: attachment; filename=" . $export_file_name);
    header("Pragma: no-cache");
    fputcsv($fp, $headers);
    while ($row = tep_db_fetch_array($result)) {
      fputcsv($fp, array_values($row));
    }
  }
}

function get_longitude_length($lat, $long, $lat_length = 111000) {
  return abs($lat_length * cos($lat));
}

function get_mile_from_meter($meter) {
  return round(($meter / 1000 * 0.625), 2);
}

/*
 * user profile
 */

function get_user_name($user_id) {
  $user = teb_one_query(TABLE_USERS, array("user_id" => $user_id));

  return $user["user_name"];
}

function get_user_fullname($user_id) {
  $user = teb_one_query(TABLE_USERS, array("user_id" => $user_id));

  return $user["user_firstname"] . " " . $user["user_lastname"];
}

function is_block_user_in_venue($venue_id, $user_id) {
  global $wpdb;
  return $wpdb->get_var("select count(*) as data_count from " . TABLE_VENUES_BLOCKED_USERS . " where user_id='" . $user_id . "' and venue_id='" . $venue_id . "'");
}

function block_user_in_venue($venue_id, $user_id) {
  global $wpdb;
  $blocked = teb_one_query(TABLE_VENUES_BLOCKED_USERS, array("user_id" => $user_id, "venue_id" => $venue_id));
  if ($blocked == '') {
    $wpdb->insert(TABLE_VENUES_BLOCKED_USERS, array("user_id" => $user_id, "venue_id" => $venue_id));
  }
}

function get_show_user_name($user_profile) {
  if ($user_profile['user_name_display_type'] == 'fullname') {
    return $user_profile['user_firstname'] . " " . $user_profile['user_lastname'];
  }

  if ($user_profile['user_name_display_type'] == 'shortname') {
    return $user_profile['user_firstname'] . " " . substr($user_profile['user_lastname'], 0, 1);
  }

  // snipstamp
  return $user_profile['user_name'];
}

/*
 * user actions
 */

function get_action_review($action) {
  $str = "";

  if ($action['action_type'] == 'signup') {
    $str = "Registred to our ";
  } elseif ($action['action_type'] == 'venue') {
    $str = "Followed venue ";
  } elseif ($action['action_type'] == 'qrcode') {
    $str = "Skined qrcode ";
  } elseif ($action['action_type'] == 'checkin') {
    $str = "Checked-in ";
  } elseif ($action['action_type'] == 'review') {
    $str = "Writed review ";
  } elseif ($action['action_type'] == 'comment') {
    $str = "Writed comment ";
  }

  $str.= "@" . tep_get_after_date_time($action['action_time']);

  return $str;
}

function add_point($user_id, $point_mark) {
  global $wpdb;
  $point = array("user_id" => $user_id, "point_week" => date('W'), "point_year" => date('Y'));

  $old_point = teb_one_query(TABLE_POINTS, $point);
  if ($old_point == "") {
    $point['point_mark'] = $point_mark;

    return $wpdb->insert(TABLE_POINTS, $point);
  } else {
    return $wpdb->update(TABLE_POINTS, array("point_mark" => $old_point['point_mark'] + $point_mark), array("user_id" => $user_id, "point_week" => date('W'), "point_year" => date('Y')));
  }
}

function checked_in_venue($venue_id, $user_id, $point_mark, $latitude, $longitude) {
  global $wpdb;
  $now_time = tep_now_datetime();

  // add location
  $wpdb->insert(TABLE_LOCATIONS, array("user_id" => $user_id, "location_latitude" => $latitude, "location_longitude" => $longitude, "location_timestamp" => $now_time));
  $new_location_id = tep_db_insert_id();

  // finished last check-in
  $last_check_in = $wpdb->get_row("select * from " . TABLE_CHECK_INS . " where user_id='" . $user_id . "' order by status_changed_from desc limit 1");
  if ($last_check_in->checked_time == 0) {
    $checked_time = $now_time - $last_check_in->status_changed_from;

    $wpdb->update(TABLE_CHECK_INS, array("check_status" => "checked_out", "checked_time" => $checked_time, "status_changed_to" => $now_time), array("location_id" => $last_check_in['location_id']));
  }

  // add check-in
  $wpdb->insert(TABLE_CHECK_INS, array("venue_id" => $venue_id, "user_id" => $user_id, "location_id" => $new_location_id, "check_status" => "checked_in", "status_changed_from" => $now_time));
  $new_check_in_id = tep_db_insert_id();

  // add actions
  $state = $wpdb->insert(TABLE_USER_ACTIONS, array("venue_id" => $venue_id, "user_id" => $user_id, "action_type" => "checkin", "releation_id" => $new_check_in_id, "point" => $point_mark, "action_time" => $now_time));

  // update user location info
  $user_info = array(
      "user_latest_location_latitude" => $latitude,
      "user_latest_location_longitude" => $longitude,
      "user_latest_location_timestamp" => $now_time,
      "user_past_location_id" => $new_location_id,
      "user_status" => "checked_in",
      "venue_id" => $venue_id);

  $state = $wpdb->update(TABLE_USERS, $user_info, array("user_id" => $user_id));

  return $state;
}

function getUploadFileRelativePath($absolutepath) {
  return substr($absolutepath, strlen(HTTP_WS_UPLOAD));
}

function getUploadFileAbsolutePath($relativepath) {
  return HTTP_WS_UPLOAD . $relativepath;
}

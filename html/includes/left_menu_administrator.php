<ul id="adminmenu">

    <!-- users -->
    <li class="wp-has-submenu menu-top menu-top-first" id="menu-users">
        <div class="wp-menu-image"><br /></div>
        <div class="wp-menu-toggle"><br /></div>
        <a href='users.php' class="wp-has-submenu menu-top" tabindex="1"><strong>Users</strong></a>
        <div class='wp-submenu'>
            <ul>
                <li><a href="users.php" >View Users</a></li>	          
                <li><a href="user_edit.php">New User</a></li>	            
            </ul>
        </div>
    </li>
    <li class="wp-has-submenu menu-top menu-top-last">
        <a class="wp-has-submenu menu-top" tabindex="1"></a>
    </li>

    <!-- categories -->
    <li class="wp-has-submenu menu-top menu-top-first" id="menu-users">
        <div class="wp-menu-image"><br /></div>
        <div class="wp-menu-toggle"><br /></div>
        <a href='categories.php' class="wp-has-submenu menu-top" tabindex="1"><strong>Categories</strong></a>
        <div class='wp-submenu'>
            <ul>
                <li><a href="categories.php" >View Categories</a></li>	          
                <li><a href="category_edit.php">New Category</a></li>	            
            </ul>
        </div>
    </li>
    <li class="wp-has-submenu menu-top menu-top-last">
        <a class="wp-has-submenu menu-top" tabindex="1"></a>
    </li>

    <!-- brands -->
    <li class="wp-has-submenu menu-top menu-top-first" id="menu-users">
        <div class="wp-menu-image"><br /></div>
        <div class="wp-menu-toggle"><br /></div>
        <a href='brands.php' class="wp-has-submenu menu-top" tabindex="1"><strong>Brands</strong></a>
        <div class='wp-submenu'>
            <ul>
                <li><a href="brands.php" >View Brands</a></li>	          
                <li><a href="brand_edit.php">New Brand</a></li>	            
            </ul>
        </div>
    </li>
    <li class="wp-has-submenu menu-top menu-top-last">
        <a class="wp-has-submenu menu-top" tabindex="1"></a>
    </li>
    
    <!-- stores -->
    <li class="wp-has-submenu menu-top menu-top-first" id="menu-users">
        <div class="wp-menu-image"><br /></div>
        <div class="wp-menu-toggle"><br /></div>
        <a href='stores.php' class="wp-has-submenu menu-top" tabindex="1"><strong>Stores</strong></a>
        <div class='wp-submenu'>
            <ul>
                <li><a href="stores.php" >View Stores</a></li>	          
                <li><a href="store_edit.php">New Store</a></li>	            
            </ul>
        </div>
    </li>
    <li class="wp-has-submenu menu-top menu-top-last">
        <a class="wp-has-submenu menu-top" tabindex="1"></a>
    </li>

    <!-- products -->
    <li class="wp-has-submenu menu-top menu-top-first" id="menu-users">
        <div class="wp-menu-image"><br /></div>
        <div class="wp-menu-toggle"><br /></div>
        <a href='products.php' class="wp-has-submenu menu-top" tabindex="1"><strong>Products</strong></a>
        <div class='wp-submenu'>
            <ul>
                <li><a href="products.php" >View Products</a></li>	          
                <li><a href="product_edit.php">New Product</a></li>	            
            </ul>
        </div>
    </li>
    <li class="wp-has-submenu menu-top menu-top-last">
        <a class="wp-has-submenu menu-top" tabindex="1"></a>
    </li>

    <!-- deals -->
    <li class="wp-has-submenu menu-top menu-top-first" id="menu-users">
        <div class="wp-menu-image"><br /></div>
        <div class="wp-menu-toggle"><br /></div>
        <a href='deals.php' class="wp-has-submenu menu-top" tabindex="1"><strong>Deals</strong></a>
        <div class='wp-submenu'>
            <ul>
                <li><a href="deals.php" >View Deals</a></li>
                <li><a href="deal_edit.php">New Deal</a></li>	            
            </ul>
        </div>
    </li>
    <li class="wp-has-submenu menu-top menu-top-last">
        <a class="wp-has-submenu menu-top" tabindex="1"></a>
    </li>
    
    <!-- product reviews -->
    <li class="wp-has-submenu menu-top menu-top-first" id="menu-users">
        <div class="wp-menu-image"><br /></div>
        <div class="wp-menu-toggle"><br /></div>
        <a href='reviews.php' class="wp-has-submenu menu-top" tabindex="1"><strong>Product Reviews</strong></a>
        <div class='wp-submenu'>
            <ul>
                <li><a href="reviews.php" >View Product Reviews</a></li>	          
                <li><a href="review_edit.php">New Product Review</a></li>	            
            </ul>
        </div>
    </li>
    <li class="wp-has-submenu menu-top menu-top-last">
        <a class="wp-has-submenu menu-top" tabindex="1"></a>
    </li>
    
    <!-- shopping lists -->
    <li class="wp-has-submenu menu-top menu-top-first" id="menu-users">
        <div class="wp-menu-image"><br /></div>
        <div class="wp-menu-toggle"><br /></div>
        <a href='shoppings.php' class="wp-has-submenu menu-top" tabindex="1"><strong>Shopping Lists</strong></a>
        <div class='wp-submenu'>
            <ul>
                <li><a href="shoppings.php" >Shopping Lists</a></li>	          
                <li><a href="shopping_edit.php">New Shopping Item</a></li>	            
            </ul>
        </div>
    </li>
    <li class="wp-has-submenu menu-top menu-top-last">
        <a class="wp-has-submenu menu-top" tabindex="1"></a>
    </li>
    
    <!-- favorite lists -->
    <li class="wp-has-submenu menu-top menu-top-first" id="menu-users">
        <div class="wp-menu-image"><br /></div>
        <div class="wp-menu-toggle"><br /></div>
        <a href='favorites.php' class="wp-has-submenu menu-top" tabindex="1"><strong>Favorite Products</strong></a>
        <div class='wp-submenu'>
            <ul>
                <li><a href="favorites.php" >Favorite Products</a></li>	          
                <li><a href="favorite_edit.php">New Favorite Product</a></li>	            
            </ul>
        </div>
    </li>
    <li class="wp-has-submenu menu-top menu-top-last">
        <a class="wp-has-submenu menu-top" tabindex="1"></a>
    </li>
    
    <!-- favorite lists -->
    <li class="wp-has-submenu menu-top menu-top-first" id="menu-users">
        <div class="wp-menu-image"><br /></div>
        <div class="wp-menu-toggle"><br /></div>
        <a href='favoritestores.php' class="wp-has-submenu menu-top" tabindex="1"><strong>Favorite Store Lists</strong></a>
        <div class='wp-submenu'>
            <ul>
                <li><a href="favoritestores.php" >Favorite Stores</a></li>	          
                <li><a href="favoritestore_edit.php">New Favorite Store</a></li>	            
            </ul>
        </div>
    </li>
    <li class="wp-has-submenu menu-top menu-top-last">
        <a class="wp-has-submenu menu-top" tabindex="1"></a>
    </li>
    
    <!-- feedback lists -->
    <li class="wp-has-submenu menu-top menu-top-first" id="menu-users">
        <div class="wp-menu-image"><br /></div>
        <div class="wp-menu-toggle"><br /></div>
        <a href='feedbacks.php' class="wp-has-submenu menu-top" tabindex="1"><strong>Feedbacks</strong></a>
        <div class='wp-submenu'>
            <ul>
                <li><a href="feedbacks.php" >Feedbacks</a></li>	                          
            </ul>
        </div>
    </li>
    <li class="wp-has-submenu menu-top menu-top-last">
        <a class="wp-has-submenu menu-top" tabindex="1"></a>
    </li>

    <!-- system setting -->
    <li class="wp-has-submenu menu-top menu-top-first" id="menu-settings">
        <div class="wp-menu-image"><br /></div>
        <div class="wp-menu-toggle"><br /></div>
        <a href='configurations.php' class="wp-has-submenu menu-top" tabindex="1"><strong>System Settings</strong></a>
        <div class='wp-submenu'>
            <ul>
                <li><a href="change_password.php" >Change Password</a></li>
                <li><a href="logout.php">Logout</a></li>
            </ul>
        </div>
    </li>
    <li class="wp-has-submenu menu-top menu-top-last">
        <a class="wp-has-submenu menu-top" tabindex="1"></a>
    </li>
</ul>
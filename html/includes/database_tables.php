<?php
/*
  $Id: $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2007 osCommerce

  Released under the GNU General Public License
*/

// define the database table names used in the project
  define('TABLE_SESSIONS', 'sessions');
  define('TABLE_CONFIGURATIONS', 'configurations');
  define('TABLE_USERS', 'users');
  define('TABLE_SETTINGS', 'settings');
  define('TABLE_LISTS', 'lists');
  define('TABLE_ADDRESS', 'address');
  define('TABLE_DISCOUNT', 'discount');
  define('TABLE_ABOUT', 'about');
  
  define('TABLE_COUNTRIES', 'countries');
  define('TABLE_CITIES', 'cities');
  define('TABLE_CITIES_EXTENDED', 'cities_extended');
  
  define('TABLE_STORES', 'stores');
  define('TABLE_PRODUCTS', 'products');
  define('TABLE_IMAGES', 'images');
  define('TABLE_FILES', 'files');
  define('TABLE_PRODUCT_IMAGES', 'product_images');
  define('TABLE_STORECARDS', 'store_cards');
  
  define('TABLE_DEVICES', 'devices');
  
?>
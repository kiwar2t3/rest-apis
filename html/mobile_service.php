<?php

require('includes/application_top.php');

$action = tep_get_value_post("action");

$service = null;

// registration
if ($action == 'registration') {
  require('mobile_service/registration.php');

  $service = new Registration();
}


// login
if ($action == 'login') {
  require('mobile_service/login.php');

  $service = new Login();
}

// forgot password
if ($action == 'forgotpassword') {
  require('mobile_service/forgotpassword.php');

  $service = new ForgotPassword();
}

// reset password
if ($action == 'resetpassword') {
  require('mobile_service/resetpassword.php');

  $service = new ResetPassword();
}

// insert profile
if ( $action == 'insertprofile' ) {
	require('mobile_service/insertprofile.php');

	$service = new Registration();
}

// reset password
if ($action == 'changepassword') {
  require('mobile_service/changepassword.php');

  $service = new ChangePassword();
}

// view profile
if ($action == 'viewprofile') {
  require('mobile_service/viewprofile.php');

  $service = new EditProfile();
}
// update profile
if ($action == 'updateprofile') {

  require('mobile_service/updateprofile.php');

  $service = new EditProfile();
}

// delete profile
if ( $action == 'deleteprofile' ) {
	require('mobile_service/deleteprofile.php');

	$service = new EditProfile();
}

// insert list
if ( $action == 'insertlist' ) {
	require('mobile_service/insertlist.php');

	$service = new EditList();
}

// view list
if ( $action == 'viewlist' ) {
	require('mobile_service/viewlist.php');

	$service = new EditList();
}

// update list
if ( $action == 'updatelist' ) {
	require('mobile_service/updatelist.php');

	$service = new UpdateList();
}

// delete list
if ( $action == 'deletelist' ) {
	require('mobile_service/deletelist.php');

	$service = new DeleteList();
}

// insert discount
if ( $action == 'insertdiscount' ) {
	require('mobile_service/insertdiscount.php');

	$service = new InsertDiscount();
}

// update discount
if ( $action == 'insertdiscount' ) {
	require('mobile_service/updatediscount.php');

	$service = new UpdateDiscount();
}

// delete discount
if ( $action == 'deletediscount' ) {
	require('mobile_service/deletediscount.php');

	$service = new DeleteDiscount();
}

// insert store
if ( $action == 'insertstore' ) {
	require('mobile_service/insertstore.php');

	$service = new InsertStore();
}

// update store
if ( $action == 'updatestore' ) {
	require('mobile_service/updatestore.php');

	$service = new UpdateStore();
}

// delete store
if ( $action == 'deletestore' ) {
	require('mobile_service/deletestore.php');

	$service = new DeleteStore();
}

// insert store cards
if ( $action == 'insertstorecards' ) {
	require('mobile_service/insertstorecards.php');

	$service = new InsertStoreCards();
}

// update store card
if ( $action == 'updatestorecard' ) {
	require('mobile_service/updatestorecard.php');

	$service = new UpdateStoreCard();
}

// delete store card
if ( $action == 'deletestorecard' ) {
	require('mobile_service/deletestorecard.php');

	$service = new DeleteStoreCard();
}

// insert user setting
if ( $action == 'insertusersetting' ) {
	require('mobile_service/insertusersetting.php');

	$service = new InsertUserSetting();
}

// update user setting
if ( $action == 'updateusersetting' ) {
	require('mobile_service/updateusersetting.php');

	$service = new UpdateUserSetting();
}

// delete user setting
if ( $action == 'deleteusersetting' ) {
	require('mobile_service/deleteusersetting.php');

	$service = new DeleteUserSetting();
}

// facebook signup
if ($action == 'facebooksignup') {
  require('mobile_service/facebooksignup.php');

  $service = new FacebookSignUp();
}


// get countries
if ($action == 'getcountries') {
  require('mobile_service/getcountries.php');

  $service = new GetCountries();
}

// get cities
if ($action == 'getcities') {
  require('mobile_service/getcities.php');

  $service = new GetCities();
}

// register product
if ($action == 'registerproduct') {
	
  require('mobile_service/registerproduct.php');
  $service = new RegisterProduct();
}

// get products
if ($action == 'getsmallproducts') {
  require('mobile_service/getsmallproducts.php');
  $service = new GetProducts();
}

if ($action == 'getlargeproducts') {
  require('mobile_service/getlargeproducts.php');
  $service = new GetProducts();
}

// upload profile image
if ($action == 'uploadprofileimage') {
  require('mobile_service/uploadprofileimage.php');
  $service = new UploadProfileImage();
}

// download product image
if ($action == 'downloadprofileimage') {
  require('mobile_service/downloadprofileimage.php');
  $service = new DownloadProfileImage();
}

// upload product image
if ($action == 'uploadproductimage') {
  require('mobile_service/uploadproductimage.php');
  $service = new UploadProductImage();
}

// upload profile file
if ($action == 'uploadfileforprofile') {
  require('mobile_service/uploadfileforprofile.php');
  $service = new UploadFileForProfile();
}

// write feedback
if ($action == 'writefeedback') {
  require('mobile_service/writefeedback.php');

  $service = new WriteFeedback();
}

// get feedback
if ( $action == 'getfeedback' ) {
	require('mobile_service/getfeedback.php');

	$service = new GetFeedback();
}




// edit product
if ($action == 'editproduct') {
  require('mobile_service/editproduct.php');
  $service = new EditProduct();
}

// delete product
if ($action == 'deleteproduct') {
  require('mobile_service/deleteproduct.php');
  $service = new DeleteProduct();
}

// hot spot page
if ($action == 'hotspot') {
  require('mobile_service/hotspot.php');

  $service = new HotSpot();
}

/*
 * venue profile
 */
if ($action == 'venueprofile') {
  require('mobile_service/venueprofile.php');

  $service = new VenueProfile();
}

if ($action == 'venuefollowers') {
  require('mobile_service/venueprofile.php');

  $service = new VenueProfile();
}

/*
 * mingle
 */
if ($action == "mingle") {
  require('mobile_service/mingle.php');

  $service = new Mingle();
}

/*
 * user information
 */
// get my profile
if ($action == 'myprofile') {
  require('mobile_service/myprofile.php');

  $service = new MyProfile();
}

// get my friends
if ($action == 'myfriends') {
  require('mobile_service/myfriends.php');

  $service = new MyFriends();
}

// get my places
if ($action == 'myplaces') {
  require('mobile_service/myplaces.php');

  $service = new MyPlaces();
}


// upload avatar
if ($action == 'uploadavatar') {
  require('mobile_service/uploadavatar.php');

  $service = new UploadAvatar();
}

/*
 * prizes
 */
if ($action == 'prizes') {
  require('mobile_service/prizes.php');

  $service = new Prizes();
}


if ($service == null) {
  echo json_encode(array("status" => "error", "msg" => "This action doesn`t get executed."));
} else {
  $service->excute();

  echo $service->get_result();
}
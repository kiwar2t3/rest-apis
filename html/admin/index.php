<?php
require('../includes/admin_application_top.php');

$titlex = "Shrink Dashboard";
require(DIR_WS_INCLUDES . 'body_header.php');
?>


<div id="edit-users" class="home_menu_box">
<div class="menu_header">
<div id="icon-users" class="icon"></div>
<div class="title"><a href="users.php"><span style="font-size: 20px; line-height: 31px;">Users</span></a></div>
</div>
<div class="menu_body">
<div class="item">- <a href="users.php">Count: <b><?= teb_query("select count(*) as data_count from " . TABLE_USERS, "data_count")?></b></a></div>
<div class="item">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<a href="users.php?user_type=user">Normal user: <b><?= teb_query("select count(*) as data_count from " . TABLE_USERS . " where user_type='user'", "data_count")?></b></a>
</div>
<div class="item">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<a href="users.php?user_type=admin">Admin user: <b><?= teb_query("select count(*) as data_count from " . TABLE_USERS . " where user_type='admin'", "data_count")?></b></a>
</div>
<div class="item">- <a href="user_edit.php">Create New user</a></div>
</div>
<div id="edit-pages" class="home_menu_box">
<div class="menu_header">
<div id="icon-edit-pages" class="icon"></div>
<div class="title"><a href="categories.php"><span style="font-size: 20px; line-height: 31px;">Categories</span></a></div>
</div>
<div class="menu_body">
<div class="item">- <a href="categories.php">Total Count: <b><?= teb_query("select count(*) as data_count from " . TABLE_CATEGORIES, "data_count")?></b></a></div>
<div class="item">- <a href="category_edit.php">Create Category</a></div>
</div>
</div>
        <div id="edit-pages" class="home_menu_box">
<div class="menu_header">
<div id="icon-edit-pages" class="icon"></div>
<div class="title"><a href="brands.php"><span style="font-size: 20px; line-height: 31px;">Brands</span></a></div>
</div>
<div class="menu_body">
<div class="item">- <a href="brands.php">Total Count: <b><?= teb_query("select count(*) as data_count from " . TABLE_BRANDS, "data_count")?></b></a></div>
<div class="item">- <a href="brand_edit.php">Create Brand</a></div>
</div>
</div>
        <div id="edit-pages" class="home_menu_box">
<div class="menu_header">
<div id="icon-edit-pages" class="icon"></div>
<div class="title"><a href="stores.php"><span style="font-size: 20px; line-height: 31px;">Stores</span></a></div>
</div>
<div class="menu_body">
<div class="item">- <a href="stores.php">Total Count: <b><?= teb_query("select count(*) as data_count from " . TABLE_STORES, "data_count")?></b></a></div>
<div class="item">- <a href="store_edit.php">Create Store</a></div>
</div>
</div>
<div id="edit-pages" class="home_menu_box">
<div class="menu_header">
<div id="icon-edit-pages" class="icon"></div>
<div class="title"><a href="products.php"><span style="font-size: 20px; line-height: 31px;">Products</span></a></div>
</div>
<div class="menu_body">
<div class="item">- <a href="products.php">Total Count: <b><?= teb_query("select count(*) as data_count from " . TABLE_PRODUCTS, "data_count")?></b></a></div>
<div class="item">- <a href="product_edit.php">Create Product</a></div>
</div>
</div>
        <div id="edit-pages" class="home_menu_box">
<div class="menu_header">
<div id="icon-edit-pages" class="icon"></div>
<div class="title"><a href="deals.php"><span style="font-size: 20px; line-height: 31px;">Deals</span></a></div>
</div>
<div class="menu_body">
<div class="item">- <a href="deals.php">Total Count: <b><?= teb_query("select count(*) as data_count from " . TABLE_DEALS, "data_count")?></b></a></div>
<div class="item">- <a href="deal_edit.php">Create Deal</a></div>
</div>
</div>
        <div id="edit-pages" class="home_menu_box">
<div class="menu_header">
<div id="icon-edit-pages" class="icon"></div>
<div class="title"><a href="reviews.php"><span style="font-size: 20px; line-height: 31px;">Product Reviews</span></a></div>
</div>
<div class="menu_body">
<div class="item">- <a href="reviews.php">Total Count: <b><?= teb_query("select count(*) as data_count from " . TABLE_REVIEWS, "data_count")?></b></a></div>
<div class="item">- <a href="review_edit.php">Create Product Review</a></div>
</div>
</div>
<div id="edit-pages" class="home_menu_box">
<div class="menu_header">
<div id="icon-edit-pages" class="icon"></div>
<div class="title"><a href="shoppings.php"><span style="font-size: 20px; line-height: 31px;">Shopping Lists</span></a></div>
</div>
<div class="menu_body">
<div class="item">- <a href="shoppings.php">Total Count: <b><?= teb_query("select count(*) as data_count from " . TABLE_SHOPPINGS, "data_count")?></b></a></div>
<div class="item">- <a href="shopping_edit.php">Create Shopping Item</a></div>
</div>
</div>
        <div id="edit-pages" class="home_menu_box">
<div class="menu_header">
<div id="icon-edit-pages" class="icon"></div>
<div class="title"><a href="favorite.php"><span style="font-size: 20px; line-height: 31px;">Favorite Products</span></a></div>
</div>
<div class="menu_body">
<div class="item">- <a href="favorites.php">Total Count: <b><?= teb_query("select count(*) as data_count from " . TABLE_FAVORITES, "data_count")?></b></a></div>
<div class="item">- <a href="favorite_edit.php">Create Favorite Product</a></div>
</div>
</div>
        <div id="edit-pages" class="home_menu_box">
<div class="menu_header">
<div id="icon-edit-pages" class="icon"></div>
<div class="title"><a href="favoritestores.php"><span style="font-size: 20px; line-height: 31px;">Favorite Stores</span></a></div>
</div>
<div class="menu_body">
<div class="item">- <a href="favoritestores.php">Total Count: <b><?= teb_query("select count(*) as data_count from " . TABLE_FAVORITESTORES, "data_count")?></b></a></div>
<div class="item">- <a href="favoritestore_edit.php">Create Favorite Store</a></div>
</div>
</div>
</div>


<div class="clearboth"></div>

<div id="edit-settings" class="home_menu_box">
<div class="menu_header">
<div id="icon-options-general" class="icon"></div>
<div class="title"><a href="feedbacks.php"><span style="font-size: 20px; line-height: 31px;">Feedbacks</span></a></div>
</div>
</div>

<div id="edit-settings" class="home_menu_box">
<div class="menu_header">
<div id="icon-options-general" class="icon"></div>
<div class="title"><a href="configurations.php"><span style="font-size: 20px; line-height: 31px;">System Settings</span></a></div>
</div>
<div class="menu_body">
<div class="item">- <a href="change_password.php" >Change Password</a></div>
</div>
</div>

<?php require(DIR_WS_INCLUDES . 'body_footer.php'); ?>
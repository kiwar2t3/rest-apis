<?php
require('../includes/admin_application_top.php');

$titlex = "Products";

require(DIR_WS_INCLUDES . 'body_header.php');

$action = tep_db_prepare_input($_REQUEST['action']);
if ($action == 'delete') {
$id = tep_get_value_get('id');

teb_delete_query(TABLE_PRODUCTS, array("id"=>$id));
teb_delete_query(TABLE_PRODUCT_IMAGES, array("product_id"=>$id));

tep_success_redirect("Successfully deleted product.", "products.php");
} elseif ($action == 'all_delete') {
$product_ids = tep_get_value_post("product_ids");

for ($i = 0; $i < count($product_ids); $i ++) {
teb_delete_query(TABLE_PRODUCTS, array("id"=>$product_ids[$i]));
teb_delete_query(TABLE_PRODUCT_IMAGES, array("product_id"=>$product_ids[$i]));
}

tep_success_redirect("Successfully deleted products.", "products.php");
}

$s_key = tep_db_prepare_input($_REQUEST['s_key']);
$s_category = tep_db_prepare_input($_REQUEST['s_category']);
$s_brand = tep_db_prepare_input($_REQUEST['s_brand']);
//$s_store = tep_db_prepare_input($_REQUEST['s_store']);

?>

<?php if ($errors['db'] != ""): ?>
<p class="error"><?= $errors['db']?></p>
<?php endif; ?>

<form class="search_form" name="search_form" method="post">
<div>
Category: <select name="s_category" onchange="this.form.submit()" style="width: 150px;">
<option value="">-- All --</option>
<?php $categories = tep_db_query("select * from ".TABLE_CATEGORIES." order by title"); while($category = tep_db_fetch_array($categories)):?>
<option value="<?= $category['category_id']?>" <?php if ($s_category == $category['category_id']) echo "selected"?>><?= $category['title']?></option>
<?php endwhile;?>
</select>&nbsp;&nbsp;&nbsp;
                Brand: <select name="s_brand" onchange="this.form.submit()" style="width: 150px;">
<option value="">-- All --</option>
<?php $brands = tep_db_query("select * from ".TABLE_BRANDS." order by title"); while($brand = tep_db_fetch_array($brands)):?>
<option value="<?= $brand['brand_id']?>" <?php if ($s_brand == $brand['brand_id']) echo "selected"?>><?= $brand['title']?></option>
<?php endwhile;?>
</select>&nbsp;&nbsp;&nbsp;
<!--Store: <select name="s_store" onchange="this.form.submit()" style="width: 150px;">
<option value="">-- All --</option>
<?php $stores = tep_db_query("select * from ".TABLE_STORES.""); while($store = tep_db_fetch_array($stores)):?>
<option value="<?= $store['store_id']?>" <?php if ($s_store == $store['store_id']) echo "selected"?>><?= $store['title'].' - '.$store['location_name']?></option>
<?php endwhile;?>
</select>&nbsp;&nbsp;&nbsp-->

Search: <input type="text" name="s_key" value="<?= $s_key?>" />
<input type="submit" value="Search" />&nbsp;&nbsp;&nbsp;
                <p>
<input type="button" value="Add product" onclick="location.href='product_edit.php'"/>
</p>
</div>
</form>

<script type="text/javascript">
<!--
function delete_new(id, title) {
if (confirm("Are you sure want to delete "+title+"?")) {
location.href = "products.php?action=delete&id=" + id;
}
}

function all_action() {
if (confirm("Are you sure want to process?")) {
document.dataListForm.submit();
}
}
//-->
</script>

<form name="dataListForm" method="post" action="products.php" style="margin-top: 15px;">
<input type="hidden" name="s_category" value="<?= $s_category?>">
        <input type="hidden" name="s_brand" value="<?= $s_brand?>">
        <!--<input type="hidden" name="s_store" value="<?= $s_store?>">-->
<input type="hidden" name="s_key" value="<?= $s_key?>">

With Selected Products: <select name="action" onchange="all_action()">
<option value="">---</option>
<option value="all_delete">Delete</option>
</select>

<table class="contents_list" cellpadding="0" cellspacing="1">
<?php 
$sort_column = "id";
$sort_order = "asc";
if (isset($_REQUEST['sort_column']))$sort_column = tep_db_prepare_input($_REQUEST['sort_column']);
if (isset($_REQUEST['sort_order']))$sort_order = tep_db_prepare_input($_REQUEST['sort_order']);

$table_headers = array();
$table_headers[] = array('id'=>'', 'title'=>'<input type="checkbox" onchange="all_checkbox($(this))" />', 'width'=>'50');
$table_headers[] = array('id'=>'id', 'title'=>'ID', 'width'=>'100');
$table_headers[] = array('id'=>'', 'title'=>'Category', 'width'=>'200');
        $table_headers[] = array('id'=>'', 'title'=>'Brand', 'width'=>'200');
//$table_headers[] = array('id'=>'', 'title'=>'Store', 'width'=>'200');
$table_headers[] = array('id'=>'title', 'title'=>'Title', 'width'=>'');
        $table_headers[] = array('id'=>'comment', 'title'=>'Description', 'width'=>'');
        $table_headers[] = array('id'=>'upccode', 'title'=>'Upc Code', 'width'=>'100');
        $table_headers[] = array('id'=>'size', 'title'=>'Size', 'width'=>'100');
        $table_headers[] = array('id'=>'units_in_package', 'title'=>'Units in Package', 'width'=>'100');
        $table_headers[] = array('id'=>'origin', 'title'=>'Origin', 'width'=>'100');
$table_headers[] = array('id'=>'', 'title'=>'Images', 'width'=>'230');
//$table_headers[] = array('id'=>'price', 'title'=>'Price', 'width'=>'150');
        //$table_headers[] = array('id'=>'featured', 'title'=>'Featured', 'width'=>'80');
$table_headers[] = array('id'=>'created', 'title'=>'Created at', 'width'=>'150');
$table_headers[] = array('id'=>'', 'title'=>'Action', 'width'=>'200');

$column_count = count($table_headers);

include DIR_WS_BOX.'table_header.php';
?>

<?
$sql = "select * from " . TABLE_PRODUCTS. " where 1=1";
if ($s_key != '') {
$sql.= " and LOWER(title) like '%".strtolower($s_key)."%'";
}

if ($s_category != '') {
$sql.= " and category_id = '".$s_category."'";
}
        
        if ($s_brand != '') {
$sql.= " and brand_id = '".$s_brand."'";
}

//if ($s_store != '') {
//$sql.= " and store_id = '".$s_store."'";
//}
$sql .= " order by ".$sort_column." ".$sort_order;

$list_split = new splitPageResults($sql);
$product_list = tep_db_query($list_split->sql_query);
 
$row = 0;
while ($product = tep_db_fetch_array($product_list)) {
$row ++;

$ext_params = "&id=".$product['id']."&s_category=".$s_category."&s_brand=".$s_brand."&s_key=".$s_key."&sort_column=".$sort_column."&sort_order=".$sort_order."&page=".$page;

$product_images = tep_db_query('SELECT * FROM '.TABLE_PRODUCT_IMAGES.' where product_id='.$product['id']. ' order by image_index asc');

?>
<tbody>   
<tr class='dataTableRow'>
<td align="center">
<input type="checkbox" name="product_ids[]" value="<?= $product['id']?>" class="all_check" />
</td>
<td align="center">
<a class="link" href="product_edit.php?id=<?= $product['id']?>" title="View Detail"><?=$product['id']?></a>
</td>
<td align="center">
<?php if ($product['category_id'] > 0) :?>
<a class="link" href="category_edit.php?id=<?= $product['category_id']?>" title="View Detail">
<?= teb_query("select title from ".TABLE_CATEGORIES." where `category_id`='".$product['category_id']."'", "title")?>
</a>
<?php endif;?>
</td>
                <td align="center">
<?php if ($product['brand_id'] > 0) :?>
<a class="link" href="brand_edit.php?id=<?= $product['brand_id']?>" title="View Detail">
<?= teb_query("select title from ".TABLE_BRANDS." where `brand_id`='".$product['brand_id']."'", "title")?>
</a>
<?php endif;?>
</td>
<!--<td align="center">
<?php if ($product['store_id'] > 0) :?>
<a class="link" href="store_edit.php?id=<?= $product['store_id']?>" title="View Detail">
<?= teb_query("select title from ".TABLE_STORES." where `store_id`=".$product['store_id'], "title")?>
                                -
                                <?= teb_query("select location_name from ".TABLE_STORES." where `store_id`=".$product['store_id'], "location_name")?>
</a>
<?php endif;?>
</td>-->
<td align="center">
<a class="link" href="product_edit.php?id=<?= $product['id']?>" title="View Detail"><?=$product['title']?></a>
</td>
                <td align="center">
<?=$product['comment']?>
</td>
                <td align="center">
<?=$product['upccode']?>
</td>
                <td align="center">
<?=$product['size']?>
</td>
                <td align="center">
<?=$product['units_in_package']?>
</td>
                <td align="center">
<?=$product['origin']?>
</td>
<td align="center">
<?php
while ($pimage = tep_db_fetch_array($product_images)) {
?>
<img src='<?=getUploadFileAbsolutePath($pimage['image_thumb'])?>' width="50" height="50" />
<?php
}
?>
</td>
<!--<td align="center">
<?=$product['price']?>
</td>-->
                <!--<td align="center">
<?=$product['featured']?>
</td>-->
<td align="center">
<?=$product['created']?>
</td>
<td align="center">
        <a class="button" href="product_edit.php?id=<?= $product['id']?>" title="Edit">Edit</a>
        <a class="button" href="javascript:delete_new(<?= $product['id']?>, '<?=$product['title']?>')" title="Edit">Delete</a>        
        </td>
</tr>
<?php
}
?>
</tbody>
<?php 
$data_message = TEXT_DISPLAY_NUMBER_OF_PRODUCTS;
$empty_message = "No Products";
include DIR_WS_BOX.'table_footer.php';
?>
</table>

</form>

<?php require(DIR_WS_INCLUDES . 'body_footer.php'); ?>
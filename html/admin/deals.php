<?php
require('../includes/admin_application_top.php');

$titlex = "Deals";

require(DIR_WS_INCLUDES . 'body_header.php');

$action = tep_db_prepare_input($_REQUEST['action']);
if ($action == 'delete') {
$id = tep_get_value_get('id');
teb_delete_query(TABLE_DEALS, array("deal_id"=>$id));
tep_success_redirect("Successfully deleted deal.", "deals.php");
} elseif ($action == 'all_delete') {
$deal_ids = tep_get_value_post("deal_ids");
for ($i = 0; $i < count($deal_ids); $i ++) {
teb_delete_query(TABLE_DEALS, array("deal_id"=>$deal_ids[$i]));
}
tep_success_redirect("Successfully deleted deals.", "deals.php");
}

$s_product = tep_db_prepare_input($_REQUEST['s_product']);
$s_store = tep_db_prepare_input($_REQUEST['s_store']);

?>

<?php if ($errors['db'] != ""): ?>
<p class="error"><?= $errors['db']?></p>
<?php endif; ?>

<form class="search_form" name="search_form" method="post">
<div>
Product: <select name="s_product" onchange="this.form.submit()" style="width: 150px;">
<option value="">-- All --</option>
<?php $products = tep_db_query("select * from ".TABLE_PRODUCTS." order by title"); while($product = tep_db_fetch_array($products)):?>
<option value="<?= $product['id']?>" <?php if ($s_product == $product['id']) echo "selected"?>><?= $product['title']?></option>
<?php endwhile;?>
</select>&nbsp;&nbsp;&nbsp;
Store: <select name="s_store" onchange="this.form.submit()" style="width: 150px;">
<option value="">-- All --</option>
<?php $stores = tep_db_query("select * from ".TABLE_STORES.""); while($store = tep_db_fetch_array($stores)):?>
<option value="<?= $store['store_id']?>" <?php if ($s_store == $store['store_id']) echo "selected"?>><?= $store['title'].' - '.$store['location_name']?></option>
<?php endwhile;?>
                </select>
                <p>
<input type="button" value="Add Deal" onclick="location.href='deal_edit.php'"/>
</p>
</div>
</form>

<script type="text/javascript">
<!--
function delete_new(id, title) {
if (confirm("Are you sure want to delete "+title+"?")) {
location.href = "deals.php?action=delete&id=" + id;
}
}

function all_action() {
if (confirm("Are you sure want to process?")) {
document.dataListForm.submit();
}
}
//-->
</script>

<form name="dataListForm" method="post" action="deals.php" style="margin-top: 15px;">
<input type="hidden" name="s_product" value="<?= $s_product?>">
        <input type="hidden" name="s_store" value="<?= $s_store?>">

With Selected Deals: <select name="action" onchange="all_action()">
<option value="">---</option>
<option value="all_delete">Delete</option>
</select>

<table class="contents_list" cellpadding="0" cellspacing="1">
<?php 
$sort_column = "deal_id";
$sort_order = "asc";
if (isset($_REQUEST['sort_column']))$sort_column = tep_db_prepare_input($_REQUEST['sort_column']);
if (isset($_REQUEST['sort_order']))$sort_order = tep_db_prepare_input($_REQUEST['sort_order']);

$table_headers = array();
$table_headers[] = array('id'=>'', 'title'=>'<input type="checkbox" onchange="all_checkbox($(this))" />', 'width'=>'50');
$table_headers[] = array('id'=>'deal_id', 'title'=>'ID', 'width'=>'100');
$table_headers[] = array('id'=>'product', 'title'=>'Product', 'width'=>'200');
$table_headers[] = array('id'=>'store', 'title'=>'Store', 'width'=>'200');
$table_headers[] = array('id'=>'regular_price', 'title'=>'Regular Price', 'width'=>'100');
        $table_headers[] = array('id'=>'sale_price', 'title'=>'Sale Price', 'width'=>'100');
        $table_headers[] = array('id'=>'savings', 'title'=>'Savings', 'width'=>'100');
        $table_headers[] = array('id'=>'startdate', 'title'=>'Start Date', 'width'=>'100');
        $table_headers[] = array('id'=>'enddate', 'title'=>'End Date', 'width'=>'100');
        $table_headers[] = array('id'=>'featured', 'title'=>'Featured', 'width'=>'100');
        $table_headers[] = array('id'=>'card_required', 'title'=>'Card Required', 'width'=>'100');
$table_headers[] = array('id'=>'created', 'title'=>'Created at', 'width'=>'150');
$table_headers[] = array('id'=>'', 'title'=>'Action', 'width'=>'200');

$column_count = count($table_headers);

include DIR_WS_BOX.'table_header.php';
?>

<?
$sql = "select * from " . TABLE_DEALS. " where 1=1";

if ($s_product != '') {
$sql.= " and product_id = '".$s_product."'";
}
        
if ($s_store != '') {
$sql.= " and store_id = '".$s_store."'";
}
$sql .= " order by ".$sort_column." ".$sort_order;

$list_split = new splitPageResults($sql);
$deal_list = tep_db_query($list_split->sql_query);
 
$row = 0;
while ($deal = tep_db_fetch_array($deal_list)) {
$row ++;

$ext_params = "&deal_id=".$deal['deal_id']."&s_category=".$s_category."&s_brand=".$s_brand."&s_store=".$s_store."&sort_column=".$sort_column."&sort_order=".$sort_order."&page=".$page;

?>
<tbody>   
<tr class='dataTableRow'>
<td align="center">
<input type="checkbox" name="deal_ids[]" value="<?= $deal['deal_id']?>" class="all_check" />
</td>
<td align="center">
<a class="link" href="deal_edit.php?id=<?= $deal['deal_id']?>" title="View Detail"><?=$deal['deal_id']?></a>
</td>
<td align="center">
<?php if ($deal['product_id'] > 0) :?>
<a class="link" href="product_edit.php?id=<?= $deal['product_id']?>" title="View Detail">
<?= teb_query("select title from ".TABLE_PRODUCTS." where `id`='".$deal['product_id']."'", "title")?>
</a>
<?php endif;?>
</td>
<td align="center">
<?php if ($deal['store_id'] > 0) :?>
<a class="link" href="store_edit.php?id=<?= $deal['store_id']?>" title="View Detail">
<?= teb_query("select title from ".TABLE_STORES." where `store_id`=".$deal['store_id'], "title")?>
                                -
                                <?= teb_query("select location_name from ".TABLE_STORES." where `store_id`=".$deal['store_id'], "location_name")?>
</a>
<?php endif;?>
</td>
                <td align="center">
<?=$deal['regular_price']?>
</td>
                <td align="center">
<?=$deal['sale_price']?>
</td>
<td align="center">
<?=$deal['savings']?>
</td>
                <td align="center">
<?=$deal['start_date']?>
</td>
                <td align="center">
<?=$deal['end_date']?>
</td>
                <td align="center">
<?=$deal['featured']?>
</td>
                <td align="center">
<?=$deal['card_required']?>
</td>
                <td align="center">
<?=$deal['created']?>
</td>
<td align="center">
                    <a class="button" href="deal_edit.php?id=<?= $deal['deal_id']?>" title="Edit">Edit</a>
                    <a class="button" href="javascript:delete_new(<?= $deal['deal_id']?>, '<?=$deal['title']?>')" title="Edit">Delete</a>        
                </td>
</tr>
<?php
}
?>
</tbody>
<?php 
$data_message = TEXT_DISPLAY_NUMBER_OF_PRODUCTS;
$empty_message = "No Deals";
include DIR_WS_BOX.'table_footer.php';
?>
</table>

</form>

<?php require(DIR_WS_INCLUDES . 'body_footer.php'); ?>
<?php
require('../includes/admin_application_top.php');

$titlex = "Stores";

require(DIR_WS_INCLUDES . 'body_header.php');

$action = tep_db_prepare_input($_REQUEST['action']);
if ($action == 'delete') {
$id = tep_get_value_get('id');
tep_db_query("update ".TABLE_STORES." set deleted='Y' where `store_id`='".$id."'");
tep_success_redirect("Successfully Deleted Store.", "stores.php");
} elseif ($action == 'actived') {
$id = tep_get_value_get('id');
$actived = tep_get_value_get('actived');
tep_db_query("update ".TABLE_STORES." set actived='".$actived."' where `store_id`='".$id."'");
} elseif ($action == 'all_actived') {
$store_ids = tep_get_value_post("store_ids");
for ($i = 0; $i < count($store_ids); $i ++) {
tep_db_query("update ".TABLE_STORES." set actived='Y' where `store_id`='".$store_ids[$i]."'");
}
} elseif ($action == 'all_disabled') {
$store_ids = tep_get_value_post("store_ids");
for ($i = 0; $i < count($store_ids); $i ++) {
tep_db_query("update ".TABLE_STORES." set actived='N' where `store_id`='".$store_ids[$i]."'");
}
} elseif ($action == 'all_delete') {
$store_ids = tep_get_value_post("store_ids");
for ($i = 0; $i < count($store_ids); $i ++) {
tep_db_query("update ".TABLE_STORES." set deleted='Y' where `store_id`='".$store_ids[$i]."'");
}
tep_success_redirect("Successfully Deleted stores.", "stores.php");
}

$s_key = tep_db_prepare_input($_REQUEST['s_key']);
$s_active = 'Y';//tep_db_prepare_input($_REQUEST['s_active']);
?>

<?php if ($errors['db'] != ""): ?>
<p class="error"><?= $errors['db']?></p>
<?php endif; ?>

<form class="search_form" name="search_form" method="post">
<div>
<!--Active: <select name="s_active" onchange="this.form.submit()">
<option value="">-- All --</option>
<option value="Y" <?php if ($s_active == 'Y') echo "selected"?>>Active</option>
<option value="N" <?php if ($s_active == 'N') echo "selected"?>>InActive</option>
</select>&nbsp;&nbsp;&nbsp;-->
Search: <input type="text" name="s_key" value="<?= $s_key?>" />
<input type="submit" value="Search" />&nbsp;&nbsp;&nbsp;
                <p>
                    <input type="button" value="Add Store" onclick="location.href='store_edit.php'"/>
                </p>
</div>
</form>

<script type="text/javascript">
<!--
function delete_new(id, title) {
if (confirm("Are you sure to delete "+title+"?")) {
location.href = "stores.php?action=delete&id=" + id;
}
}

function all_action() {
if (confirm("Are you sure to process?")) {
document.dataListForm.submit();
}
}
//-->
</script>

<form name="dataListForm" method="post" action="stores.php" style="margin-top: 15px;">
<input type="hidden" name="s_active" value="<?= $s_active?>">
<input type="hidden" name="s_key" value="<?= $s_key?>">

With selected: <select name="action" onchange="all_action()">
<option value="">---</option>
<!--<option value="all_actived">Activate</option>-->
<!--<option value="all_disabled">Deactivate</option>-->
<option value="all_delete">Delete</option>
</select>

<table class="contents_list" cellpadding="0" cellspacing="1">
<?php 
$sort_column = "title";
$sort_order = "ASC";
if (isset($_REQUEST['sort_column']))$sort_column = tep_db_prepare_input($_REQUEST['sort_column']);
if (isset($_REQUEST['sort_order']))$sort_order = tep_db_prepare_input($_REQUEST['sort_order']);

$table_headers = array();
$table_headers[] = array('id'=>'', 'title'=>'<input type="checkbox" onchange="all_checkbox($(this))" />', 'width'=>'50');
$table_headers[] = array('id'=>'store_id', 'title'=>'ID', 'width'=>'50');
        $table_headers[] = array('id'=>'logo', 'title'=>'Logo', 'width'=>'60');
$table_headers[] = array('id'=>'title', 'title'=>'Store Name', 'width'=>'150');
        $table_headers[] = array('id'=>'location_name', 'title'=>'Location Name', 'width'=>'150');
        $table_headers[] = array('id'=>'photo', 'title'=>'Photo', 'width'=>'');
        $table_headers[] = array('id'=>'title', 'title'=>'Phone', 'width'=>'100');
        $table_headers[] = array('id'=>'email', 'title'=>'EMail', 'width'=>'100');
        $table_headers[] = array('id'=>'website', 'title'=>'Websie', 'width'=>'100');        
        $table_headers[] = array('id'=>'address', 'title'=>'Address', 'width'=>'200');
        $table_headers[] = array('id'=>'neighborhood', 'title'=>'Neighborhood', 'width'=>'100');
        $table_headers[] = array('id'=>'coordinate', 'title'=>'Coordinate', 'width'=>'100');
        $table_headers[] = array('id'=>'store_hours', 'title'=>'Hours', 'width'=>'100');
        $table_headers[] = array('id'=>'loyalty_card', 'title'=>'Loyalty Card', 'width'=>'50');
$table_headers[] = array('id'=>'created', 'title'=>'Created at', 'width'=>'100');
//$table_headers[] = array('id'=>'actived', 'title'=>'Active', 'width'=>'100');
$table_headers[] = array('id'=>'', 'title'=>'Action', 'width'=>'100');

$column_count = count($table_headers);

include DIR_WS_BOX.'table_header.php';
?>

<?
$sql = "select * from " . TABLE_STORES. " where deleted<>'Y'";
if ($s_key != '') {
$sql.= " and LOWER(title) like '%".strtolower($s_key)."%'";
}
if ($s_active != '') {
$sql.= " and actived = '".$s_active."'";
}
$sql .= " order by ".$sort_column." ".$sort_order;
$list_split = new splitPageResults($sql);
$stores = tep_db_query($list_split->sql_query);
        
$row = 0;
while ($store = tep_db_fetch_array($stores)) {
$row ++;
$ext_params = "&id=".$store['store_id']."&s_active=".$s_active."&s_key=".$s_key."&sort_column=".$sort_column."&sourt_order=".$sort_order."&page=".$page;
?>
<tbody>   
<tr class='dataTableRow'>
<td align="center">
<input type="checkbox" name="store_ids[]" value="<?= $store['store_id']?>" class="all_check" />
</td>
<td align="center">
<a class="link" href="store_edit.php?id=<?= $store['store_id']?>" title="View Detail"><?=$store['store_id']?></a>
</td>
                <td align="center">
                    <?php if ($store['logo'] != ''):?>
<img src="<?=getUploadFileAbsolutePath($store['logo'])?>" width="50"/>
                    <?php endif; ?>
</td>
                <td align="left">
<a class="link" href="store_edit.php?id=<?= $store['store_id']?>" title="View Detail"><?=$store['title']?></a>
</td>
                <td align="left">
<a class="link" href="store_edit.php?id=<?= $store['store_id']?>" title="View Detail"><?=$store['location_name']?></a>
</td>
<td align="center">
<img src="<?=getUploadFileAbsolutePath($store['image_thumb'])?>" width="50"/>
</td>
                <td align="center"><?=$store['phone']?></td>
                <td align="center"><?=$store['email']?></td>
                <td align="center"><?=$store['website']?></td>                
                <td align="center"><?=$store['address_street'].' '.$store['address_city'].' '.$store['address_state'].' '.$store['address_country'].' '.$store['address_zip']?></td>
                <td align="center"><?=$store['neighborhood']?></td>
                <td align="center"><?=$store['latitude'].' '.$store['longitude']?></td>
                <td align="center"><?=$store['store_hours']?></td>
                <td align="center"><?=$store['loyalty_card']?></td>
<td align="center"><?=$store['created']?></td>                
<!--<td align="center"><?=$store['actived']?></td>-->
<td align="center">
                    <a class="button" href="store_edit.php?id=<?= $store['store_id']?>" title="Edit">Edit</a>
                    <a class="button" href="javascript:delete_new(<?= $store['store_id']?>, '<?=$store['title']?>')" title="Edit">Delete</a>
                    <!--<?php if ($store['actived'] == 'Y') : ?>
                            <a class="button" href="stores.php?action=actived&actived=N<?= $ext_params?>" title="Edit">DeActivate</a>
                    <?php else : ?>
                            <a class="button" href="stores.php?action=actived&actived=Y<?= $ext_params?>" title="Edit">Activate</a>
                    <?php endif;?>-->
                </td>
</tr>
<?php
}
?>
</tbody>
<?php 
$data_message = TEXT_DISPLAY_NUMBER_OF_STORES;
$empty_message = "No stores";
include DIR_WS_BOX.'table_footer.php';
?>
</table>

</form>

<?php require(DIR_WS_INCLUDES . 'body_footer.php'); ?>
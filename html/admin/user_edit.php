<?php
require_once('../includes/admin_application_top.php');

$titlex = "New User";

$user_id = 0;
if (isset($_GET['user_id'])) {
include 'user_submenu.php';
} else {
require(DIR_WS_INCLUDES . 'body_header.php');
}

$user_email = "";
$user_password = "";

$user_fullname = "";
$user_address_zip = "";

$user_sex = "";
$user_bithday = "";

$user_created = "";
$user_modified = "";

$user_avatar = "";

if (isset($_POST[user_email]))
{
$user_email = tep_get_value_post('user_email', 'Email', 'require;');
if ($user_id == 0) {
$user_password = tep_get_value_post('user_password', 'Password', 'require;');
}
$user_fullname = tep_get_value_post('user_fullname', 'Name', 'require;');
$user_address_zip = tep_get_value_post("user_address_zip");
        $user_sex = tep_get_value_post("user_sex");
        $user_birthday = tep_get_value_post("user_birthday");

tep_unique_check(TABLE_USERS, array('user_email'=>$user_email), "user_id!='".$user_id."'", 'user_email', 'Email');

        if ($message_cls->is_empty_error()) {
$user = array(
'user_email'=> $user_email,
                        'user_fullname'=> $user_fullname,
'user_address_zip' => $user_address_zip,
                        'user_sex' => $user_sex,
                        'user_birthday' => $user_birthday,
                        'user_modified'=> tep_now_datetime(),
);
if ($user_id == 0) {
$user['user_password']= tep_encrypt_password($user_password);
$user['user_created']= tep_now_datetime();
$result = tep_db_perform(TABLE_USERS, $user, 'insert');
if ($result > 0) {
$user_id = tep_db_insert_id();
$user_avatar = upload_avatar($user_id, $user_email, "user_avatar");
if ($user_avatar != '') {
tep_db_perform(TABLE_USERS, array("user_avatar"=>getUploadFileRelativePath($user_avatar)), "update", "user_id='".$user_id."'");
} else {
tep_db_perform(TABLE_USERS, array("user_avatar"=>($user_sex == 'Male' ? getUploadFileRelativePath(DEFAULT_MALE_AVATAR) : getUploadFileRelativePath(DEFAULT_FEMALE_AVATAR))), "update", "user_id='".$user_id."'");
}

//tep_success_redirect("Success new registed user!", "user_edit.php?user_id=".$user_id);
tep_success_redirect("Success new registed user!", "users.php");
}
} else {
$result = tep_db_perform(TABLE_USERS, $user, 'update', "user_id='".$user_id."'");

if ($result > 0) {                                
$user_avatar = upload_avatar($user_id, $user_email, "user_avatar");
if ($user_avatar != '') {
tep_db_perform(TABLE_USERS, array("user_avatar"=>getUploadFileRelativePath($user_avatar)), "update", "user_id='".$user_id."'");
}

//tep_success_redirect("Success saved user information!", "user_edit.php?user_id=".$user_id);
tep_success_redirect("Success new registed user!", "users.php");
}
}

$error_db = "Faild register user.";
}
} elseif($user_id > 0) {
$user_avatar = getUploadFileAbsolutePath($user_basic['user_avatar']);
$user_email = $user_basic['user_email'];
$user_password = $user_basic['user_password'];
$user_fullname = $user_basic['user_fullname'];
$user_address_zip = $user_basic['user_address_zip'];
        $user_sex = $user_basic['user_sex'];
        $user_birthday = $user_basic['user_birthday'];
$user_created = $user_basic['user_created'];
$user_modified = $user_basic['user_modified'];
$user_lastlogined = $user_basic['user_last_logined'];
}
?>

<script type="text/javascript">
<!--
$(function() {
$('.show_table').click(function() {
var table = $("#" + $(this).attr('for'));
if (table.hasClass('hidden')) {
table.removeClass('hidden');
$(this).html("Hidden");
} else {
table.addClass('hidden');
$(this).html("Show");
}
});

get_states('<?= $user_address_country?>', 'user_address_state', '<?= $user_address_state?>', '<?= $user_address_city?>');
get_cities('<?= $user_address_country?>', '<?= $user_address_state?>', 'user_address_city', '<?= $user_address_city?>');
//get_status('<?= $user_address_country?>', 'user_address_state', '<?= $user_address_state?>');
});

function save(){
$('span.show_table').html('Hidden');
$('#user_login').removeClass('hidden');
$('#user_basic').removeClass('hidden');
$('#user_address').removeClass('hidden');
$('#user_community').removeClass('hidden');
$('#user_others').removeClass('hidden');


if ($("#UserForm").validationEngine('validate')) {
document.user_form.submit();
}
}
//-->
</script>

<form name="user_form" encType="multipart/form-data" method="post" autocomplete="off" class="edit_form" id="UserForm">
<?php if (isset($error_db)): ?>
<p class="error"><?= $error_db?></p>
<?php endif; ?>

<div class="alert forward">* Required information</div>
<br />
<h3>
Login Informaion
<span style="float: right; font-size: 10px; font-weight: normal; cursor: pointer;" class="show_table" for="user_login">Hidden</span>
</h3>
<hr />
        <table class="contents_edit" id="user_login">    
<tr>
<td class="label">Email *</td>
<td class="edit">
<input type="text" name="user_email" id="user_email" value="<?= $user_email?>" style="width: 300px;" class="validate[required,custom[email]]" />
<?php $message_cls->show_error('user_email')?>
</td>
</tr>
<tr>
<td class="label">Password *</td>
<td class="edit">
<?php if ($user_id > 0) : ?>
******
<?php else : ?>
<input type="password" name="user_password" id="user_password" value="<?= $user_password?>" style="width: 300px;" class="validate[required]" />
<?php $message_cls->show_error('user_password')?>
<?php endif;?>
</td>
</tr>
</table>

<br/><br/>
<h3>
Basic Profile
<span style="float: right; font-size: 10px; font-weight: normal; cursor: pointer;" class="show_table" for="user_basic">Hidden</span>
</h3>
<hr />
    <table class="contents_edit" id="user_basic">
<tr>
<td class="label">Profile Photo</td>
<td class="edit">
<?php if ($user_avatar != '') : ?>
<img src="<?= $user_avatar?>" />
<?php endif;?>
<input type="file" name="user_avatar" id="user_avatar" style="width: 300px;" />
<?php $message_cls->show_error('user_avatar')?>
</td>
</tr>
    <tr>
                <td class="label">Name *</td>
<td class="edit">
<input type="text" name="user_fullname" id="user_fullname" value="<?= $user_fullname?>" style="width: 300px;" class="validate[required]" />
<?php $message_cls->show_error('user_firstname')?>
</td>
</tr>
                <tr>
<td class="label">Zip Code</td>
<td class="edit">
<input type="text" name="user_address_zip" id="user_address_zip" value="<?= $user_address_zip?>" style="width: 100px;" />
<?php $message_cls->show_error('user_address_zip')?>
</td>
</tr>
                <tr>
<td class="label">Sex</td>
<td class="edit">
<select name="user_sex" id="user_sex">
<option value="Male" <?php if ($user_sex=='Male') echo "selected"?>>Male</option>
<option value="Female" <?php if ($user_sex=='Female') echo "selected"?>>Female</option>
</select>
</td>
</tr>
<tr>
<td class="label">Birthday</td>
<td class="edit">
<input type="text" name="user_birthday" id="user_birthday" value="<?= $user_birthday?>" style="width: 100px;" class="input_birthday" />
<?php $message_cls->show_error('user_birthday')?>
</td>
</tr>
</table>

<br/><br/>
        
<h3>
Status Informaion
<span style="float: right; font-size: 10px; font-weight: normal; cursor: pointer;" class="show_table" for="user_others">Hidden</span>
</h3>
<hr />
<table class="contents_edit" id="user_others">    
    <tr>
<td class="label">Created</td>
<td class="edit">
<?= $user_created?>
</td>
</tr>
<tr>
<td class="label">Last Logged In</td>
<td class="edit">
<?= $user_lastlogined?>
</td>
</tr>
</table>
<table class="contents_edit">
<tr height="35px">
<td width="100px"/>
<td class="edit">
<input type="button" value="  Save " name="action" style="width:80px;" onclick="save()"/>
<input type="button" value="  Cancel " style="width:80px;" onclick="location.href='users.php'" />
</td>
</tr>
</table>
</form>

<?php require(DIR_WS_INCLUDES . 'body_footer.php'); ?>
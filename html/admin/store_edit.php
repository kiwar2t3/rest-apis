<?php
require('../includes/admin_application_top.php');

$titlex = "Create Store";

$id = 0;
if (isset($_GET['id'])) {
$titlex = "Store Edit";
$id = tep_get_value_get("id");
}

require(DIR_WS_INCLUDES . 'body_header.php');

$title= "";
$location_name  = "";
$logo           = "";
$description= "";
$image_original= "";
$image_thumb= "";

$phone  = "";
$email  = "";
$website = "";

$address_zip = "";
$address_country = "";
$address_state = "";
$address_city = "";
$address_street = "";
$neighborhood = "";
$latitude = 0;
$longitude = 0;

$store_hours = "";
$loyalty_card = "";
$created= "";
$actived= "Y";

if (isset($_POST["title"]))
{
$title= tep_get_value_post('title', 'Title', 'require;length[3,200];');
        $location_name  = tep_get_value_post('location_name', 'Location Name', '');
$description= tep_get_value_post('description', 'Description', '');

        $logo_mode     = tep_get_value_post('logo_mode', 'Logo Mode', 'require;');
        $logo_url      = tep_get_value_post('logo_url', 'Logo URL', '');        
        if($logo_mode=='upload'){
            $logo = upload_file($title.'_logo', 'logo', $id == 0, 100, 100);
        }else if($logo_mode=='url'){
            $logo = download_file($title.'_logo', $logo_url, $id == 0, 100, 100);
        }
        
$image_mode     = tep_get_value_post('image_mode', 'Image Mode', 'require;');
        $image_url      = tep_get_value_post('image_url', 'Image URL', '');        
        if($image_mode=='upload'){
            $image_original = upload_file($title, 'image', $id == 0);
        }else if($image_mode=='url'){
            $image_original = download_file($title, $image_url, $id == 0);
        }
if ($image_original != '' && $upload_img_path != '') {
$image_thumb= formated_image($image_original, $upload_img_path, 100, 100);
}
        
        $phone  = tep_get_value_post('phone', 'Phone', '');
        $email          = tep_get_value_post('email', 'EMail', 'email;');
        $website        = tep_get_value_post('website', 'Website','');
        
        $address_zip= tep_get_value_post('address_zip', 'Zipcode', '');
        $address_country= tep_get_value_post('address_country', 'Country', '');
        $address_state= tep_get_value_post('address_state', 'State', '');
        $address_city= tep_get_value_post('address_city', 'City', '');
        $address_street= tep_get_value_post('address_street', 'Street', '');
        $neighborhood   = tep_get_value_post('neighborhood', 'Neighborhood','');
        $latitude= tep_get_value_post('latitude', 'Street', '');
        $longitude      = tep_get_value_post('longitude', 'Street', '');
        
        $store_hours    = tep_get_value_post('store_hours', 'Store Hours', '');
        $loyalty_card   = tep_get_value_post('loyalty_card', 'Loyalty Card', '');

if ($message_cls->is_empty_error()) {
$store = array(
'title'=> $title,
                        'location_name'         => $location_name,
"description"           => $description,                    
                        "phone"                 => $phone,
                        "email"                 => $email,
                        "website"               => $website,
                        "address_zip"           => $address_zip,
                        "address_country"       => $address_country,
                        "address_state"         => $address_state,
                        "address_city"          => $address_city,
                        "address_street"        => $address_street,
                        "neighborhood"          => $neighborhood,
                        "longitude"             => $longitude,
                        "latitude"              => $latitude,
                        "store_hours"           => $store_hours,
                        "loyalty_card"          => $loyalty_card,
);
if ($id == 0) {
$store['created']= tep_now_datetime();
$store['actived']= $actived;
$store['image_original']= getUploadFileRelativePath($image_original);
$store['image_thumb']                   = getUploadFileRelativePath($image_thumb);
                        $store['logo']                          = getUploadFileRelativePath($logo);
$result = tep_db_perform(TABLE_STORES, $store, 'insert');
if ($result > 0) {
$id = tep_db_insert_id();
}
} else {
if ($image_original != '') {
$store['image_original']= getUploadFileRelativePath($image_original);
$store['image_thumb']                   = getUploadFileRelativePath($image_thumb);
}
                        if ($logo != '')
                            $store['logo']  = getUploadFileRelativePath ($logo);
$result = tep_db_perform(TABLE_STORES, $store, 'update', "store_id='".$id."'");
}
if ($result > 0) {
tep_success_redirect("Success saved store information!", "stores.php");
} else {
$error_db = "Faild register store.";
}
}
} elseif($id > 0) {
$store_info = teb_one_query(TABLE_STORES, array("store_id"=>$id));

$title= $store_info['title'];
        $location_name  = $store_info['location_name'];
        $logo   = getUploadFileAbsolutePath($store_info['logo']);
$description= $store_info['description'];        
$image_original= getUploadFileAbsolutePath($store_info['image_original']);
$image_thumb    = getUploadFileAbsolutePath($store_info['image_thumb']);
$created= $store_info['created'];
        
        $phone          = $store_info['phone'];
        $email          = $store_info['email'];
        $website        = $store_info['website'];        
        
        $address_zip    = $store_info['address_zip'];
        $address_country= $store_info['address_country'];
        $address_state  = $store_info['address_state'];
        $address_city   = $store_info['address_city'];
        $address_street = $store_info['address_street'];
        $neighborhood   = $store_info['neighborhood'];
        $longitude      = $store_info['longitude'];
        $latitude       = $store_info['latitude'];
        
        $store_hours    = $store_info['store_hours'];
        $loyalty_card   = $store_info['loyalty_card'];
}
?>

<form name="ad_form" encType="multipart/form-data" method="post" autocomplete="off" class="edit_form" id="ADForm">
<?php if (isset($error_db)): ?>
<p class="error"><?= $error_db?></p>
<?php endif; ?>
    <br/>
    <h3>
            Basic
    </h3>
    <hr />
    <table class="contents_edit" id="store_basic">
                <tr>
                        <td class="label" width="120px">Store Name *</td>
<td class="edit">
<input type="text" name="title" id="title" value="<?= $title?>" style="width: 400px;" class="validate[required,length[5-200]]" />
<?php $message_cls->show_error('title')?>
</td>
</tr>
                <tr>
                        <td class="label" width="120px">Location Name </td>
<td class="edit">
<input type="text" name="location_name" id="location_name" value="<?= $location_name?>" style="width: 400px;" class="" />
<?php $message_cls->show_error('location_name')?>
</td>
</tr>
                <tr>
<td class="label">Logo *</td>
<td class="edit">
<?php if ($logo != ''):?>
<a href="<?= $logo?>" target="_image"><img src="<?= $logo?>" /></a><br>
<?php endif;?>
                                <input type="radio" name="logo_mode" value="upload" checked onclick="document.getElementById('logo').style.display='block'; document.getElementById('logo_url').style.display='none';" />Upload
                                <input type="radio" name="logo_mode" value="url"  onclick="document.getElementById('logo').style.display='none'; document.getElementById('logo_url').style.display='block';" />URL<br>
<input type="file" name="logo" id="logo" style="width:400px;">
                                <input type="text" name="logo_url" id="logo_url" style="width:400px; " hidden>
<?php $message_cls->show_error('logo')?>
</td>
</tr>
<tr>
<td class="label">Photo *</td>
<td class="edit">
<?php if ($image_thumb != ''):?>
<a href="<?= $image_thumb?>" target="_image"><img src="<?= $image_thumb?>" /></a><br>
<?php endif;?>
                                <input type="radio" name="image_mode" value="upload" checked onclick="document.getElementById('image').style.display='block'; document.getElementById('image_url').style.display='none';" />Upload
                                <input type="radio" name="image_mode" value="url"  onclick="document.getElementById('image').style.display='none'; document.getElementById('image_url').style.display='block';" />URL<br>
<input type="file" name="image" id="image" style="width:400px;">
                                <input type="text" name="image_url" id="image_url" style="width:400px; " hidden>
<?php $message_cls->show_error('image')?>
</td>
</tr>
<tr>
<td class="label">Description *</td>
<td class="edit">
<textarea name="description" id="description" style="width: 400px; height: 150px;" class="validate[required,length[5-2000]]"><?= $description?></textarea>
<?php $message_cls->show_error('description')?>
</td>
</tr>
    </table>
    <br/>
    <h3>
            Contact
    </h3>
    <hr />
    <table class="contents_edit" id="store_contact">
                <tr>
                        <td class="label" width="120px">Phone</td>
<td class="edit">
<input type="text" name="phone" id="phone" value="<?= $phone?>" style="width: 400px;" class="" />
<?php $message_cls->show_error('phone')?>
</td>
</tr>
                <tr>
                        <td class="label" width="120px">EMail</td>
<td class="edit">
<input type="text" name="email" id="phone" value="<?= $email?>" style="width: 400px;" class="" />
<?php $message_cls->show_error('email')?>
</td>
</tr>
                <tr>
                        <td class="label" width="120px">Website</td>
<td class="edit">
<input type="text" name="website" id="website" value="<?= $website?>" style="width: 400px;" class="" />
<?php $message_cls->show_error('website')?>
</td>
</tr>
    </table>
    <br/>
    <h3>
            Address
    </h3>
    <hr />
    <table class="contents_edit" id="store_address">
                <tr>
                        <td class="label" width="120px">Zip Code</td>
<td class="edit">
<input type="text" name="address_zipcode" id="address_zipcode" value="<?= $address_zipcode?>" style="width: 400px;" class="" />
<?php $message_cls->show_error('address_zipcode')?>
</td>
</tr>
                <tr>
                        <td class="label" width="120px">Country</td>
<td class="edit">
<input type="text" name="address_country" id="address_country" value="<?= $address_country?>" style="width: 400px;" class="" />
<?php $message_cls->show_error('address_country')?>
</td>
</tr>
                <tr>
                        <td class="label" width="120px">State</td>
<td class="edit">
<input type="text" name="address_state" id="address_state" value="<?= $address_state?>" style="width: 400px;" class="" />
<?php $message_cls->show_error('address_state')?>
</td>
</tr>
                <tr>
                        <td class="label" width="120px">City</td>
<td class="edit">
<input type="text" name="address_city" id="address_city" value="<?= $address_city?>" style="width: 400px;" class="" />
<?php $message_cls->show_error('address_city')?>
</td>
</tr>
                <tr>
                        <td class="label" width="120px">Street</td>
<td class="edit">
<input type="text" name="address_street" id="address_street" value="<?= $address_street?>" style="width: 400px;" class="" />
<?php $message_cls->show_error('address_street')?>
</td>
</tr>
                <tr>
                        <td class="label" width="120px">Neighborhood</td>
<td class="edit">
<input type="text" name="neighborhood" id="neighborhood" value="<?= $neighborhood?>" style="width: 400px;" class="" />
<?php $message_cls->show_error('neighborhood')?>
</td>
</tr>
                <tr>
                        <td class="label" width="120px">Latitude</td>
<td class="edit">
<input type="text" name="latitude" id="latitude" value="<?= $latitude?>" style="width: 400px;" class="" />
<?php $message_cls->show_error('latitude')?>
</td>
</tr>
                <tr>
                        <td class="label" width="120px">Longitude</td>
<td class="edit">
<input type="text" name="longitude" id="longitude" value="<?= $longitude?>" style="width: 400px;" class="" />
<?php $message_cls->show_error('longitude')?>
</td>
</tr>
    </table>
    <br/>
    <h3>
            Other
    </h3>
    <hr />
    <table class="contents_edit" id="store_other">
                <tr>
                        <td class="label" width="120px">Store Hours</td>
<td class="edit">
<input type="text" name="store_hours" id="store_hours" value="<?= $store_hours?>" style="width: 400px;" class="" />
<?php $message_cls->show_error('store_hours')?>
</td>
</tr>
                <tr>
                        <td class="label">Loyalty Card</td>
                        <td class="edit">
                            <input type="checkbox" name="loyalty_card" value="Y" <?php if($loyalty_card=='Y') echo 'checked'; ?>>                    
                        </td>
                </tr>
                <?php if($id!=0) { ?>
                <tr>
                        <td class="label">Created </td>
                        <td class="edit">
                            <?= $created ?>
                        </td>
                </tr>
                <?php } ?>
    </table>
    <table class="contents_edit" id="store_action">        
<tr height="35px">
<td class="label"></td>
<td class="edit">
<input type="submit" value="  Save " name="action" style="width:80px;"/>

<a href="stores.php" class="button">&nbsp;&nbsp;&nbsp;Cancel&nbsp;&nbsp;&nbsp;</a>
</td>
</tr>
    </table>
</form>

<?php require(DIR_WS_INCLUDES . 'body_footer.php'); ?>
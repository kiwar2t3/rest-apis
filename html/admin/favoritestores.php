<?php
require('../includes/admin_application_top.php');

$titlex = "Favorite Stores";

require(DIR_WS_INCLUDES . 'body_header.php');

$action = tep_db_prepare_input($_REQUEST['action']);
if ($action == 'delete') {
$id = tep_get_value_get('id');

teb_delete_query(TABLE_FAVORITESTORES, array("id"=>$id));

tep_success_redirect("Successfully deleted favorite store.", "favoritestores.php");
} elseif ($action == 'all_delete') {
$favorite_ids = tep_get_value_post("favorite_ids");

for ($i = 0; $i < count($favorite_ids); $i ++) {
teb_delete_query(TABLE_FAVORITESTORES, array("id"=>$favorite_ids[$i]));
}

tep_success_redirect("Successfully deleted favorite stores.", "favoritestores.php");
}

$s_key = tep_db_prepare_input($_REQUEST['s_key']);
$s_store = tep_db_prepare_input($_REQUEST['s_store']);
$s_user = tep_db_prepare_input($_REQUEST['s_user']);

?>

<?php if ($errors['db'] != ""): ?>
<p class="error"><?= $errors['db']?></p>
<?php endif; ?>

<form class="search_form" name="search_form" method="post">
<div>
                User: <select name="s_user" onchange="this.form.submit()" style="width: 150px;">
<option value="">-- All --</option>
<?php $users = tep_db_query("select * from ".TABLE_USERS.""); while($user = tep_db_fetch_array($users)):?>
<option value="<?= $user['user_id']?>" <?php if ($s_user == $user['user_id']) echo "selected"?>><?= $user['user_fullname']?></option>
<?php endwhile;?>
</select>&nbsp;&nbsp;&nbsp;
Store: <select name="s_store" onchange="this.form.submit()" style="width: 150px;">
<option value="">-- All --</option>
<?php $stores = tep_db_query("select * from ".TABLE_STORES." order by title"); while($store = tep_db_fetch_array($stores)):?>
<option value="<?= $store['store_id']?>" <?php if ($s_store == $store['store_id']) echo "selected"?>><?= $store['title']?></option>
<?php endwhile;?>
</select>&nbsp;&nbsp;&nbsp;
<p>
<input type="button" value="Add Favorite Store" onclick="location.href='favoritestore_edit.php'"/>
</p>
</div>
</form>

<script type="text/javascript">
<!--
function delete_new(id, title) {
if (confirm("Are you sure want to delete "+title+"?")) {
location.href = "favoritestores.php?action=delete&id=" + id;
}
}

function all_action() {
if (confirm("Are you sure want to process?")) {
document.dataListForm.submit();
}
}
//-->
</script>

<form name="dataListForm" method="post" action="favoritestores.php" style="margin-top: 15px;">
<input type="hidden" name="s_store" value="<?= $s_store?>">
        <input type="hidden" name="s_user" value="<?= $s_user?>">
<input type="hidden" name="s_key" value="<?= $s_key?>">

With Selected Favorite Stores: <select name="action" onchange="all_action()">
<option value="">---</option>
<option value="all_delete">Delete</option>
</select>

<table class="contents_list" cellpadding="0" cellspacing="1">
<?php 
$sort_column = "id";
$sort_order = "asc";
if (isset($_REQUEST['sort_column']))$sort_column = tep_db_prepare_input($_REQUEST['sort_column']);
if (isset($_REQUEST['sort_order']))$sort_order = tep_db_prepare_input($_REQUEST['sort_order']);

$table_headers = array();
$table_headers[] = array('id'=>'', 'title'=>'<input type="checkbox" onchange="all_checkbox($(this))" />', 'width'=>'50');
$table_headers[] = array('id'=>'id', 'title'=>'ID', 'width'=>'100');
        $table_headers[] = array('id'=>'', 'title'=>'User', 'width'=>'200');
$table_headers[] = array('id'=>'', 'title'=>'Store', 'width'=>'200');
$table_headers[] = array('id'=>'created', 'title'=>'Created at', 'width'=>'150');
$table_headers[] = array('id'=>'', 'title'=>'Action', 'width'=>'200');

$column_count = count($table_headers);

include DIR_WS_BOX.'table_header.php';
?>

<?
$sql = "select * from " . TABLE_FAVORITESTORES. " where 1=1";
if ($s_key != '') {
//$sql.= " and LOWER(favorite) like '%".strtolower($s_key)."%'";
}

if ($s_store != '') {
$sql.= " and store_id = '".$s_store."'";
}

if ($s_user != '') {
$sql.= " and user_id = '".$s_user."'";
}
$sql .= " order by ".$sort_column." ".$sort_order;

$list_split = new splitPageResults($sql);
        $favorite_list = tep_db_query($list_split->sql_query);
 
$row = 0;
while ($favorite = tep_db_fetch_array($favorite_list)) {
$row ++;

$ext_params = "&id=".$favorite['id']."&s_store=".$s_store."&s_user=".$s_user."&s_key=".$s_key."&sort_column=".$sort_column."&sort_order=".$sort_order."&page=".$page;


?>
<tbody>   
<tr class='dataTableRow'>
<td align="center">
<input type="checkbox" name="favorite_ids[]" value="<?= $favorite['id']?>" class="all_check" />
</td>
<td align="center">
<a class="link" href="favoritestore_edit.php?id=<?= $favorite['id']?>" title="View Detail"><?=$favorite['id']?></a>
</td>
                <td align="center">
<?php if ($favorite['user_id'] > 0) :?>
<a class="link" href="user_edit.php?user_id=<?= $favorite['user_id']?>" title="View Detail">
<?= teb_query("select user_fullname from ".TABLE_USERS." where `user_id`=".$favorite['user_id'], "user_fullname")?>
</a>
<?php endif;?>
</td>
<td align="center">
<?php if ($favorite['store_id'] > 0) :?>
<a class="link" href="store_edit.php?id=<?= $favorite['store_id']?>" title="View Detail">
<?= teb_query("select title from ".TABLE_STORES." where `store_id`='".$favorite['store_id']."'", "title")?>
</a>
<?php endif;?>
</td>
<td align="center">
<?=$favorite['created']?>
</td>
<td align="center">
                        <!--<a class="button" href="favoritestore_edit.php?id=<?= $favorite['id']?>" title="Edit">Edit</a>-->
                        <a class="button" href="javascript:delete_new(<?= $favorite['id']?>, '')" title="Delete">Delete</a>        
                </td>
</tr>
<?php
}
?>
</tbody>
<?php 
$data_message = TEXT_DISPLAY_NUMBER_OF_FAVORITESTORES;
$empty_message = "No Favorite Store";
include DIR_WS_BOX.'table_footer.php';
?>
</table>

</form>

<?php require(DIR_WS_INCLUDES . 'body_footer.php'); ?>
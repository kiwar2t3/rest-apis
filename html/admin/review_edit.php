<?php
require('../includes/admin_application_top.php');

$titlex = "Create Product Review";

$id = 0;
if (isset($_GET['id'])) {
    $titlex = "Product Review Edit";
    $id = tep_get_value_get("id");
}

require(DIR_WS_INCLUDES . 'body_header.php');

$deal_id = 0;
$user_id = 0;
$mark = "";
$review = "";

if (isset($_POST["deal_id"])) {
    $deal_id = tep_get_value_post('deal_id');
    $user_id = tep_get_value_post('user_id');
    $mark = tep_get_value_post('mark', 'Mark', 'require;');
    $review = tep_get_value_post('review', 'Review', '');

    if ($message_cls->is_empty_error()) {

        $review = array(
            'deal_id' => $deal_id,
            'user_id' => $user_id,
            'mark' => $mark,
            'review' => $review,
            'created' => tep_now_datetime(),
        );

        if ($id == 0) {
            $result = tep_db_perform(TABLE_REVIEWS, $review, 'insert');
        } else {

            $result = tep_db_perform(TABLE_REVIEWS, $review, 'update', "id='" . $id . "'");
            //teb_delete_query(TABLE_PRODUCT_IMAGES, array("deal_id"=>$id));

        }

        if ($result > 0) {
            //tep_success_redirect("Success saved product information!", "product_edit.php?id=".$id);
            tep_success_redirect("Success saved product review!", "reviews.php");
        } else {
            $error_db = "Faild register review.";
        }
    }
} elseif ($id > 0) {
    $review_info = teb_one_query(TABLE_REVIEWS, array("id" => $id));

    $deal_id = $review_info['deal_id'];
    $user_id = $review_info['user_id'];
    $mark = $review_info['mark'];
    $review = $review_info['review'];
    $created = $review_info['created'];

}
?>

<form name="ad_form" encType="multipart/form-data" method="post" autocomplete="off" class="edit_form" id="ADForm">
    <?php if (isset($error_db)): ?>
        <p class="error"><?= $error_db ?></p>
    <?php endif; ?>

    <table class="contents_edit" id="ad_basic">
        <tr>
            <td class="label" width="120px">Deal *</td>
            <td class="edit">
                <select name="deal_id" style="width: 300px;">
                    <option value="0" <?php if ($deal_id == 0) echo "selected" ?>>-- Select Product --</option>
                    <?php $deals = tep_db_query("select d.*,p.*,p.title as product_title,s.*,s.title as store_title from ".TABLE_DEALS." as d join (".TABLE_PRODUCTS." as p, ".TABLE_STORES." as s) on (d.product_id = p.id and d.store_id = s.store_id)"); 
                            while($deal = tep_db_fetch_array($deals)):
                    ?>
                                    <option value="<?= $deal['deal_id']?>" <?php if ($deal_id == $deal['deal_id']) echo "selected"?>><?= $deal['product_title'].' - '.$deal['store_title'].','.$deal['location_name'].' ('.$deal['start_date'].' ~ '.$deal['end_date'].')'?></option>
                    <?php endwhile;?>
                </select>
            </td>
        </tr>
        <tr>
            <td class="label" width="120px">User *</td>
            <td class="edit">
                <select name="user_id" style="width: 300px;">
                    <option value="0" <?php if ($user_id == 0) echo "selected" ?>>-- Select User --</option>
                    <?php $users = tep_db_query("select * from " . TABLE_USERS . " order by user_fullname");
                    while ($user = tep_db_fetch_array($users)): ?>
                        <option value="<?= $user['user_id'] ?>" <?php if ($user_id == $user['user_id']) echo "selected" ?>><?= $user['user_fullname'] ?></option>
                    <?php endwhile; ?>
                </select>
            </td>
        </tr>
        <tr>
            <td class="label" width="120px">Mark *</td>
            <td class="edit">
                <select name="mark" style="width: 300px;">
                    <?php 
                        for($i=5; $i>=1; $i--){
                    ?>
                    <option value="<?=$i?>" <?php if($i==$mark) echo "selected" ?>><?=$i?></option>
                    <?php                            
                        }
                    ?>                    
                </select>
            </td>
        </tr>
        <tr>
                <td class="label">Review </td>
                <td class="edit">
                        <textarea name="review" id="review" style="width: 400px; height: 150px;" class=""><?= $review?></textarea>
                        <?php $message_cls->show_error('review')?>
                </td>
        </tr>
        <?php if($id!=0) { ?>
        <tr>
            <td class="label">Created </td>
            <td class="edit">
                <?= $created ?>
            </td>
        </tr>
        <?php } ?>
        <tr height="35px">
            <td class="label"></td>
            <td class="edit">
                <input type="submit" value="  Save " name="action" style="width:80px;"/>

                <input type="button" value="  Cancel " style="width:80px;" onclick="location.href = 'reviews.php'" />
            </td>
        </tr>
    </table>
</form>

<?php require(DIR_WS_INCLUDES . 'body_footer.php'); ?>
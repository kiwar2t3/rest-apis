<?php
require('../includes/admin_application_top.php');

$titlex = "Create Favorite Store";

$id = 0;
if (isset($_GET['id'])) {
    $titlex = "Favorite Store Edit";
    $id = tep_get_value_get("id");
}

require(DIR_WS_INCLUDES . 'body_header.php');

$store_id = 0;
$user_id = 0;

if (isset($_POST["store_id"])) {
    $store_id = tep_get_value_post('store_id');
    $user_id = tep_get_value_post('user_id');
    if ($message_cls->is_empty_error()) {

        $favorite = array(
            'store_id' => $store_id,
            'user_id' => $user_id,
            'created' => tep_now_datetime(),
        );

        $favoritestore_db = teb_one_query(TABLE_FAVORITESTORES, array("store_id"=>$store_id, "user_id"=>$user_id)); 
        if ($favoritestore_db) {
            $result = tep_db_perform(TABLE_FAVORITESTORES, $favorite, 'update', "user_id='" . $user_id . "' and store_id='".$store_id."'");            
        } else {
            $result = tep_db_perform(TABLE_FAVORITESTORES, $favorite, 'insert');
        }

        if ($result > 0) {
            //tep_success_redirect("Success saved store information!", "store_edit.php?id=".$id);
            tep_success_redirect("Success saved store review!", "favoritestores.php");
        } else {
            $error_db = "Faild register review.";
        }
    }
} elseif ($id > 0) {
    $favorite_info = teb_one_query(TABLE_FAVORITESTORES, array("id" => $id));

    $store_id = $favorite_info['store_id'];
    $user_id = $favorite_info['user_id'];
    $mark = $favorite_info['mark'];
    $created = $favorite_info['created'];
}
?>

<form name="ad_form" encType="multipart/form-data" method="post" autocomplete="off" class="edit_form" id="ADForm">
    <?php if (isset($error_db)): ?>
        <p class="error"><?= $error_db ?></p>
    <?php endif; ?>

    <table class="contents_edit" id="ad_basic">
        <tr>
            <td class="label" width="120px">User *</td>
            <td class="edit">
                <select name="user_id" style="width: 300px;">
                    <option value="0" <?php if ($user_id == 0) echo "selected" ?>>-- Select User --</option>
                    <?php $users = tep_db_query("select * from " . TABLE_USERS . " order by user_fullname");
                    while ($user = tep_db_fetch_array($users)): ?>
                        <option value="<?= $user['user_id'] ?>" <?php if ($user_id == $user['user_id']) echo "selected" ?>><?= $user['user_fullname'] ?></option>
                    <?php endwhile; ?>
                </select>
            </td>
        </tr>
        <tr>
            <td class="label" width="120px">Store *</td>
            <td class="edit">
                <select name="store_id" style="width: 300px;">
                    <option value="0" <?php if ($store_id == 0) echo "selected" ?>>-- Select Store --</option>
                    <?php $stores = tep_db_query("select * from " . TABLE_STORES . " order by title");
                    while ($store = tep_db_fetch_array($stores)): ?>
                        <option value="<?= $store['store_id'] ?>" <?php if ($store_id == $store['store_id']) echo "selected" ?>><?= $store['title'] ?></option>
                    <?php endwhile; ?>
                </select>
            </td>
        </tr>
        <?php if($id!=0) { ?>
        <tr>
            <td class="label">Created </td>
            <td class="edit">
                <?= $created ?>
            </td>
        </tr>
        <?php } ?>
        <tr height="35px">
            <td class="label"></td>
            <td class="edit">
                <input type="submit" value="  Save " name="action" style="width:80px;"/>
                <input type="button" value="  Cancel " style="width:80px;" onclick="location.href = 'favoritestores.php'" />
            </td>
        </tr>
    </table>
</form>

<?php require(DIR_WS_INCLUDES . 'body_footer.php'); ?>
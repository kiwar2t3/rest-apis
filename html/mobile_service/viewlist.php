<?php
include 'shrinkService.php';

class EditList extends ShrinkService {
	function excute() {
		global $message_cls;
		
		$list_id = tep_get_value_post('list_id', 'ListId', 'require;');
		if ($message_cls->is_empty_error()) {
			$list = (array) teb_one_query(TABLE_LISTS, 'id = ' . $list_id);
			$result = array();
			foreach ( $list as $key => $val ) {
				if ( $key != 'id' && $key != 'user_id' && $key != 'is_updated' )
					$result[$key] = $val;
			}
			$this->_result = $result;
		}else{
			$this->set_error($message_cls->get_all_message());
		}
	}	
}
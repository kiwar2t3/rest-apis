<?php
include 'shrinkService.php';

class EditProfile extends ShrinkService {
	function excute() {
		global $message_cls;
		
		$this->check_login_user();
		if ($message_cls->is_empty_error()) {
				$this->_result = $this->_user;
				$this->_result -> user_avatar = getUploadFileAbsolutePath($this->_result -> user_avatar);
			
		}else{
			$this->set_error($message_cls->get_all_message());
		}
	}	
}
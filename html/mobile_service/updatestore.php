<?php

include 'shrinkService.php';

class UpdateStore extends ShrinkService {

  function excute() {
    global $message_cls;
	
	$store_id = tep_get_value_post('store_id', 'StoreId', 'require;');
	$store_name = tep_get_value_post('store_name', 'Name', 'require;');
	$hours = tep_get_value_post('hours', 'Hours', 'require;');
	$notes = tep_get_value_post('notes', 'Notes', 'require;');
    $address = tep_get_value_post('address', 'Address', 'require;');
    $city	= tep_get_value_post('city', 'City', 'require;');
    $country	= tep_get_value_post('country', 'Country', 'require;');
    $postal_code	= tep_get_value_post('postal_code', 'PostalCode', 'require;');
    $phone	= tep_get_value_post('phone', 'Phone', 'require;');
	
	if ($message_cls->is_empty_error()) {
		$storeData = teb_one_query(TABLE_STORES, 'id = ' . $store_id);
		$address_id = $storeData -> address_id;
		$addressData = array(
			'address' => $address,
			'city' => $city,
			'country' => $country,
			'postal_code' => $postal_code,
			'phone' => $phone
		);
		$addressData = tep_db_perform(TABLE_ADDRESS, $addressData, 'update', 'id = ' . $address_id);
		if ( $addressData <= 0 ) {
			$this->set_error(ERROR_SERVER_PROBLEM);
		}
		$store = array(
			'address_id' => $address_id,
			'name' => $store_name,
			'hours' => $hours,
			'notes' => $notes
		);
		$result = tep_db_perform(TABLE_STORES, $store, 'update' , 'id = ' . $store_id);
		if ($result > 0) {
			$result = array();
			$result['StoreId'] = $store_id;
			$result['name'] = 'Upadated Store';
			$result['description'] = 'Successfully Updated New Store';
			$this -> _result = $result;
		} else {
			$this->set_error(ERROR_SERVER_PROBLEM);
		}
    } else {
		$this->set_error($message_cls->get_all_message());
    }
  }

}

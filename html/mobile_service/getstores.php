<?php
include 'shrinkService.php';

class GetStores extends ShrinkService {
	function excute() {
	
                if(tep_get_value_post('auth_token'))
                    $this->check_login_user();

		$this->_result = array();
                
                if(tep_get_value_post('auth_token')){
                    $temp = tep_db_query("select s.*, f.user_id from ".TABLE_STORES." as s LEFT JOIN ".TABLE_FAVORITESTORES." as f ".
                            " on s.store_id = f.store_id ".                            
                            " where (f.user_id = ".$this->_userid." or f.user_id is NULL) and s.actived='Y' and s.deleted<>'Y' ".
                            " order by f.user_id desc, s.created desc");		
                    while($store = tep_db_fetch_array($temp)) {
                            $store['image_original'] = getUploadFileAbsolutePath($store['image_original']);
                            $store['image_thumb'] = getUploadFileAbsolutePath($store['image_thumb']);
                            $store['logo'] = getUploadFileAbsolutePath($store['logo']);
                            if($store['user_id'])
                                $store['favorite'] = TRUE;
                            else
                                $store['favorite'] = FALSE;
                            $this->_result[] = $store;
                    }
                }else{
                    $temp = tep_db_query("select * from ".TABLE_STORES." where actived='Y' and deleted<>'Y' order by created desc");		
                    while($store = tep_db_fetch_array($temp)) {
                            $store['image_original'] = getUploadFileAbsolutePath($store['image_original']);
                            $store['image_thumb'] = getUploadFileAbsolutePath($store['image_thumb']);
                            $store['logo'] = getUploadFileAbsolutePath($store['logo']);
                            $this->_result[] = $store;
                    }
                }
		
		
	}
}
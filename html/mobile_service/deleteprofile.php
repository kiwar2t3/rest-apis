<?php
include 'shrinkService.php';

class EditProfile extends ShrinkService {
	function excute() {
		global $message_cls;
		
		$this->check_login_user();
		
		if ($message_cls->is_empty_error()) {
			$query = 'DELETE FROM ' . TABLE_USERS . ' WHERE user_id = ' . $this -> _userid;
			tep_excute_query($query);
			$result = array();
			$result['description'] = 'Profile is successfully deleted.';
			$this -> _result = $result;
			
		}else{
			$this->set_error($message_cls->get_all_message());
		}
	}	
}
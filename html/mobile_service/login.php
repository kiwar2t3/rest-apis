<?php
include 'shrinkService.php';

class Login extends ShrinkService {
	function excute() {
		global $message_cls;
		
		$user_email	= tep_get_value_post('user_email', 'Name', 'require;');
		$user_password	= tep_get_value_post('user_password', 'Password', 'require');
				
		if ($message_cls->is_empty_error()) {
			$this->get_login_user_email($user_email);
			
			if ($this->_userid == 0) {
				$this->set_error("This user doesn't exist.");
			} else {
				if (tep_validate_password($user_password, $this->_user -> user_password)) {
					
					$newauth = $this->_generateAuthToken();
					$user_update = array(						
						'user_last_logined'				=> tep_now_datetime(),
						'user_authtoken'				=> $newauth,
						'user_last_logined'				=> tep_now_datetime()
					);
					
					$result = tep_db_perform(TABLE_USERS, $user_update, "update", "user_id='".$this->_userid."'");
					
					if ($result > 0) {
						$this->_result = $this->_user;
						$this->_result -> user_authtoken	= $newauth;
						$this->_result -> user_avatar = getUploadFileAbsolutePath($this->_result -> user_avatar);						
					} else {
						$this->set_error(ERROR_SERVER_PROBLEM);
					}					
				} else {
					$this->set_error("Password is incorrect.");
				}
			}	
		} else {
			$this->set_error($message_cls->get_all_message());
		}
	}	
}
<?php
include 'shrinkService.php';

class DeleteStore extends ShrinkService {
	function excute() {
		global $message_cls;
		
		$store_id = tep_get_value_post('store_id', 'StoreId', 'require;');
		if ($message_cls->is_empty_error()) {
			$storeData = teb_one_query(TABLE_STORES, 'id = ' . $store_id);
			// $address_id = $storeData -> address_id;
			$query = 'DELETE FROM ' . TABLE_STORES . ' WHERE id = ' . $store_id . ';';
			tep_excute_query($query);
			$result = array();
			$result['description'] = 'Store is successfully deleted.';
			$this -> _result = $result;
			
		}else{
			$this->set_error($message_cls->get_all_message());
		}
	}	
}
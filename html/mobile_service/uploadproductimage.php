<?php
include 'shrinkService.php';

class UploadProductImage extends ShrinkService {
	function excute() {
		global $message_cls;
		
		$product_id = tep_get_value_post('product_id', 'ProductId', 'require;');
		if ($message_cls->is_empty_error()) {
			$product_info = teb_one_query(TABLE_PRODUCTS, array("product_id" => $product_id));
			$product_image = upload_product_image($product_id, $product_info -> product_name, 'product_image');
			$image = array(
				'image_original' => $product_image['original'],
				'image_small' => $product_image['small'],
				'image_large' => $product_image['large']
			
			);
			$imageResult = tep_db_perform(TABLE_IMAGES, $image, 'insert');
			$image_id = tep_db_insert_id();
			$updateQuery = 'UPDATE ' . TABLE_PRODUCTS . ' SET image_id = ' . $image_id . ' WHERE product_id = ' . $product_id . ';';
			tep_excute_query($updateQuery);
			$this->_result = teb_one_query(TABLE_IMAGES, array("image_id" => $image_id));
		}else{
			$this->set_error($message_cls->get_all_message());
		}
	}	
}
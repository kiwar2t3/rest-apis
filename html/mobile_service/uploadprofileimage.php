<?php
include 'shrinkService.php';

class UploadProfileImage extends ShrinkService {
	function excute() {
		global $message_cls;
		
		$profile_id = tep_get_value_post('profile_id', 'ProfileId', 'require;');
		if ($message_cls->is_empty_error()) {
			$user_info = teb_one_query(TABLE_USERS, array("user_id" => $profile_id));
			$user_avatar = upload_avatar($profile_id, $user_info -> user_email, "user_avatar");
			$this -> _result = teb_one_query(TABLE_USERS, array("user_id" => $profile_id));
			$this -> _result -> user_avatar = getUploadFileAbsolutePath($this -> _result -> user_avatar);
		}else{
			$this->set_error($message_cls->get_all_message());
		}
	}	
}
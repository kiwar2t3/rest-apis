<?php
include 'shrinkService.php';

class GetProduct extends ShrinkService {
	function excute() {
		//$this->check_login_user();
                
                $product_id = tep_get_value_post('product_id', 'Product ID', 'require');
                $product = teb_one_query(TABLE_PRODUCTS, array("id"=>$product_id));
                
                if($product){
			$productimages = tep_db_query("select * from ".TABLE_PRODUCT_IMAGES." where product_id=".$product['id']);
			while($pimage = tep_db_fetch_array($productimages)){
				$pimage['image_original'] = getUploadFileAbsolutePath($pimage['image_original']);
				$pimage['image_thumb'] = getUploadFileAbsolutePath($pimage['image_thumb']);
				$product['images'][] = $pimage;
			}			
                        
                        /*$product['mark'] = 0; $kk=0;
                        $productmarks = tep_db_query("select * from ".TABLE_REVIEWS." where product_id=".$product['id']);
			while($pmark = tep_db_fetch_array($productmarks)){
                            $product['mark'] = $product['mark'] + $pmark['mark'];
                            $kk++;
                        }
                        if($kk>0)
                            $product['mark'] = $product['mark'] / $kk;*/
                        
                        $product['category']= teb_one_query(TABLE_CATEGORIES, array("category_id"=>$product['category_id']));
                        $product['brand']= teb_one_query(TABLE_BRANDS, array("brand_id"=>$product['brand_id']));
			$this->_result = $product;
		}else{
                        $this->set_error('Not Found');
                }		
	}
}

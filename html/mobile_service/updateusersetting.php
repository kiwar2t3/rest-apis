<?php
include 'shrinkService.php';

class UpdateUserSetting extends ShrinkService {
	function excute() {
		global $message_cls;
		
		$this -> check_login_user();
		$theme_color  		= tep_get_value_post('theme_color', 'ThemeColor', 'require;');
		$font_size			= tep_get_value_post('font_size', 'FontSize', 'require;');
		
		if ($message_cls->is_empty_error()) {
			$userSetting = array(
				'user_id'		=> $this -> _userid,
				'theme_color'	=> $theme_color,					
				'font_size'	=> $font_size
			);
			$result = tep_db_perform(TABLE_SETTINGS, $userSetting, 'update', 'user_id = ' . $this -> _userid);
			if ($result > 0) {
				$user_id = $this->_userid;
				$result = array();
				$result['UserId'] = $user_id;
				$result['name'] = 'Update User Setting';
				$result['description'] = 'Successfully UPDATED';
				$this -> _result = $result;
				
			} else {
				$this->set_error(ERROR_SERVER_PROBLEM);
			}	
		} else {
			$this->set_error($message_cls->get_all_message());
		}
	}	
}
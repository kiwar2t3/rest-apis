<?php

include 'shrinkService.php';

class DeleteDiscount extends ShrinkService {

  function excute() {
    global $message_cls;
	$product_id = tep_get_value_post('product_id', 'ProductId', 'require;');
	$discount_id = tep_get_value_post('discount_id', 'DiscountId', 'require;');
	if ($message_cls->is_empty_error()) {
		$deleteQuery = 'Delete from ' . TABLE_DISCOUNT . ' where id = ' . $discount_id;
		tep_excute_query($deleteQuery);
		$updateQuery = 'UPDATE ' . TABLE_PRODUCTS . ' SET discount_id = NULL WHERE product_id = ' . $product_id . ';';
		tep_excute_query($deleteQuery);
		$result = array();
		$result['discount'] = 'Successfully Deleted';
		$this->_result = $result;
    } else {
      $this->set_error($message_cls->get_all_message());
    }
  }

}

<?php
include 'shrinkService.php';

class DeleteList extends ShrinkService {
	function excute() {
		global $message_cls;
		
		$list_id = tep_get_value_post('list_id', 'ListId', 'require;');
		if ($message_cls->is_empty_error()) {
			$query = 'DELETE FROM ' . TABLE_LISTS . ' WHERE id = ' . $list_id . ';';
			tep_excute_query($query);
			$result = array();
			$result['description'] = 'List Details is successfully deleted.';
			$this -> _result = $result;
			
		}else{
			$this->set_error($message_cls->get_all_message());
		}
	}	
}
<html>
<head>
<title>Test web service</title>
<style>
	li{
		margin: 10px;
	}
</style>
</head>
<body>
<br>
<ol>
<li><a href='registration.php'>User Sign Up</a></li>
<li><a href='login.php'>User Log In</a></li>
<br>
<li><a href='facebooksignup.php'>Facebook Sign Up</a></li>
<br>
<li><a href='insertprofile.php'>Insert Profile</a></li>
<li><a href='viewprofile.php'>View Profile</a></li>
<li><a href='updateprofile.php'>Update Profile</a></li>
<li><a href='deleteprofile.php'>Delete Profile</a></li>
<br>
<li><a href = 'registerproduct.php'>Register Product</a></li>
<li><a href='getsmallproducts.php'>Get Products with Small Info</a></li>
<li><a href='getlargeproducts.php'>Get Products with Large Info</a></li>
<br>
<li><a href='uploadprofileimage.php'>Upload Profile Image</a></li>
<!--<li><a href='downloadprofileimage.php'>Download Profile Image</a></li>-->
<li><a href='uploadproductimage.php'>Upload Product Image</a></li>
<!--<li><a href='downloadproductimage.php'>Download Product Image</a></li>-->
<br>
<li><a href='uploadfileforprofile.php'>Upload Profile File</a></li>
<!--<li><a href='downloadfileforprofile.php'>Download Profile File</a></li>-->
<li><a href='uploadproductfile.php'>Upload Product File</a></li>
<!--<li><a href='downloadproductfile.php'>Download Product File</a></li>-->
<br>
<li><a href='insertlist.php'>Insert List</a></li>
<li><a href='viewlist.php'>View List</a></li>
<li><a href='updatelist.php'>Update List</a></li>
<li><a href='deletelist.php'>Delete List</a></li>
<br>
<li><a href='insertdiscount.php'>Insert Discount</a></li>
<li><a href='updatediscount.php'>Update Discount</a></li>
<li><a href='deletediscount.php'>Delete Discount</a></li>
<br>
<li><a href='insertstore.php'>Insert Store</a></li>
<li><a href='updatestore.php'>Update Store</a></li>
<li><a href='deletestore.php'>Delete Store</a></li>
<br>
<li><a href='insertstorecards.php'>Insert StoreCards</a></li>
<li><a href='updatestorecard.php'>Update StoreCards</a></li>
<li><a href='deletestorecard.php'>Delete StoreCards</a></li>
<br>
<li><a href='insertusersetting.php'>Insert User Settings</a></li>
<li><a href='updateusersetting.php'>Update User Settings</a></li>
<li><a href='deleteusersetting.php'>Delete User Settings</a></li>
<br>
<li><a href='writefeedback.php'>Write Feedback</a></li>
<li><a href='getfeedback.php'>See All The Feedbacks</a></li>
<br>
<!--<li><a href='changepassword.php'>Change Password</a></li>
<li><a href='getproducts.php'>Get Products</a></li>
<li><a href='editproduct.php'>Add/Edit Product</a></li>
<li><a href='deleteproduct.php'>Delete Product</a></li>-->
</ol>
</body>
</html>
<html>
<title>Test web service</title>
<body style="width: 100%;">

<a href="index.php">back to menu</a>

<h1>Insert List</h1>
<h3>mobile_service.php</h3>

<form encType="multipart/form-data" method="post" id="edit_form" target="result" action="../mobile_service.php">
	<input type="hidden" name="action" value="insertlist" />	
	<table class="contents_edit" id="public_profile">		
		<tr height="35px">
			<td class="label">action</td>
			<td class="edit">
				insertlist
			</td>
		</tr>		
		<tr height="35px">
			<td class="label">store_name</td>
			<td class="edit">
				<input type="text" name="store_name" value="" style="width:300px;"/>
			</td>
		</tr>
		<tr height="35px">
			<td class="label">created_date</td>
			<td class="edit">
				<input type="text" name="created_date" value="" style="width:300px;"/>
			</td>
		</tr>
		<tr height="35px">
			<td class="label">items_count</td>
			<td class="edit">
				<input type="text" name="items_count" value="" style="width:300px;"/>
			</td>
		</tr>
		<tr height="35px">
			<td class="label">notes</td>
			<td class="edit">
				<input type="text" name="notes" value="" style="width:300px;"/>
			</td>
		</tr>
		<tr height="35px">
			<td class="label">is_shared</td>
			<td class="edit">
				<select name = 'is_shared'>
					<option value = 1>Yes</option>
					<option value = 0>No</option>
				</select>
			</td>
		</tr>
		<tr height="35px">
			<td class="label">is_newcomments</td>
			<td class="edit">
				<select name = 'is_newcomments'>
					<option value = 1>Yes</option>
					<option value = 0>No</option>
				</select>
			</td>
		</tr>
		<tr height="35px">
			<td class="label">auth_token</td>
			<td class="edit">
				<input type="text" name="auth_token" value="" style="width:300px;"/>
			</td>
		</tr>
		<tr height="35px">
			<td class="label"></td>
			<td class="edit">
				<input type="submit" value="  Insert " style="width:300px;"/>
			</td>
		</tr>
	</table>

</form>

<b>Result:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b><a href="http://json.parser.online.fr/" target="blank">JSON Beautifier</a>
<iframe name="result" style="width: 100%; height: 100%;"></iframe>

</body>
</html>
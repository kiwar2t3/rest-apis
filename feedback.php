<?php
require_once ('./library/application_top.php');
include_once ('./views/header.php');
?>
<!--=== Breadcrumbs ===-->
<div class="breadcrumbs">
<div class="container">
<h1 class="pull-left">Feedback</h1>
<ul class="pull-right breadcrumb">
<li><a href="index.html">Home</a></li>
<li><a href="">Pages</a></li>
<li class="active">Feedback</li>
</ul>
</div>
</div><!--/breadcrumbs-->
<!--=== End Breadcrumbs ===-->


<div class="body-container">
<!--=== Content Part ===-->
<div class="container content">
<div class="row-fluid feedback">
<div class = 'col-md-12 feedback-first'>
<p>We would greatly appreciate your thoughts in the short survey below. Thank you for helping us to improve the FoodAppexperience for our customers!</p>
</div>
<div class = 'col-md-12 feedback-rate'>
<div class = 'col-md-5'>
How would you rate the experience of completing this task today?
</div>
<div class = 'col-md-7'>
<select class = 'feedback-rateselect'>
<option value = 0>Please choose one...</option>
<option value = 1>1</option>
<option value = 2>2</option>
<option value = 3>3</option>
<option value = 4>4</option>
<option value = 5>5</option>
<option value = 6>6</option>
<option value = 7>7</option>
<option value = 8>8</option>
<option value = 9>9</option>
<option value = 10>10</option>
</select>
</div>

</div>
<div class = 'col-md-12 feedback-submit'>
<div class = 'col-md-10'>
Please do not leave personal information as your comments
</div>
<div class = 'col-md-2'>
<button class = 'feedback-submit'>Submit</button>
</div>
</div>
</div><!--/row-fluid-->
</div><!--/container-->
<!--=== End Content Part ===-->
</div>

<!--=== Footer Version 1 ===-->
<?php include './views/footer.php'; ?>
</div><!--/wrapper-->


<!--=== End Style Switcher ===-->

<!-- JS Global Compulsory -->
<script type="text/javascript" src="assets/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript" src="assets/plugins/jquery/jquery-migrate.min.js"></script>
<script type="text/javascript" src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<!-- JS Implementing Plugins -->
<script type="text/javascript" src="assets/plugins/back-to-top.js"></script>
<script type="text/javascript" src="assets/plugins/smoothScroll.js"></script>
<!-- JS Customization -->
<script type="text/javascript" src="assets/js/custom.js"></script>
<!-- JS Page Level -->
<script type="text/javascript" src="assets/js/app.js"></script>
<script type="text/javascript" src="assets/js/plugins/style-switcher.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() {
App.init();
StyleSwitcher.initStyleSwitcher();
var i = 1;
$('.head-icon').each(function () {
$(this).click(function () {
 $(this).toggleClass('fa-plus fa-minus');
 $(this).parent().next('.body-text').slideToggle(100);
});
i ++;
});
});
</script>
</body>
</html>

<?php 
require_once './library/application_top.php';

tep_session_unregister(SESSION_USER_ID);
tep_session_unregister(SESSION_AUTH_TOKEN);

tep_session_destroy();
tep_redirect("login.php");

exit();
?>
<?php
define('DEFAULT_TIMEZONE', 'Europe/Warsaw');

define('SITE_TITLE', "Food Copia");

define('HTTP_SERVER', "http://" . $_SERVER["SERVER_NAME"]);
define('HTTP_CATALOG_SERVER', HTTP_SERVER . "/foo/restapi/");
//define('HTTP_CATALOG_SERVER', HTTP_SERVER . "/2016/weitang/Ali/");

define('HTTP_REST_API_HOST', "localhost");
define('HTTP_REST_API_URL', "/restapi/mobile_service.php");

define('DIR_WS_CONFIGURE', DIR_FS_DOCUMENT_ROOT . 'config/');
define('DIR_WS_LIBRARY', DIR_FS_DOCUMENT_ROOT . 'library/');
define('DIR_WS_FUNCTIONS', DIR_FS_DOCUMENT_ROOT . 'library/functions/');
define('DIR_WS_CLASSES', DIR_FS_DOCUMENT_ROOT . 'library/classes/');
define('DIR_WS_BOX', DIR_FS_DOCUMENT_ROOT . 'library/box/');
define('CACHE_DIR', DIR_WS_LIBRARY . 'cache/');

define('HTTP_WS_STATIC', HTTP_CATALOG_SERVER . 'static/');
define('HTTP_WS_VIDEOS', HTTP_WS_STATIC . 'videos/');
define('HTTP_WS_IMAGES', HTTP_WS_STATIC . 'images/');

//Session
define('SESSION_USER_ID', 'food_copia_user_id');
define('SESSION_AUTH_TOKEN', 'food_copia_auth_token');
define('SESSION_WRITE_DIRECTORY', DIR_WS_LIBRARY . 'cache/');

define('USE_PCONNECT', 'false');
define('STORE_SESSIONS', 'file');

$no_login_pages = array(
"index.php",
"login.php",
"about_us.php",
"contact_us.php",
"faq.php",
"privacy_policy.php",
"signup.php"
);
?>
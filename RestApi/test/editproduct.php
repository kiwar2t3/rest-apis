<html>
<title>Test web service</title>
<body style="width: 100%;">

<a href="index.php">back to menu</a>

<h1>Add / Edit Product</h1>
<h2>mobile_service.php</h2>

<form encType="multipart/form-data" method="post" id="edit_form" target="result" action="../mobile_service.php">
	<input type="hidden" name="action" value="editproduct" />
	<table class="contents_edit" id="public_profile">		
		<tr height="35px">
			<td class="label">action</td>
			<td class="edit">
				editproduct
			</td>
		</tr>		
		<tr height="35px">
			<td class="label">auth_token</td>
			<td class="edit">
				<input type="text" name="auth_token" value="" style="width:300px;"/>
			</td>
		</tr>		
		<tr height="35px">
			<td class="label">id</td>
			<td class="edit">
				<input type="text" name="id" value="" style="width:300px;"/>
			</td>
		</tr>
		<tr height="35px">
			<td class="label">category_id</td>
			<td class="edit">
				<input type="text" name="category_id" value="" style="width:300px;"/>
			</td>
		</tr>
		<tr height="35px">
			<td class="label">title</td>
			<td class="edit">
				<input type="text" name="title" value="Test Product" style="width:300px;"/>
			</td>
		</tr>	
		<tr height="35px">
			<td class="label">price</td>
			<td class="edit">
				<input type="text" name="price" value="" style="width:300px;"/>
			</td>
		</tr>		
		<tr height="35px">
			<td class="label">comment</td>
			<td class="edit">
				<input type="text" name="comment" value="" style="width:300px;"/>
			</td>
		</tr>
		<tr height="35px">
			<td class="label">address</td>
			<td class="edit">
				<input type="text" name="address" value="" style="width:300px;"/>
			</td>
		</tr>
		<tr height="35px">
			<td class="label">latitude</td>
			<td class="edit">
				<input type="text" name="latitude" value="" style="width:300px;"/>
			</td>
		</tr>
		<tr height="35px">
			<td class="label">longitude</td>
			<td class="edit">
				<input type="text" name="longitude" value="" style="width:300px;"/>
			</td>
		</tr>		
		<tr height="35px">
			<td class="label">image0</td>
			<td class="edit">
				<input type="file" name="image0" style="width:300px;"/>
			</td>
		</tr>
		<tr height="35px">
			<td class="label">image1</td>
			<td class="edit">
				<input type="file" name="image1" style="width:300px;"/>
			</td>
		</tr>
		<tr height="35px">
			<td class="label">image2</td>
			<td class="edit">
				<input type="file" name="image2" style="width:300px;"/>
			</td>
		</tr>
		<tr height="35px">
			<td class="label">image3</td>
			<td class="edit">
				<input type="file" name="image3" style="width:300px;"/>
			</td>
		</tr>
		<tr height="35px">
			<td class="label"></td>
			<td class="edit">
				<input type="submit" value="  Get " style="width:300px;"/>
			</td>
		</tr>
	</table>

</form>

<b>Result:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b><a href="http://json.parser.online.fr/" target="blank">JSON Beautifier</a>
<iframe name="result" style="width: 100%; height: 100%;"></iframe>

</body>
</html>
<html>
<title>Test web service</title>
<body style="width: 100%;">

<a href="index.php">back to menu</a>

<h1>Update List</h1>
<h2>mobile_service.php</h2>

<form encType="multipart/form-data" method="post" id="edit_form" target="result" action="../mobile_service.php">
	<input type="hidden" name="action" value="updatelist" />	
	<table class="contents_edit" id="public_profile">		
		<tr height="35px">
			<td class="label">action</td>
			<td class="edit">
				updatelist
			</td>
		</tr>		
		<tr height="35px">
			<td class="label">list_id</td>
			<td class="edit">
				<input type="text" name="list_id" value="" style="width:300px;"/>
			</td>
		</tr>		
		<tr height="35px">
			<td class="label">store_name</td>
			<td class="edit">
				<input type="text" name="store_name" value="" style="width:300px;"/>
			</td>
		</tr>		
		<tr height="35px">
			<td class="label">items_count</td>
			<td class="edit">
				<input type="text" name="items_count" value="" style="width:300px;"/>
			</td>
		</tr>		
		<tr height="35px">
			<td class="label">notes</td>
			<td class="edit">
				<input type="text" name="notes" value="" style="width:300px;"/>
			</td>
		</tr>		
		<tr height="35px">
			<td class="label">is_shared</td>
			<td class="edit">
				<select name = 'is_shared'>
					<option value = 1>Yes</option>
					<option value = 0>No</option>
				</select>
			</td>
        </tr>	
		<tr height="35px">
			<td class="label">is_newcomments</td>
			<td class="edit">
				<select name = 'is_newcomments'>
					<option value = 1>Yes</option>
					<option value = 0>No</option>
				</select>
			</td>
        </tr>	
		
		<tr height="35px">
			<td class="label"></td>
			<td class="edit">
				<input type="submit" value="  Update " style="width:300px;"/>
			</td>
		</tr>
	</table>

</form>

<b>Result:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b><a href="http://json.parser.online.fr/" target="blank">JSON Beautifier</a>
<iframe name="result" style="width: 100%; height: 100%;"></iframe>

</body>
</html>
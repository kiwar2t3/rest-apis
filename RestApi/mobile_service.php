<?php

require('includes/application_top.php');
header('Access-Control-Allow-Origin: *');

$action = tep_get_value_post("action");

$service = null;

switch ($action){
	
	case 'registration':
		require('mobile_service/registration.php');
		$service = new Registration();
		break;

	case'login':
		require('mobile_service/login.php');
		$service= new Login();
		break;
	
		//forgotpassword
	case'forgotpassword':
		require('mobile_service/forgotpassword.php');
		$service= new ForgotPassword();
		break;
	
		//resetpassword
	case'resetpassword':
		require('mobile_service/resetpassword.php');
		$service=new ResetPassword();
		break;
	
		//insertprofile
	case'insertprofile':
		require('mobile_service/insertprofile.php');
		$service=new Registration();
		break;
	
		//resetpassword
	case'changepassword':
		require('mobile_service/changepassword.php');
		$service=new ChangePassword();
		break;
	
		//viewprofile
	case'viewprofile':
		require('mobile_service/viewprofile.php');
		$service=new EditProfile();
		break;
		//updateprofile
	case'updateprofile':
		require('mobile_service/updateprofile.php');
		$service=new EditProfile();
		break;
	
		//deleteprofile
	case'deleteprofile':
		require('mobile_service/deleteprofile.php');
		$service=new EditProfile();
		break;
		
		//insertlist
	case'insertlist':
		require('mobile_service/insertlist.php');
		$service=new EditList();
		break;
		
		//viewlist
	case'viewlist':
		require('mobile_service/viewlist.php');
		$service=new MyListItems();
		break;
		
	//mylists
	case'mylists':
		require('mobile_service/viewlists.php');
		$service=new MyLists();
		break;
	
		//updatelist
	case'updatelist':
		require('mobile_service/updatelist.php');
		$service=new UpdateList();
		break;
	
		//deletelist
	case'deletelist':
		require('mobile_service/deletelist.php');
	
		$service=new DeleteList();
		break;
	
		//insertdiscount
	case'insertdiscount':
		require('mobile_service/insertdiscount.php');
		$service=new InsertDiscount();
		break;
	
		//updatediscount
	case'insertdiscount':
		require('mobile_service/updatediscount.php');
		$service=new UpdateDiscount();
		break;
	
		//deletediscount
	case'deletediscount':
		require('mobile_service/deletediscount.php');
		$service=new DeleteDiscount();
		break;
	
		//insertstore
	case'insertstore':
		require('mobile_service/insertstore.php');
		$service=new InsertStore();
		break;
	
		//updatestore
	case'updatestore':
		require('mobile_service/updatestore.php');
		$service=new UpdateStore();
		break;
	
		//deletestore
	case'deletestore':
		require('mobile_service/deletestore.php');
		$service=new DeleteStore();
		break;
	
		//insertstorecards
	case'insertstorecards':
		require('mobile_service/insertstorecards.php');
		$service=new InsertStoreCards();
		break;
	
		//updatestorecard
	case'updatestorecard':
		require('mobile_service/updatestorecard.php');
		$service=new UpdateStoreCard();
		break;
	
		//deletestorecard
	case'deletestorecard':
		require('mobile_service/deletestorecard.php');
		$service=new DeleteStoreCard();
		break;
	
		//insertusersetting
	case'insertusersetting':
		require('mobile_service/insertusersetting.php');
		$service=new InsertUserSetting();
		break;
	
		//updateusersetting
	case'updateusersetting':
		require('mobile_service/updateusersetting.php');
		$service=new UpdateUserSetting();
		break;
	
		//deleteusersetting
	case'deleteusersetting':
		require('mobile_service/deleteusersetting.php');
		$service=new DeleteUserSetting();
		break;
	
		//facebooksignup
	case'facebooksignup':
		require('mobile_service/facebooksignup.php');
		$service=new FacebookSignUp();
		break;
	
	
		//getcountries
	case'getcountries':
		require('mobile_service/getcountries.php');
		$service=new GetCountries();
		break;
	
		//getcities
	case'getcities':
		require('mobile_service/getcities.php');
		$service=new GetCities();
		break;
	
		//registerproduct
	case'registerproduct':
		require('mobile_service/registerproduct.php');
		$service=new RegisterProduct();
		break;
	
		//getproducts
	case'getsmallproducts':
		require('mobile_service/getsmallproducts.php');
		$service=new GetProducts();
		break;
	
	case'getlargeproducts':
		require('mobile_service/getlargeproducts.php');
		$service=new GetProducts();
		break;
	
		//uploadprofileimage
	case'uploadprofileimage':
		require('mobile_service/uploadprofileimage.php');
		$service=new UploadProfileImage();
		break;
	
		//downloadproductimage
	case'downloadprofileimage':
		require('mobile_service/downloadprofileimage.php');
		$service=new DownloadProfileImage();
		break;
	
		//uploadproductimage
	case'uploadproductimage':
		require('mobile_service/uploadproductimage.php');
		$service=new UploadProductImage();
		break;
	
		//uploadprofilefile
	case'uploadfileforprofile':
		require('mobile_service/uploadfileforprofile.php');
		$service=new UploadFileForProfile();
		break;
	
		//writefeedback
	case'writefeedback':
		require('mobile_service/writefeedback.php');
		$service=new WriteFeedback();
		break;
	
		//getfeedback
	case'getfeedback':
		require('mobile_service/getfeedback.php');
		$service=new GetFeedback();
		break;

		//editproduct
	case'editproduct':
		require('mobile_service/editproduct.php');
		$service=new EditProduct();
		break;
	
		//deleteproduct
	case'deleteproduct':
		require('mobile_service/deleteproduct.php');
		$service=new DeleteProduct();
		break;
	
		//hotspotpage
	case'hotspot':
		require('mobile_service/hotspot.php');
		$service=new HotSpot();
		break;
	
		/*
		 *venueprofile
		 */
	case'venueprofile':
		require('mobile_service/venueprofile.php');
		$service=new VenueProfile();
		break;
	
	case'venuefollowers':
		require('mobile_service/venueprofile.php');
		$service=new VenueProfile();
		break;
	
		/*
		 *mingle
		 */
	case"mingle":
		require('mobile_service/mingle.php');
		$service=new Mingle();
		break;
	
		/*
		 *userinformation
		 */
		//getmyprofile
	case'myprofile':
		require('mobile_service/myprofile.php');
		$service=new MyProfile();
		break;
	
		//getmyfriends
	case'myfriends':
		require('mobile_service/myfriends.php');
		$service=new MyFriends();
		break;
	
		//getmyplaces
	case'myplaces':
		require('mobile_service/myplaces.php');
		$service=new MyPlaces();
		break;
	
	
		//uploadavatar
	case'uploadavatar':
		require('mobile_service/uploadavatar.php');
		$service=new UploadAvatar();
		break;
	
		/*
		 *prizes
		 */
	case'prizes':
		require('mobile_service/prizes.php');
		$service=new Prizes();
		break;
		
}


if ($service == null) {
  echo json_encode(array("status" => "error", "msg" => "This action doesn`t get executed."));
} else {
  $service->excute();

  echo $service->get_result();
}
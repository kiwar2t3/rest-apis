<?php

include 'shrinkService.php';

class Registration extends ShrinkService {

  function excute() {
    global $message_cls;
    $user_type = 'user';
	$user_name = tep_get_value_post('user_name', 'Name', 'require;');
	$user_email = tep_get_value_post('user_email', 'Email', 'require;');
    $user_password = tep_get_value_post('user_password', 'Password', 'require;');
	$user_phone = tep_get_value_post('user_phone', 'Phone', 'require;');
    $user_city	= tep_get_value_post('user_city', 'City', 'require');
	$user_postal = tep_get_value_post('user_postal', 'Postal', 'require;');
	$user_authtoken = '9d7f7b96a4ffad5be1a907dc9ca18129';

	$currentUserInfo = (array) teb_one_query(TABLE_USERS, 'user_authtoken = "' . $user_authtoken . '"');
	$user_id = $currentUserInfo['user_id'];
	// tep_unique_check(TABLE_USERS, array('user_email' => $user_email), "1=1", 'user_email', 'Email');

    if ($message_cls->is_empty_error()) {
      $user = array(
          'user_type' => $user_type,
          'user_email' => $user_email,
          'user_password' => tep_encrypt_password($user_password),
          'user_name' => $user_name,
          'user_phone' => $user_phone,
          'user_city' => $user_city,
          'user_postal' => $user_postal,
      );

      $result = tep_db_perform(TABLE_USERS, $user, 'update', "user_id='" . $user_id . "'");
	  if ($result > 0) {
		$user_avatar = upload_avatar($user_id, $user_email, "user_avatar");

        if ($user_avatar != '') {
          tep_db_perform(TABLE_USERS, array("user_avatar" => getUploadFileRelativePath($user_avatar)), "update", "user_id='" . $user_id . "'");
        } else {
          tep_db_perform(TABLE_USERS, array("user_avatar" => getUploadFileRelativePath(DEFAULT_MALE_AVATAR)), "update", "user_id='" . $user_id . "'");
        }

        $email_subject = "Wecome to " . SITE_TITLE . "!";

        $email_body = "Hello <br />";
        $email_body = "You are successfully registered.";
        //$email_body	= "If your accout will complate, click here:";
        //$email_body.= "<a href='".HTTP_CATALOG_SERVER."mobile_service.php?action=verifiedaccount&user_id=".$user_id."'>".HTTP_CATALOG_SERVER."mobile_service.php?action=verifiedaccount&user_id=".$user_id."</a>";
        //tep_phpmail($user_fullname, $user_email, $email_subject, $email_body);
		$query = 'select user_avatar from ' . TABLE_USERS . ' where user_id = ' . $user_id;
		$profileInfo = tep_db_query($query);
		$result = array();
		$result['Profileid'] = $user_id;
		$result['name'] = $user_name;
		$result['description'] = 'Successfully created';
		$result['avatarUrls'] = $profileInfo[0] -> user_avatar;
		$this->_result = $result;
      } else {
        $this->set_error(ERROR_SERVER_PROBLEM);
      }
    } else {
      $this->set_error($message_cls->get_all_message());
    }
  }

}

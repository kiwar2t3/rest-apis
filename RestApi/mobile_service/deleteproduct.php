<?php
include 'shrinkService.php';

class DeleteProduct extends ShrinkService {
	function excute() {
		global $message_cls;

		$this->check_login_user();
		
		$id = tep_get_value_post('id', 'ID', 'require');
		
		if ($message_cls->is_empty_error()) {
			teb_delete_query(TABLE_PRODUCTS, array("id"=>$id));
		
			teb_delete_query(TABLE_PRODUCT_IMAGES, array("product_id"=>$id));
			
			$this->_result	= "ok";
		}else{
			$this->set_error($message_cls->get_all_message());
		}
		
	}	
}
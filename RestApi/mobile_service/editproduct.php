<?php
include 'shrinkService.php';

class EditProduct extends ShrinkService {
	function excute() {
		global $message_cls, $upload_img_path;

		$this->check_login_user();
		$id				= tep_get_value_post('id', 'ID', '');
		$category_id	= tep_get_value_post('category_id', 'Category ID', 'require;');
		$title		 	= tep_get_value_post('title', 'Title', 'require;');
		$comment		= tep_get_value_post('comment', 'Comment', '');
		$price			= tep_get_value_post('price', 'Price', '');
		$address		= tep_get_value_post('address', 'Address', '');
		$latitude		= tep_get_value_post('latitude', 'Latitude', '');
		$longitude		= tep_get_value_post('longitude', 'Longitude', '');
		$user_id		= $this->_userid;
		
		
		$imagecount = PRODUCT_IMAGE_COUNT;
		$image_original	= array("","","","");
		$image_web		= array("","","","");
		$image_web_thumb= array("","","","");
		$image_mobile		= array("","","","");
		$image_mobile_thumb= array("","","","");
		for($ii=0;$ii<$imagecount;$ii++){
			$image_upload = upload_file($title, 'image'.$ii, false);								
			if ($image_upload != '' && $upload_img_path != '') {
				$image_original[$ii]	= $image_upload;
				$image_web[$ii]			= formated_image($image_original[$ii], $upload_img_path, 600, 400);
				$image_web_thumb[$ii]	= formated_image($image_original[$ii], $upload_img_path, 187, 105, true);
				$image_mobile[$ii]		= formated_image($image_original[$ii], $upload_img_path, 320, 215);
				$image_mobile_thumb[$ii]	= formated_image($image_original[$ii], $upload_img_path, 100, 100);			
			}
			$image_upload = '';
			$upload_img_path = '';
		}
		
		if ($message_cls->is_empty_error()) {
			$product = array(
					'category_id'			=> $category_id,
					'title'				=> $title,
					"comment"		=> $comment,
					"price"		=> $price,
					"address"	=> $address,
					"longitude"	=> $longitude,
					"latitude"	=> $latitude,
					"user_id"	=> $user_id
			);
			if($id==''){
				$product['created'] = tep_now_datetime();
				$result = tep_db_perform(TABLE_PRODUCTS, $product, 'insert');
				if ($result > 0) {
					$id = tep_db_insert_id();
					$pimages = array();
					for($ii=0; $ii<$imagecount; $ii++){
						if($image_original[$ii]!=''){
							$product_image = array(
								'product_id'			=> $id,
							);
							$product_image['image_original']		= getUploadFileRelativePath($image_original[$ii]);
							$product_image['image_web']			= getUploadFileRelativePath($image_web[$ii]);
							$product_image['image_web_thumb']	= getUploadFileRelativePath($image_web_thumb[$ii]);
							$product_image['image_mobile']		= getUploadFileRelativePath($image_mobile[$ii]);
							$product_image['image_mobile_thumb']= getUploadFileRelativePath($image_mobile_thumb[$ii]);
							$product_image['image_index'] = $ii;
							$result = tep_db_perform(TABLE_PRODUCT_IMAGES, $product_image, 'insert');
							$pimages[] = $product_image;
						}
					}
					$product['id'] = $id;
					$this->_result = $product;
					$this->_result['images'] = $pimages;
					$this->_result['status']		= "success";
					$this->_result['msg']			= "ok";
				}else{
					$this->set_error(ERROR_SERVER_PROBLEM);
				}
			}else{
				$result = tep_db_perform(TABLE_PRODUCTS, $product, 'update', "id='".$id."'");
				//teb_delete_query(TABLE_PRODUCT_IMAGES, array("product_id"=>$id));
				if ($result > 0) {
					$pimages = array();
					for($ii=0; $ii<$imagecount; $ii++){
						if($image_original[$ii]!=''){
							teb_delete_query(TABLE_PRODUCT_IMAGES, array("product_id"=>$id,"image_index"=>$ii));
							$product_image = array(
								'product_id'			=> $id,
							);
							$product_image['image_original']		= getUploadFileRelativePath($image_original[$ii]);
							$product_image['image_web']			= getUploadFileRelativePath($image_web[$ii]);
							$product_image['image_web_thumb']	= getUploadFileRelativePath($image_web_thumb[$ii]);
							$product_image['image_mobile']		= getUploadFileRelativePath($image_mobile[$ii]);
							$product_image['image_mobile_thumb']= getUploadFileRelativePath($image_mobile_thumb[$ii]);
							$product_image['image_index'] = $ii;
							$result = tep_db_perform(TABLE_PRODUCT_IMAGES, $product_image, 'insert');
							$pimages[] = $product_image;
						}
					}
					$product['id'] = $id;
					$this->_result = $product;
					$this->_result['images'] = $pimages;
				}else{
					$this->set_error(ERROR_SERVER_PROBLEM);
				}
			}
		
			
		} else {
			$this->set_error($message_cls->get_all_message());
		}
	}	
}
<?php
include 'snipstampService.php';

class ResetPassword extends SnipstampServce {
	function excute() {
		global $message_cls;
		
		$user_name		= tep_get_value_get('username');
		$user_session	= tep_get_value_get('key');
		
		$this->get_login_user_name($user_name);
		
		if ($this->_userid == 0) {
			exit(0);
		} else {
			$user_email	= $this->_user -> user_email;
			$user_name	= $this->_user -> user_name;
			
			if ($this->_user -> user_session == $user_session) {
				$new_password = tep_generator_password(4, false,false,false);
				
				tep_excute_query("update " . TABLE_USERS . " set user_password='" . tep_encrypt_password($new_password) . "' where user_id='" . $this->_userid . "'");
				
				$email_subject = "Succed reset password.";
				
				$email_body	= "Hellow " . $user_name . ".<br />";
				$email_body	= "Send to you new password: " . $new_password . "<br/><br/>";
				$email_body.= "When after login, please use the this password.";
			} else {
				$email_subject = "Failed reset password.";
				
				$email_body	= "Hellow " . $user_name . ".<br />";
				$email_body.= "Your inputed key is incorrect. Please contact manager.";
			}
			
			tep_phpmail($user_name, $user_email, $email_subject, $email_body);
			
			echo "Please check your email.";
			
			exit(0);
		}
	}	
}
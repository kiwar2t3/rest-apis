<?php

abstract class ShrinkService {

  var $_user;
  var $_userid = "";
  var $_result;

  function __construct() {
    $this->_result = new stdClass();
  }

  function get_userid($user_id) {
    $this->_user = teb_one_query(TABLE_USERS, array("user_id" => $user_id));

    if (empty($this->_user))
      return;

    $this->_userid = $this->_user->user_id;
  }

  function check_login_user() {
    //$user_id		= tep_get_value_post('user_id', 'UserID', 'require;');
    $user_authtoken = tep_get_value_post('auth_token', 'Auth Token', 'require');

    $this->check_valudations();

    $this->_user = teb_one_query(TABLE_USERS, array("user_authtoken" => $user_authtoken));

    if (empty($this->_user)) {
      $this->set_error("Unregistered user.");

      echo $this->get_result();

      exit(0);
    }

    $this->_userid = $this->_user->user_id;
  }

  function get_login_user_name($user_name) {
    $this->_user = teb_one_query(TABLE_USERS, array("user_name" => $user_name));

    if (empty($this->_user)) {
      return;
    }

    $this->_userid = $this->_user->user_id;
  }

  function get_login_user_email($user_email) {
    $this->_user = teb_one_query(TABLE_USERS, array("user_email" => $user_email));

    if (empty($this->_user)) {
      return;
    }

    $this->_userid = $this->_user->user_id;
  }

  function set_error($error_msg) {
    $this->_result->status = "error";
    $this->_result->result = $error_msg;
  }

  function check_valudations() {
    global $message_cls;

    if (!$message_cls->is_empty_error()) {
      $this->set_error($message_cls->get_all_message());
      echo json_encode($this->_result);
      exit(0);
    }
  }

  abstract function excute();

  function get_result() {
    $result = new stdClass();
    if (isset($this->_result->status) && $this->_result->status == 'error')
      $result = $this->_result;
    else {
      $result->status = 'success';
      $result->result = $this->_result;
    }

    return json_encode($result);
  }

  function _generateAuthToken() {
    return md5(uniqid());
  }

  function getCountryName($code) {
    if ($code == null || $code == '')
      return "";
    $country = teb_one_query(TABLE_COUNTRIES, array("country_code" => $code));
    if ($country != FALSE)
      return $country->country_name;
    else
      return "";
  }

  function getStateName($code, $country) {
    if ($code == null || $code == '')
      return "";
    $state = teb_one_query(TABLE_STATES, array("cd" => $code, "country_code" => $country));
    if ($state != FALSE)
      return $state->name;
    else
      return "";
  }

}

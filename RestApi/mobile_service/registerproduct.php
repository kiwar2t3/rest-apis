<?php

include 'shrinkService.php';
class RegisterProduct extends ShrinkService {

  function excute() {
    global $message_cls;

    $product_name = tep_get_value_post('product_name', 'Name', 'require');
    $price = tep_get_value_post('price', 'Price', 'require;');

    $weight = tep_get_value_post('weight', 'Weight', 'require;');
    $quantity = tep_get_value_post('quantity', 'Quantity', 'require;');
    $is_alert = tep_get_value_post('is_alert', 'IsAlert', 'require;');
    $is_favorite = tep_get_value_post('is_favorite', 'IsFavorite', 'require;');
	$product_details = tep_get_value_post('product_details', 'ProductDetails', 'require;');
	$personal_notes = tep_get_value_post('personal_notes', 'PersonalNotes', 'require;');

    tep_unique_check(TABLE_PRODUCTS, array('product_name' => $product_name, 'price' => $price, 'weight' => $weight, 'quantity' => $quantity), "1=1", 'product_name', 'Product');


    if ($message_cls->is_empty_error()) {
        $product = array(
		   'product_name' => $product_name,
		   'price' => $price,
		   'weight' => $weight,
		   'quantity' => $quantity,
		   'is_alert' => $is_alert,
		   'is_favorite' => $is_favorite,
		   'product_details' => $product_details,
		   'personal_notes' => $personal_notes,
        );
        $result = tep_db_perform(TABLE_PRODUCTS, $product, 'insert');
        if ($result > 0) {
			$product_id = tep_db_insert_id();
			$product_image = upload_product_image($product_id, $product_name, 'product_image');
			$image = array(
				'image_original' => $product_image['original'],
				'image_small' => $product_image['small'],
				'image_large' => $product_image['large']
			
			);
			$imageResult = tep_db_perform(TABLE_IMAGES, $image, 'insert');
			$image_id = tep_db_insert_id();
			$updateQuery = 'UPDATE ' . TABLE_PRODUCTS . ' SET image_id = ' . $image_id . ' WHERE product_id = ' . $product_id . ';';
			tep_excute_query($updateQuery);
			$this->_result = teb_one_query(TABLE_PRODUCTS, array("product_id" => $product_id));
        } else {
		    $this->set_error(ERROR_SERVER_PROBLEM);
        }
    } else {
      $this->set_error($message_cls->get_all_message());
    }
  }

}

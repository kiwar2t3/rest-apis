<?php
include 'shrinkService.php';

class GetStore extends ShrinkService {
	function excute() {
	
                if(tep_get_value_post('auth_token'))
                    $this->check_login_user(); 
                
                $store_id = tep_get_value_post('store_id', 'Store ID', 'require');
                $store = teb_one_query(TABLE_STORES, array("store_id"=>$store_id));
                
                if($store){
			$store['image_original'] = getUploadFileAbsolutePath($store['image_original']);
			$store['image_thumb'] = getUploadFileAbsolutePath($store['image_thumb']);
                        $store['logo'] = getUploadFileAbsolutePath($store['logo']);
                        
                        if(tep_get_value_post('auth_token')){
                            $favorite_store = teb_one_query(TABLE_FAVORITESTORES, array("store_id"=>$store['store_id'], "user_id"=>$this->_userid));
                            if($favorite_store){
                                $store['favorite'] = TRUE;                                
                            }else{
                                $store['favorite'] = FALSE;
                            }                            
                        }
                        
			$this->_result = $store;
		}else{
                        $this->set_error('Not Found');
                }		
	}
}
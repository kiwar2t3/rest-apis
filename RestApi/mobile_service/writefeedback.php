<?php
include 'shrinkService.php';

class WriteFeedback extends ShrinkService {
	function excute() {
                
		global $message_cls;
		
		$user_id        = 0;
		$details	= tep_get_value_post('details', 'Details', 'require;');
		if ( tep_get_value_post('auth_token', 'ID', '') != "" ) {
			$this->check_login_user();
			$user_id	= $this->_userid;
		}		
		
		if ($message_cls->is_empty_error()) {
			
			$feedback_ = array(
					'user_id'	=> $user_id,
					"details"	=> $details,
					'datetime'   => tep_now_datetime(),
			);
			$result = tep_db_perform(TABLE_FEEDBACKS, $feedback_, 'insert');
			if ($result > 0) {
				$id = tep_db_insert_id();
				$feedback_['id'] = $id;
				$this->_result = $feedback_;
			}else{
				$this->set_error(ERROR_SERVER_PROBLEM);
			}
		}else {
			$this->set_error($message_cls->get_all_message());
		}
                		
	}
}
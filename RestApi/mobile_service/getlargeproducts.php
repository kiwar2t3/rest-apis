<?php
include 'shrinkService.php';

class GetProducts extends ShrinkService {
	function excute() {
		$product_name       = tep_get_value_post('product_name', 'Product Name', 'require');
		$this->_result = array();
		
		$condition = "product_name like '%" . $product_name . "%'";
		$query = "select image_id, product_name, price, weight, quantity, is_alert, is_favorite, product_details, personal_notes from " . TABLE_PRODUCTS . " where " . $condition . " order by product_id desc;";
		
		$products = tep_db_query($query);
		foreach ($products as &$product) {
			$productimages = tep_db_query("select * from " . TABLE_IMAGES . " where image_id = " . $product -> image_id);
			$images = array();
			foreach ( $productimages as $pimage ) {
				$array = array();
				$array['image_small'] = getUploadFileAbsolutePath($pimage -> image_small);
				$array['image_large'] = getUploadFileAbsolutePath($pimage -> image_large);
				$images[] = $array;
			}
			$product -> images = $images;
		}
		$this->_result = $products;
	}
}
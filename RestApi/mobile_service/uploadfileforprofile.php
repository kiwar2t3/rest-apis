<?php
include 'shrinkService.php';

class UploadFileForProfile extends ShrinkService {
	function excute() {
		global $message_cls;
		
		$profile_id = tep_get_value_post('profile_id', 'ProfileId', 'require;');
		if ($message_cls->is_empty_error()) {
			$profile_info = teb_one_query(TABLE_USERS, array("user_id" => $profile_id));
			$profile_file = upload_profile_file($profile_id, $profile_info -> user_email, 'profile_file');
			$file = array(
				'path' => $profile_file['file_url'],
				'type' => $profile_file['type']
			);
			$file_result = tep_db_perform(TABLE_FILES, $file, 'insert');
			$file_id = tep_db_insert_id();
			$updateQuery = 'UPDATE ' . TABLE_USERS . ' SET file_id = ' . $file_id . ' WHERE user_id = ' . $profile_id . ';';
			tep_excute_query($updateQuery);
			$this->_result = teb_one_query(TABLE_FILES, array("id" => $file_id));
			
		}else{
			$this->set_error($message_cls->get_all_message());
		}
	}	
}
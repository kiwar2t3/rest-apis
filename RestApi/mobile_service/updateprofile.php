<?php
include 'shrinkService.php';

class EditProfile extends ShrinkService {
	function excute() {
		global $message_cls;
		
		$this->check_login_user();
		
		$user_type 			= 'user';
		$user_name  		= tep_get_value_post('user_name', 'Name', 'require;');
		$user_email			= tep_get_value_post('user_email', 'Email', 'require;email;');
		$user_password  	= tep_get_value_post('user_password', 'Password', '');
		$user_phone			= tep_get_value_post('user_phone', 'Phone', '');
		$user_postal      	= tep_get_value_post('user_postal', 'PostalCode', '');
        	
		//tep_unique_check(TABLE_USERS, array('user_email'=>$user_email),	"user_id!='".$user_id."'", 'user_email', 'Email');
		
		//if ($user_zip != '') {
		//	tep_zipcode_check($user_country, $user_state, $user_city, $user_zip, "user_zip", "ZipCode");
		//}
		
		if ($message_cls->is_empty_error()) {
			$user = array(
				'user_name'		=> $user_name,
				'user_email'	=> $user_email,					
				'user_password'	=> $user_password,
				'user_phone'	=> $user_phone,
				'user_postal'	=> $user_postal,
			);
			if($user_password!='')
				$user['user_password']  = tep_encrypt_password($user_password);
			$result = tep_db_perform(TABLE_USERS, $user, 'update', 'user_id='.$this->_userid);
			if ($result > 0) {
				$user_id = $this->_userid;
		
				$user_avatar = upload_avatar($user_id, $user_name, "user_avatar");
		
				if ($user_avatar != '') {
					tep_db_perform(TABLE_USERS, array("user_avatar"=>getUploadFileRelativePath($user_avatar)), "update", "user_id='".$user_id."'");
				} else {
					//tep_db_perform(TABLE_USERS, array("user_avatar"=>($user_sex == 'Male' ? getUploadFileRelativePath(DEFAULT_MALE_AVATAR) : getUploadFileRelativePath(DEFAULT_FEMALE_AVATAR))), "update", "user_id='".$user_id."'");
				}
				$query = 'select user_avatar from ' . TABLE_USERS . ' where user_id = ' . $user_id;
				$profileInfo = tep_db_query($query);
				$result = array();
				$result['Profileid'] = $user_id;
				$result['name'] = $user_name;
				$result['description'] = 'Successfully UPDATED';
				$result['avatarUrls'] = $profileInfo[0] -> user_avatar;
				$this->_result = $result;
				
			} else {
				$this->set_error(ERROR_SERVER_PROBLEM);
			}	
		} else {
			$this->set_error($message_cls->get_all_message());
		}
	}	
}
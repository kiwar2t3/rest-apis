<?php
include 'shrinkService.php';

class ForgotPassword extends ShrinkService {
	function excute() {
		global $message_cls;
		
		$user_name		= tep_get_value_post('user_name', 'Name', 'require;');
		
		if ($message_cls->is_empty_error()) {
			$this->get_login_user_name($user_name);
		
			if ($this->_userid == 0) {
				$this->set_error("The username is incorrect.");
			} else {
				$user_session	= tep_session_id();
				$user_email		= $this->_user -> user_email;
				$user_fullname	= $this->_user -> user_name;
				
				$email_body	= "Hellow " . $user_fullname . ".<br />";
				$email_body .= "If you want to reset password, please click under link:<br />";
				$email_body .= "<a href='".HTTP_CATALOG_SERVER."mobile_service.php?action=resetpassword&username=" . $user_name . "&key=".$user_session . "'>Reset password</a><br /><br />";
				$email_body .= "Pleass refresh mail afther cliked link, so you will receive new passowrd.";
				
				if (tep_phpmail($user_fullname, $user_email, "Forgot password.", $email_body)) {
					$user_update = array(
						"user_session"	=> $user_session,
					);
					
					$result = tep_db_perform(TABLE_USERS, $user_update, "update", "user_id='" . $this->_userid ."'");
							
					if ($result > 0) {
						$this->_result['status']		= "success";
						$this->_result['msg']			= "Successed login.";
						$this->_result['user_email']	= $user_email;
					} else {
						$this->set_error(ERROR_SERVER_PROBLEM);
					}
				} else {
					$this->set_error("Failed send email to you. Try again.");
				}
			}	
		} else {
			$this->set_error($message_cls->get_all_message());
		}
	}	
}
<?php

include 'shrinkService.php';

class UpdateDiscount extends ShrinkService {

  function excute() {
    global $message_cls;
	$product_id = tep_get_value_post('product_id', 'ProductId', 'require;');
	$discount_id = tep_get_value_post('discount_id', 'DiscountId', 'require;');
	$details = tep_get_value_post('details', 'Details', 'require;');
	if ($message_cls->is_empty_error()) {
		$discount = array(
			'details' => $details
		);
      $result = tep_db_perform(TABLE_DISCOUNT, $discount, 'update', array('id' => $discount_id));
	  if ($result > 0) {
		$image = upload_discount_image($discount_id, 'discount_image');
		$updateDiscountQuery = 'UPDATE ' . TABLE_DISCOUNT . ' SET image_url = "' . $image . '" WHERE id = ' . $discount_id . ';';
		tep_excute_query($updateDiscountQuery);
		$updateProductQuery = 'UPDATE ' . TABLE_PRODUCTS . ' SET discount_id = ' . $discount_id . ' WHERE product_id = ' . $product_id . ';';
		tep_excute_query($updateProductQuery);
		$this->_result = teb_one_query(TABLE_DISCOUNT, array("id" => $discount_id));
      } else {
        $this->set_error(ERROR_SERVER_PROBLEM);
      }
    } else {
      $this->set_error($message_cls->get_all_message());
    }
  }

}

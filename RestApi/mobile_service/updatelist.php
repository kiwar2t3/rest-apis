<?php
include 'shrinkService.php';

class UpdateList extends ShrinkService {
	function excute() {
		global $message_cls;
		$list_id = tep_get_value_post('list_id', 'ListId', 'require;');
		$store_name = tep_get_value_post('store_name', 'Name', 'require;');
		$items_count = tep_get_value_post('items_count', 'ItemCount', 'require;');
		$notes = tep_get_value_post('notes', 'Notes', 'require;');
		$is_shared	= tep_get_value_post('is_shared', 'IsShared', 'require;');
		$is_newcomments = tep_get_value_post('is_newcomments', 'IsNewComments', 'require;');
		
		if ($message_cls->is_empty_error()) {
			$list = array(
				'store_name' => $store_name,
				'items_count' => $items_count,
				'notes' => $notes,
				'is_shared' => $is_shared,
				'is_newcomments' => $is_newcomments,
				'is_updated' => 1
			);
			$result = tep_db_perform(TABLE_LISTS, $list, 'update', 'id=' . $list_id);
			if ($result > 0) {
				$result = array();
				$result['ListId'] = $list_id;
				$result['name'] = 'List Details Updated';
				$result['description'] = 'Successfully updated current list details';
				$this->_result = $result;
				
			} else {
				$this->set_error(ERROR_SERVER_PROBLEM);
			}	
		} else {
			$this->set_error($message_cls->get_all_message());
		}
	}	
}
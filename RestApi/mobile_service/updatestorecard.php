<?php

include 'shrinkService.php';

class UpdateStoreCard extends ShrinkService {

  function excute() {
    global $message_cls;
	
	$store_id = tep_get_value_post('store_id', 'StoreId', 'require;');
	$card_number = tep_get_value_post('card_number', 'CardNumber', 'require;');
	$card_type = tep_get_value_post('card_type', 'CardType', 'require;');
	
	if ($message_cls->is_empty_error()) {
		$storeCard = array(
			'store_id' => $store_id,
			'card_number' => $card_number,
			'card_type' => $card_type
		);
		$result = tep_db_perform(TABLE_STORECARDS, $storeCard, 'update' , 'store_id = ' . $store_id);
		if ($result > 0) {
			$result = array();
			$result['StoreId'] = $store_id;
			$result['name'] = 'Upadated Store Card';
			$result['description'] = 'Successfully Updated Store Card';
			$this -> _result = $result;
		} else {
			$this->set_error(ERROR_SERVER_PROBLEM);
		}
    } else {
		$this->set_error($message_cls->get_all_message());
    }
  }

}

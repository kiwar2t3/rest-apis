<?php

include 'shrinkService.php';

class Registration extends ShrinkService {

  function excute() {
    global $message_cls;

    $user_type = 'user';

    $user_email = tep_get_value_post('user_email', 'Email', 'require;email;');
    $user_password = tep_get_value_post('user_password', 'Password', 'require;');

    $user_city = tep_get_value_post('user_city', 'City', '');
    $user_postal = tep_get_value_post('user_postal', 'PostalCode', '');

    $user_address = tep_get_value_post('user_address', 'Address', '');
    $user_phone = tep_get_value_post('user_phone', 'Phone', '');

    //$user_device_token	= tep_get_value_post('user_device', 'DeviceToken', '');		

    tep_unique_check(TABLE_USERS, array('user_email' => $user_email), "1=1", 'user_email', 'Email');

    if ($message_cls->is_empty_error()) {
      $user = array(
          'user_type' => $user_type,
          'user_email' => $user_email,
          'user_password' => tep_encrypt_password($user_password),
          'user_city' => $user_city,
          'user_postal' => $user_postal,
          'user_address' => $user_address,
          'user_phone' => $user_phone,
          'user_created' => tep_now_datetime(),
          'user_modified' => tep_now_datetime(),
          'user_last_logined' => tep_now_datetime(),
          //'user_device_token'	=> $user_device_token,					
          'user_authtoken' => $this->_generateAuthToken(),
      );

      $result = tep_db_perform(TABLE_USERS, $user, 'insert');
      if ($result > 0) {
        $user_id = tep_db_insert_id();

        $user_avatar = upload_avatar($user_id, $user_email, "user_avatar");

        if ($user_avatar != '') {
          tep_db_perform(TABLE_USERS, array("user_avatar" => getUploadFileRelativePath($user_avatar)), "update", "user_id='" . $user_id . "'");
        } else {
          tep_db_perform(TABLE_USERS, array("user_avatar" => getUploadFileRelativePath(DEFAULT_MALE_AVATAR)), "update", "user_id='" . $user_id . "'");
        }

        $email_subject = "Wecome to " . SITE_TITLE . "!";

        $email_body = "Hello <br />";
        $email_body = "You are successfully registered.";
        //$email_body	= "If your accout will complate, click here:";
        //$email_body.= "<a href='".HTTP_CATALOG_SERVER."mobile_service.php?action=verifiedaccount&user_id=".$user_id."'>".HTTP_CATALOG_SERVER."mobile_service.php?action=verifiedaccount&user_id=".$user_id."</a>";
        //tep_phpmail($user_fullname, $user_email, $email_subject, $email_body);

        $this->_result = teb_one_query(TABLE_USERS, array("user_id" => $user_id));
        $this->_result->user_avatar = getUploadFileAbsolutePath($this->_result->user_avatar);
      } else {
        $this->set_error(ERROR_SERVER_PROBLEM);
      }
    } else {
      $this->set_error($message_cls->get_all_message());
    }
  }

}

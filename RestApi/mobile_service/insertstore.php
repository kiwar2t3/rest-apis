<?php

include 'shrinkService.php';

class InsertStore extends ShrinkService {

  function excute() {
    global $message_cls;
	
	$store_name = tep_get_value_post('store_name', 'Name', 'require;');
	$hours = tep_get_value_post('hours', 'Hours', 'require;');
	$notes = tep_get_value_post('notes', 'Notes', 'require;');
    $address = tep_get_value_post('address', 'Address', 'require;');
    $city	= tep_get_value_post('city', 'City', 'require;');
    $country	= tep_get_value_post('country', 'Country', 'require;');
    $postal_code	= tep_get_value_post('postal_code', 'PostalCode', 'require;');
    $phone	= tep_get_value_post('phone', 'Phone', 'require;');
	
	if ($message_cls->is_empty_error()) {
		$addressData = array(
			'address' => $address,
			'city' => $city,
			'country' => $country,
			'postal_code' => $postal_code,
			'phone' => $phone
		);
		$fieldArr = array_keys($addressData);
		$insertQuery = 'Insert into ' . TABLE_ADDRESS . ' (' . implode(',', $fieldArr) . ') Values(';
		foreach ( $addressData as $field => $val )
			$insertQuery .= "'" . $val . "', ";
		$insertQuery = substr($insertQuery, 0, -2) . ');';
		tep_excute_query($insertQuery);
		$getAddressIdSql = 'Select MAX(id) as address_id From ' . TABLE_ADDRESS . ';';
		$addressIdResult = tep_db_query($getAddressIdSql);
		$address_id = $addressIdResult[0] -> address_id;
		$address_id = tep_db_insert_id();
		$store = array(
			'address_id' => $address_id,
			'name' => $store_name,
			'hours' => $hours,
			'notes' => $notes
		);
		$result = tep_db_perform(TABLE_STORES, $store, 'insert');
		if ($result > 0) {
			$store_id = tep_db_insert_id();
			$result = array();
			$result['StoreId'] = $store_id;
			$result['name'] = 'New Store';
			$result['description'] = 'Successfully created New Store';
			$this -> _result = $result;
		} else {
			$this->set_error(ERROR_SERVER_PROBLEM);
		}
    } else {
		$this->set_error($message_cls->get_all_message());
    }
  }

}

<?php
include 'shrinkService.php';

class DeleteStoreCard extends ShrinkService {
	function excute() {
		global $message_cls;
		
		$store_id = tep_get_value_post('store_id', 'StoreId', 'require;');
		if ($message_cls->is_empty_error()) {
			// $address_id = $storeData -> address_id;
			$query = 'DELETE FROM ' . TABLE_STORECARDS . ' WHERE store_id = ' . $store_id . ';';
			tep_excute_query($query);
			$result = array();
			$result['description'] = 'Store Card is successfully deleted.';
			$this -> _result = $result;
			
		}else{
			$this->set_error($message_cls->get_all_message());
		}
	}	
}
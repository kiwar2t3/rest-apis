<?php

include 'shrinkService.php';

class EditList extends ShrinkService {

  function excute() {
    global $message_cls;
    // $user_type = 'user';
	$this -> check_login_user();
	$list_name = tep_get_value_post('list_name', 'ListName', 'require;');
	$store_name = tep_get_value_post('store_name', 'StoreName', 'require;');
    $items_count = tep_get_value_post('items_count', 'ItemCount', 'require;');
	$notes = tep_get_value_post('notes', 'Notes', 'require;');
    $is_shared	= tep_get_value_post('is_shared', 'IsShared', 'require;');
	$is_newcomments = tep_get_value_post('is_newcomments', 'IsNewComments', 'require;');
	if ($message_cls->is_empty_error()) {
		$list = array(
			'list_name' => $list_name,
			'store_name' => $store_name,
			'created_date' => tep_now_datetime(),
			'items_count' => $items_count,
			'notes' => $notes,
			'is_shared' => $is_shared,
			'is_newcomments' => $is_newcomments,
			'user_id' => $this -> _userid
		);
      $result = tep_db_perform(TABLE_LISTS, $list, 'insert');
	  if ($result > 0) {
		$list_id = tep_db_insert_id();
		$result = array();
		$result['ListId'] = $list_id;
		$result['name'] = 'New List Details';
		$result['description'] = 'Successfully created new List Details';
		$this->_result = $result;
      } else {
        $this->set_error(ERROR_SERVER_PROBLEM);
      }
    } else {
      $this->set_error($message_cls->get_all_message());
    }
  }

}

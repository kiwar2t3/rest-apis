<?php

include 'shrinkService.php';

class InsertUserSetting extends ShrinkService {

  function excute() {
    global $message_cls;
	$this -> check_login_user();
	$theme_color = tep_get_value_post('theme_color', 'ThemeColor', 'require;');
	$font_size = tep_get_value_post('font_size', 'FontSize', 'require;');
    if ($message_cls->is_empty_error()) {
		$userSetting = array(
			'user_id' => $this -> _userid,
			'theme_color' => $theme_color,
			'font_size' => $font_size
		);
      $result = tep_db_perform(TABLE_SETTINGS, $userSetting, 'insert');
	  if ($result > 0) {
		$list_id = tep_db_insert_id();
		$result = array();
		$result['UserId'] = $this -> _userid;
		$result['name'] = 'New User Settings';
		$result['description'] = 'Successfully created User Settings';
		$this->_result = $result;
      } else {
        $this->set_error(ERROR_SERVER_PROBLEM);
      }
    } else {
      $this->set_error($message_cls->get_all_message());
    }
  }

}

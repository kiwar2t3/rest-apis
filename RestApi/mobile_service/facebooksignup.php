<?php
include 'shrinkService.php';

class FacebookSignUp extends ShrinkService {
	function excute() {
		global $message_cls;

		$user_type = 'user';
		
		$user_facebook_id = tep_get_value_post('user_facebook_id', 'Facebook Account ID', 'require;');
		$user_email		= tep_get_value_post('user_email', 'Email', 'require;');		
		$user_password	= tep_encrypt_password(tep_rand());
		
		$user_fullname = tep_get_value_post('user_fullname', 'Name', 'require;');
		$user_address_zip = tep_get_value_post('user_address_zip', 'ZipCode', '');		
		$user_avatar = tep_get_value_post('user_avatar', 'Avatar', '');		
		//$user_device_token	= tep_get_value_post('user_device', 'DeviceToken', '');
                
		if ($message_cls->is_empty_error()) {
			$user_old = teb_one_query(TABLE_USERS, array("user_facebook_id"=>$user_facebook_id,'user_email'=>$user_email));
			if($user_old){
					$newauth = $this -> _generateAuthToken();
					$user_update = array(	
							'user_name'                                 => $user_fullname,
							'user_address_zip'                              => $user_address_zip,
							'user_avatar'                                   => $user_avatar,
							'user_last_logined'				=> tep_now_datetime(),
							"user_device_token"				=> $device_token,						
							'user_authtoken'				=> $newauth
					);

					$result = tep_db_perform(TABLE_USERS, $user_update, "update", "user_id='".$user_old -> user_id."'");
					if ($result > 0) {
						$this->_result = teb_one_query(TABLE_USERS, array("user_id"=>$user_old -> user_id));					
					} else {
						$this->set_error(ERROR_SERVER_PROBLEM);
					}					
					return;
			}
			
			$user_old = teb_one_query(TABLE_USERS, array('user_email'=>$user_email));
			if($user_old){
				$this->set_error('Email linked with your facebook account has already been registered');
				return;
			}
		}                
                
		
		if ($message_cls->is_empty_error()) {
			$user = array(
				'user_type'		=> $user_type,
				'user_facebook_id'	=> $user_facebook_id,
				'user_email'	=> $user_email,                            
				'user_password'	=> $user_password,
				'user_name'=> $user_fullname,
				'user_address_zip'		=> $user_zip,					
				'user_created'		=> tep_now_datetime(),
				'user_modified'		=> tep_now_datetime(),
				'user_last_logined'	=> tep_now_datetime(),
				'user_avatar'	=> $user_avatar,
				//'user_device_token'	=> $user_device_token,					
				'user_authtoken'	=> $this->_generateAuthToken(),
			);
		
			$result = tep_db_perform(TABLE_USERS, $user, 'insert');
			if ($result > 0) {
				$user_id = tep_db_insert_id();
		
				$email_subject = "Wecome to ".SITE_TITLE."!";
				
				$email_body	= "Hello <br />";
				$email_body = "You are successfully registered.";
				//$email_body	= "If your accout will complate, click here:";
				//$email_body.= "<a href='".HTTP_CATALOG_SERVER."mobile_service.php?action=verifiedaccount&user_id=".$user_id."'>".HTTP_CATALOG_SERVER."mobile_service.php?action=verifiedaccount&user_id=".$user_id."</a>";
				
				tep_phpmail($user_name, $user_email, $email_subject, $email_body);
				
				$this->_result = teb_one_query(TABLE_USERS, array("user_id"=>$user_id));
				
			} else {
				$this->set_error(ERROR_SERVER_PROBLEM);
			}	
		} else {
			$this->set_error($message_cls->get_all_message());
		}
	}	
}
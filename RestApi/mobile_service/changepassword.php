<?php
include 'shrinkService.php';

class ChangePassword extends ShrinkService {
	function excute() {
		global $message_cls;
		
		$this->check_login_user();
		$old_password		= tep_get_value_post('old_password', 'Old Password', 'require;');
		$new_password		= tep_get_value_post('new_password', 'New Password', 'require');
		
		if ($message_cls->is_empty_error()) {
			if (tep_validate_password($old_password, $this->_user['user_password'])) {
				$user_update = array(						
					'user_password'				=> tep_encrypt_password($new_password)
				);
				$result = tep_db_perform(TABLE_USERS, $user_update, "update", "user_id='".$this->_userid."'");					
				if ($result > 0) {
					$this->_result['status']		= "success";
					$this->_result['msg']			= "successfully changed password";
				}else{
					$this->set_error(ERROR_SERVER_PROBLEM);
				}
			}else{
				$this->set_error("The password you entered is incorrect.");
			}
		}else{
			$this->set_error($message_cls->get_all_message());
		}
		
	}	
}
<?php
require('../includes/admin_application_top.php');

$titlex = "Shopping Lists";

require(DIR_WS_INCLUDES . 'body_header.php');

$action = tep_db_prepare_input($_REQUEST['action']);
if ($action == 'delete') {
	$id = tep_get_value_get('id');
        $shopping = array(
            'status' => 'Deleted'
        );
	//teb_delete_query(TABLE_SHOPPINGS, array("id"=>$id));	
        tep_db_perform(TABLE_SHOPPINGS, $shopping, 'update', "id=".$id);
	tep_success_redirect("Successfully deleted shopping item.", "shoppings.php");
} elseif ($action == 'all_delete') {
	$shopping_ids = tep_get_value_post("shopping_ids");
        $shopping = array(
            'status' => 'Deleted'
        );
	for ($i = 0; $i < count($shopping_ids); $i ++) {
            //teb_delete_query(TABLE_SHOPPINGS, array("id"=>$shopping_ids[$i]));
            tep_db_perform(TABLE_SHOPPINGS, $shopping, 'update', "id=".$shopping_ids[$i]);
	}
	tep_success_redirect("Successfully deleted shopping items.", "shoppings.php");
} else if($action=='complete'){
    $id = tep_get_value_get('id');
    $shopping = array(
        'status' => 'Completed'
    );
    //teb_delete_query(TABLE_SHOPPINGS, array("id"=>$id));	
    tep_db_perform(TABLE_SHOPPINGS, $shopping, 'update', "id=".$id);
    tep_success_redirect("Successfully deleted shopping item.", "shoppings.php");
} else if($action=='all_complete'){
    $shopping_ids = tep_get_value_post("shopping_ids");
    $shopping = array(
        'status' => 'Completed'
    );
    for ($i = 0; $i < count($shopping_ids); $i ++) {
        //teb_delete_query(TABLE_SHOPPINGS, array("id"=>$shopping_ids[$i]));
        tep_db_perform(TABLE_SHOPPINGS, $shopping, 'update', "id=".$shopping_ids[$i]);
    }
    tep_success_redirect("Successfully deleted shopping items.", "shoppings.php");
} else if($action=='use'){
    $id = tep_get_value_get('id');
    $shopping = array(
        'status' => 'InUse'
    );
    //teb_delete_query(TABLE_SHOPPINGS, array("id"=>$id));	
    tep_db_perform(TABLE_SHOPPINGS, $shopping, 'update', "id=".$id);
    tep_success_redirect("Successfully deleted shopping item.", "shoppings.php");
} else if($action=='all_use'){
    $shopping_ids = tep_get_value_post("shopping_ids");
    $shopping = array(
        'status' => 'InUse'
    );
    for ($i = 0; $i < count($shopping_ids); $i ++) {
        //teb_delete_query(TABLE_SHOPPINGS, array("id"=>$shopping_ids[$i]));
        tep_db_perform(TABLE_SHOPPINGS, $shopping, 'update', "id=".$shopping_ids[$i]);
    }
    tep_success_redirect("Successfully deleted shopping items.", "shoppings.php");
}

$s_key      = tep_db_prepare_input($_REQUEST['s_key']);
$s_deal  = tep_db_prepare_input($_REQUEST['s_deal']);
$s_user     = tep_db_prepare_input($_REQUEST['s_user']);
$s_status   = tep_db_prepare_input($_REQUEST['s_status']);

?>

<?php if ($errors['db'] != ""): ?>
<p class="error"><?= $errors['db']?></p>
<?php endif; ?>

<form class="search_form" name="search_form" method="post">
	<div>
                User: <select name="s_user" onchange="this.form.submit()" style="width: 150px;">
			<option value="">-- All --</option>
		<?php $users = tep_db_query("select * from ".TABLE_USERS.""); while($user = tep_db_fetch_array($users)):?>
			<option value="<?= $user['user_id']?>" <?php if ($s_user == $user['user_id']) echo "selected"?>><?= $user['user_fullname']?></option>
		<?php endwhile;?>			
		</select>&nbsp;&nbsp;&nbsp;	
		Deal: <select name="s_deal" onchange="this.form.submit()" style="width: 150px;">
			<option value="">-- All --</option>
		<?php $deals = tep_db_query("select d.*,p.*,p.title as product_title,s.*,s.title as store_title from ".TABLE_DEALS." as d join (".TABLE_PRODUCTS." as p, ".TABLE_STORES." as s) on (d.product_id = p.id and d.store_id = s.store_id)"); 
                        while($deal = tep_db_fetch_array($deals)):
                ?>
        			<option value="<?= $deal['deal_id']?>" <?php if ($s_deal == $deal['deal_id']) echo "selected"?>><?= $deal['product_title'].' - '.$deal['store_title'].','.$deal['location_name'].' ('.$deal['start_date'].' ~ '.$deal['end_date'].')'?></option>
		<?php endwhile;?>					
		</select>&nbsp;&nbsp;&nbsp;	
                Status: <select name="s_status" onchange="this.form.submit()" style="width: 150px;">
			<option value="">-- All --</option>
                        <option value="InUse" <?php if ($s_status == "InUse") echo "selected"?>>In Use</option>
                        <option value="Completed" <?php if ($s_status == "Completed") echo "selected"?>>Completed</option>
                        <option value="Deleted" <?php if ($s_status == "Deleted") echo "selected"?>>Deleted</option>
		</select>&nbsp;&nbsp;&nbsp;
		<p>
			<input type="button" value="Add Shopping Item" onclick="location.href='shopping_edit.php'"/>
		</p>
	</div>
</form>

<script type="text/javascript">
<!--
function delete_new(id, title) {
	if (confirm("Are you sure want to delete "+title+"?")) {
		location.href = "shoppings.php?action=delete&id=" + id;
	}
}

function all_action() {
	if (confirm("Are you sure want to process?")) {
		document.dataListForm.submit();
	}
}
//-->
</script>

<form name="dataListForm" method="post" action="shoppings.php" style="margin-top: 15px;">
	<input type="hidden" name="s_deal" value="<?= $s_deal?>">
        <input type="hidden" name="s_user" value="<?= $s_user?>">
        <input type="hidden" name="s_status" value="<?= $s_status?>">
	<input type="hidden" name="s_key" value="<?= $s_key?>">
	
	With Selected Shopping Items: <select name="action" onchange="all_action()">
		<option value="">---</option>
                <option value="all_use">Use</option>
                <option value="all_complete">Complete</option>
		<option value="all_delete">Delete</option>
	</select>
	
<table class="contents_list" cellpadding="0" cellspacing="1">
<?php 
	$sort_column = "id";
	$sort_order = "asc";
	if (isset($_REQUEST['sort_column']))	$sort_column = tep_db_prepare_input($_REQUEST['sort_column']);
	if (isset($_REQUEST['sort_order']))	$sort_order = tep_db_prepare_input($_REQUEST['sort_order']);

	$table_headers = array();
	$table_headers[] = array('id'=>'', 'title'=>'<input type="checkbox" onchange="all_checkbox($(this))" />', 'width'=>'20');
	$table_headers[] = array('id'=>'id', 'title'=>'ID', 'width'=>'50');	
	$table_headers[] = array('id'=>'user_id', 'title'=>'User', 'width'=>'');
        $table_headers[] = array('id'=>'deal_id', 'title'=>'Deal', 'width'=>'');
	$table_headers[] = array('id'=>'created', 'title'=>'Created at', 'width'=>'200');
        $table_headers[] = array('id'=>'status', 'title'=>'Status', 'width'=>'200');
	$table_headers[] = array('id'=>'', 'title'=>'Action', 'width'=>'200');
	
	$column_count = count($table_headers);
	
	include DIR_WS_BOX.'table_header.php';
?>

<?
	$sql = "select * from " . TABLE_SHOPPINGS. " where 1=1";
	if ($s_key != '') {
		//$sql.= " and LOWER(shopping) like '%".strtolower($s_key)."%'";
	}	
	if ($s_deal != '') {
		$sql.= " and deal_id = '".$s_deal."'";
	}	
	if ($s_user != '') {
		$sql.= " and user_id = '".$s_user."'";
	}        
        if ($s_status != '') {
		$sql.= " and status = '".$s_status."'";
	}
	$sql .= " order by ".$sort_column." ".$sort_order;
	
	$list_split = new splitPageResults($sql);
	$shopping_list = tep_db_query($list_split->sql_query);
 
	$row = 0;
	while ($shopping = tep_db_fetch_array($shopping_list)) {
		$row ++;
		
		$ext_params = "&id=".$shopping['id']."&s_deal=".$s_deal."&s_user=".$s_user."&s_status=".$s_status."&s_key=".$s_key."&sort_column=".$sort_column."&sort_order=".$sort_order."&page=".$page;
?>	
<tbody>   
	<tr class='dataTableRow'>
		<td align="center">
			<input type="checkbox" name="shopping_ids[]" value="<?= $shopping['id']?>" class="all_check" />
		</td>
		<td align="center">
			<a class="link" href="shopping_edit.php?id=<?= $shopping['id']?>" title="View Detail"><?=$shopping['id']?></a>
		</td>
                <td align="center">
		<?php if ($shopping['user_id'] > 0) :?>
			<a class="link" href="user_edit.php?user_id=<?= $shopping['user_id']?>" title="View Detail">
				<?= teb_query("select user_fullname from ".TABLE_USERS." where `user_id`=".$shopping['user_id'], "user_fullname")?>
			</a>
		<?php endif;?>
		</td>
		<td align="center">
		<?php if ($shopping['deal_id'] > 0) :?>
			<a class="link" href="deal_edit.php?id=<?= $shopping['deal_id']?>" title="View Detail">
				<?php 
                                    $deals = tep_db_query("select d.*,p.*,p.title as product_title,s.*,s.title as store_title from ".TABLE_DEALS." as d join (".TABLE_PRODUCTS." as p, ".TABLE_STORES." as s) on (d.product_id = p.id and d.store_id = s.store_id) where d.deal_id = ".$shopping['deal_id']);
                                    while($deal = tep_db_fetch_array($deals)){
                                        echo $deal['product_title'].' - '.$deal['store_title'].','.$deal['location_name'].' ('.$deal['start_date'].' ~ '.$deal['end_date'].')';
                                        break;
                                    }
                                ?>
			</a>
		<?php endif;?>
		</td>		
		<td align="center">
			<?=$shopping['created']?>
		</td>	
                <td align="center">
			<?=$shopping['status']?>
		</td>	
		<td align="center">
                        <!--<a class="button" href="shopping_edit.php?id=<?= $shopping['id']?>" title="Edit">Edit</a>-->
                    <?php //if($shopping['status']=='InUse') { ?>
                        <a class="button" href="shoppings.php?action=use<?= $ext_params?>" title="Complete">Use</a>
                        <a class="button" href="shoppings.php?action=complete<?= $ext_params?>" title="Complete">Complete</a>
                        <a class="button" href="javascript:delete_new(<?= $shopping['id']?>, '')" title="Delete">Delete</a>        
                    <?php //} ?>
                </td>
	</tr>
<?php
	}
?>
</tbody>
<?php 
	$data_message = TEXT_DISPLAY_NUMBER_OF_SHOPPINGS;
	$empty_message = "No Shopping Item";
	include DIR_WS_BOX.'table_footer.php';
?>
</table>

</form>

<?php require(DIR_WS_INCLUDES . 'body_footer.php'); ?>
<?php
require('../includes/admin_application_top.php');

$titlex = "Feedbacks";

require(DIR_WS_INCLUDES . 'body_header.php');

?>

<?php if ($errors['db'] != ""): ?>
<p class="error"><?= $errors['db']?></p>
<?php endif; ?>


	
<table class="contents_list" cellpadding="0" cellspacing="1">
<?php 
	$sort_column = "created";
	$sort_order = "DESC";
	if (isset($_REQUEST['sort_column']))	$sort_column = tep_db_prepare_input($_REQUEST['sort_column']);
	if (isset($_REQUEST['sort_order']))		$sort_order = tep_db_prepare_input($_REQUEST['sort_order']);

	$table_headers = array();
	$table_headers[] = array('id'=>'user_id', 'title'=>'User', 'width'=>'200');
        $table_headers[] = array('id'=>'feedback', 'title'=>'Feedback', 'width'=>'');
	$table_headers[] = array('id'=>'created', 'title'=>'Created at', 'width'=>'170');
	
	$column_count = count($table_headers);
	
	include DIR_WS_BOX.'table_header.php';
?>

<?
	$sql = "select * from " . TABLE_FEEDBACKS;
	$sql .= " order by ".$sort_column." ".$sort_order;
	
	$list_split = new splitPageResults($sql);
	$feedbacks = tep_db_query($list_split->sql_query);
 
	$row = 0;
	while ($feedback = tep_db_fetch_array($feedbacks)) {
		$row ++;
		
?>	
<tbody>   
	<tr class='dataTableRow'>
		<td align="center">
                    <?php
                        if($feedback['user_id']){
                            $writer = teb_one_query(TABLE_USERS, array("user_id"=>$feedback['user_id']));
                            ?>
                            <a class="link" href="user_edit.php?user_id=<?= $writer['user_id']?>" title="View Detail"><b><?=$writer['user_fullname'];?></b></a>
                            <?php
                        }else{
                            echo 'Anonymous';
                        }
                    ?>
                </td>
		<td align="center"><?=$feedback['feedback']?></td>
		<td align="center"><?=$feedback['created']?></td>
	</tr>
<?php
	}
?>
</tbody>
<?php 
	$data_message = TEXT_DISPLAY_NUMBER_OF_FEEDBACKS;
	$empty_message = "No Feedbacks";
	include DIR_WS_BOX.'table_footer.php';
?>
</table>


<?php require(DIR_WS_INCLUDES . 'body_footer.php'); ?>
<?php
require('../includes/admin_application_top.php');

$titlex = "Create Product";

$imagecount = PRODUCT_IMAGE_COUNT;

$id = 0;
if (isset($_GET['id'])) {
    $titlex = "Product Edit";
    $id = tep_get_value_get("id");
}

require(DIR_WS_INCLUDES . 'body_header.php');

$category_id = 0;
$brand_id = 0;
//$store_id = 0;
$title = "";
$comment = "";

$upccode = "";
$size = "";
$units_in_package = "";
$origin = "";
//$featured = "";
//$price			= 0;
//$comment		= "";
//$address		= "";
//$longitude		= 0;
//$latitude		= 0;

$image_original = array("", "", "");
$image_thumb = array("", "", "");

if (isset($_POST["title"])) {
    $category_id = tep_get_value_post('category_id');
    $brand_id = tep_get_value_post('brand_id');
    //$store_id = tep_get_value_post('store_id');
    $title = tep_get_value_post('title', 'Title', 'require;length[1,200];');
    $comment = tep_get_value_post('comment', 'Description', '');
    $upccode = tep_get_value_post('upccode', 'Upc Code', '');
    $size = tep_get_value_post('size', 'Size', '');
    $units_in_package = tep_get_value_post('units_in_package', 'Units in Package', '');
    $origin = tep_get_value_post('origin', 'Origin', '');
    //$featured = tep_get_value_post('featured', 'Featured', '');    
    //$comment		= tep_get_value_post('comment', 'Description', '');
    //$price			= tep_get_value_post('price', 'Price', '');
    //$address		= tep_get_value_post('address', 'Address', '');
    //$longitude		= tep_get_value_post('longitude', 'Longitude', '');
    //$latitude		= tep_get_value_post('latitude', 'Latitude', '');

    for ($ii = 0; $ii < $imagecount; $ii++) {
        $image_mode     = tep_get_value_post('image_mode'.$ii, 'Image Mode', 'require;');
        $image_url      = tep_get_value_post('image_url'.$ii, 'Image URL', '');     
        
        if($image_mode=='upload'){
            $image_upload = upload_file($title, 'image' . $ii, false);
        }else if($image_mode=='url'){
            $image_upload = download_file($title, $image_url, false);
        }
        
        if ($image_upload != '' && $upload_img_path != '') {
            $image_original[$ii] = $image_upload;
            $image_thumb[$ii] = formated_image($image_original[$ii], $upload_img_path, 100, 100);
        }
        $image_upload = '';
        $upload_img_path = '';
    }
    if ($message_cls->is_empty_error()) {

        $product = array(
            'category_id' => $category_id,
            'brand_id' => $brand_id,
            //'store_id' => $store_id,
            'title' => $title,
            'comment' => $comment,
            'upccode' => $upccode,
            'size' => $size,
            'units_in_package' => $units_in_package,
            'origin' => $origin,
                //'featured' => $featured,
                //"comment"		=> $comment,
                //"price"		=> $price,
                //"address"	=> $address,
                //"longitude"	=> $longitude,
                //"latitude"	=> $latitude
        );

        if ($id == 0) {
            $product['created'] = tep_now_datetime();
            $result = tep_db_perform(TABLE_PRODUCTS, $product, 'insert');
            if ($result > 0) {
                $id = tep_db_insert_id();

                for ($ii = 0; $ii < $imagecount; $ii++) {
                    if ($image_original[$ii] != '') {
                        $product_image = array(
                            'product_id' => $id,
                        );
                        $product_image['image_original'] = getUploadFileRelativePath($image_original[$ii]);
                        $product_image['image_thumb'] = getUploadFileRelativePath($image_thumb[$ii]);
                        $product_image['image_index'] = $ii;
                        $result = tep_db_perform(TABLE_PRODUCT_IMAGES, $product_image, 'insert');
                    }
                }

                //tep_success_redirect("Success new registed product!", "product_edit.php?id=".$id);
            }
        } else {
            $result = tep_db_perform(TABLE_PRODUCTS, $product, 'update', "id='" . $id . "'");
            //teb_delete_query(TABLE_PRODUCT_IMAGES, array("product_id"=>$id));

            for ($ii = 0; $ii < $imagecount; $ii++) {
                if ($image_original[$ii] != '') {
                    teb_delete_query(TABLE_PRODUCT_IMAGES, array("product_id" => $id, "image_index" => $ii));
                    $product_image = array(
                        'product_id' => $id,
                    );
                    $product_image['image_original'] = getUploadFileRelativePath($image_original[$ii]);
                    $product_image['image_thumb'] = getUploadFileRelativePath($image_thumb[$ii]);
                    $product_image['image_index'] = $ii;
                    $result = tep_db_perform(TABLE_PRODUCT_IMAGES, $product_image, 'insert');
                }
            }
        }

        if ($result > 0) {
            //tep_success_redirect("Success saved product information!", "product_edit.php?id=".$id);
            tep_success_redirect("Success saved product information!", "products.php");
        } else {
            $error_db = "Faild register product.";
        }
    }
} elseif ($id > 0) {
    $product_info = teb_one_query(TABLE_PRODUCTS, array("id" => $id));

    $title = $product_info['title'];
    $comment = $product_info['comment'];
    $upccode = $product_info['upccode'];
    $size = $product_info['size'];
    $units_in_package = $product_info['units_in_package'];
    $origin = $product_info['origin'];    
    $category_id = $product_info['category_id'];
    $brand_id = $product_info['brand_id'];
    //$store_id = $product_info['store_id'];
    //$featured = $product_info['featured'];
    //$comment		= $product_info['comment'];	
    //$price		= $product_info['price'];	
    //$address		= $product_info['address'];	
    //$latitude		= $product_info['latitude'];	
    //$longitude		= $product_info['longitude'];	
    $created = $product_info['created'];

    $product_image_list = tep_db_query('SELECT * FROM ' . TABLE_PRODUCT_IMAGES . ' where product_id=' . $id);

    while ($product_image = tep_db_fetch_array($product_image_list)) {
        $image_original[$product_image['image_index']] = getUploadFileAbsolutePath($product_image['image_original']);
        $image_thumb[$product_image['image_index']] = getUploadFileAbsolutePath($product_image['image_thumb']);
    }
}
?>

<form name="ad_form" encType="multipart/form-data" method="post" autocomplete="off" class="edit_form" id="ADForm">
    <?php if (isset($error_db)): ?>
        <p class="error"><?= $error_db ?></p>
    <?php endif; ?>

    <table class="contents_edit" id="ad_basic">
        <tr>
            <td class="label" width="120px">Category *</td>
            <td class="edit">
                <select name="category_id" style="width: 300px;">
                    <option value="0" <?php if ($category_id == 0) echo "selected" ?>>-- Select Category --</option>
                    <?php $categories = tep_db_query("select * from " . TABLE_CATEGORIES . " where deleted <> 'Y' order by title");
                    while ($category = tep_db_fetch_array($categories)): ?>
                        <option value="<?= $category['category_id'] ?>" <?php if ($category_id == $category['category_id']) echo "selected" ?>><?= $category['title'] ?></option>
                    <?php endwhile; ?>
                </select>
            </td>
        </tr>
        <tr>
            <td class="label" width="120px">Brand *</td>
            <td class="edit">
                <select name="brand_id" style="width: 300px;">
                    <option value="0" <?php if ($brand_id == 0) echo "selected" ?>>-- Select Brand --</option>
                    <?php $brands = tep_db_query("select * from " . TABLE_BRANDS . " where deleted <> 'Y' order by title");
                    while ($brand = tep_db_fetch_array($brands)): ?>
                        <option value="<?= $brand['brand_id'] ?>" <?php if ($brand_id == $brand['brand_id']) echo "selected" ?>><?= $brand['title'] ?></option>
                    <?php endwhile; ?>
                </select>
            </td>
        </tr>
        <!--<tr>
            <td class="label" width="120px">Store *</td>
            <td class="edit">
                <select name="store_id" style="width: 300px;">
                    <option value="0" <?php if ($store_id == 0) echo "selected" ?>>-- Select Store --</option>
                    <?php $stores = tep_db_query("select * from " . TABLE_STORES . " where deleted <> 'Y' order by title");
                    while ($store = tep_db_fetch_array($stores)): ?>
                        <option value="<?= $store['store_id'] ?>" <?php if ($store_id == $store['store_id']) echo "selected" ?>><?= $store['title'].' - '.$store['location_name'] ?></option>
                    <?php endwhile; ?>
                </select>
            </td>
        </tr>-->
        <tr>
            <td class="label" width="120px">Title *</td>
            <td class="edit">
                <input type="text" name="title" id="title" value="<?= $title ?>" style="width: 400px;" class="validate[required,length[1-200]]" />
                <?php $message_cls->show_error('title') ?>
            </td>
        </tr>
        <tr>
                <td class="label">Description</td>
                <td class="edit">
                        <textarea name="comment" id="comment" style="width: 400px; height: 150px;" class=""><?= $comment?></textarea>
                        <?php $message_cls->show_error('comment')?>
                </td>
        </tr>
        <tr>
            <td class="label" width="120px">Upc Code</td>
            <td class="edit">
                <input type="text" name="upccode" id="upccode" value="<?= $upccode ?>" style="width: 400px;" class="" />
                <?php $message_cls->show_error('upccode') ?>
            </td>
        </tr>
        <tr>
            <td class="label" width="120px">Size</td>
            <td class="edit">
                <input type="text" name="size" id="size" value="<?= $size ?>" style="width: 400px;" class="" />
                <?php $message_cls->show_error('size') ?>
            </td>
        </tr>
        <tr>
            <td class="label" width="120px">Units in Package</td>
            <td class="edit">
                <input type="text" name="units_in_package" id="units_in_package" value="<?= $units_in_package ?>" style="width: 400px;" class="" />
                <?php $message_cls->show_error('units_in_package') ?>
            </td>
        </tr>
        <tr>
            <td class="label" width="120px">Origin</td>
            <td class="edit">
                <input type="text" name="origin" id="origin" value="<?= $origin ?>" style="width: 400px;" class="" />
                <?php $message_cls->show_error('origin') ?>
            </td>
        </tr>
        <tr>
            <td class="label">Image</td>
            <td class="edit">
                <?php if ($image_thumb[0] != ''): ?>
                    <a href="<?= $image_original[0] ?>" target="_image"><img src="<?= $image_thumb[0] ?>" /></a>
                <?php endif; ?>
                <input type="radio" name="image_mode0" value="upload" checked onclick="document.getElementById('image0').style.display='block'; document.getElementById('image_url0').style.display='none';" />Upload
                <input type="radio" name="image_mode0" value="url"  onclick="document.getElementById('image0').style.display='none'; document.getElementById('image_url0').style.display='block';" />URL<br>
                <input type="file" name="image0" id="image0" style="width:400px;">
                <input type="text" name="image_url0" id="image_url0" style="width:400px; " hidden>
                <?php $message_cls->show_error('image0') ?>
            </td>
        </tr>
        <tr>
            <td class="label"></td>
            <td class="edit">
                <?php if ($image_thumb[1] != ''): ?>
                    <a href="<?= $image_original[1] ?>" target="_image"><img src="<?= $image_thumb[1] ?>" /></a>
                <?php endif; ?>
                <input type="radio" name="image_mode1" value="upload" checked onclick="document.getElementById('image1').style.display='block'; document.getElementById('image_url1').style.display='none';" />Upload
                <input type="radio" name="image_mode1" value="url"  onclick="document.getElementById('image1').style.display='none'; document.getElementById('image_url1').style.display='block';" />URL<br>
                <input type="file" name="image1" id="image1" style="width:400px;">
                <input type="text" name="image_url1" id="image_url1" style="width:400px; " hidden>
                <?php $message_cls->show_error('image1') ?>
            </td>
        </tr>
        <tr>
            <td class="label"></td>
            <td class="edit">
                <?php if ($image_thumb[2] != ''): ?>
                    <a href="<?= $image_original[2] ?>" target="_image"><img src="<?= $image_thumb[2] ?>" /></a>
                <?php endif; ?>
                <input type="radio" name="image_mode2" value="upload" checked onclick="document.getElementById('image2').style.display='block'; document.getElementById('image_url2').style.display='none';" />Upload
                <input type="radio" name="image_mode2" value="url"  onclick="document.getElementById('image2').style.display='none'; document.getElementById('image_url2').style.display='block';" />URL<br>
                <input type="file" name="image2" id="image2" style="width:400px;">
                <input type="text" name="image_url2" id="image_url2" style="width:400px; " hidden>
                <?php $message_cls->show_error('image2') ?>
            </td>
        </tr>
        <!--<tr>
            <td class="label">Featured </td>
            <td class="edit">
                <input type="checkbox" name="featured" value="Y" <?php if($featured=='Y') echo 'checked'; ?>>                    
            </td>
        </tr>-->
        <?php if($id!=0) { ?>
        <tr>
            <td class="label">Created </td>
            <td class="edit">
                <?= $created ?>				
            </td>
        </tr>
        <?php } ?>
        <tr height="35px">
            <td class="label"></td>
            <td class="edit">
                <input type="submit" value="  Save " name="action" style="width:80px;"/>

                <input type="button" value="  Cancel " style="width:80px;" onclick="location.href = 'products.php'" />
            </td>
        </tr>	
    </table>
</form>

<?php require(DIR_WS_INCLUDES . 'body_footer.php'); ?>

<?php
require('../includes/admin_application_top.php');

$titlex = "Product Reviews";

require(DIR_WS_INCLUDES . 'body_header.php');

$action = tep_db_prepare_input($_REQUEST['action']);
if ($action == 'delete') {
	$id = tep_get_value_get('id');
	
	teb_delete_query(TABLE_REVIEWS, array("id"=>$id));	
	
	tep_success_redirect("Successfully deleted review.", "reviews.php");
} elseif ($action == 'all_delete') {
	$review_ids = tep_get_value_post("review_ids");
		
	for ($i = 0; $i < count($review_ids); $i ++) {
		teb_delete_query(TABLE_REVIEWS, array("id"=>$review_ids[$i]));
	}
	
	tep_success_redirect("Successfully deleted reviews.", "reviews.php");
}

$s_key = tep_db_prepare_input($_REQUEST['s_key']);
$s_deal = tep_db_prepare_input($_REQUEST['s_deal']);
$s_user = tep_db_prepare_input($_REQUEST['s_user']);

?>

<?php if ($errors['db'] != ""): ?>
<p class="error"><?= $errors['db']?></p>
<?php endif; ?>

<script type="text/javascript">
<!--
function delete_new(id, title) {
	if (confirm("Are you sure want to delete "+title+"?")) {
		location.href = "reviews.php?action=delete&id=" + id;
	}
}

function all_action() {
	if (confirm("Are you sure want to process?")) {
		document.dataListForm.submit();
	}
}
//-->
</script>

<form class="search_form" name="search_form" method="post">
	<div>
		Deal: <select name="s_deal" onchange="this.form.submit()" style="width: 150px;">
			<option value="">-- All --</option>
		<?php $deals = tep_db_query("select d.*,p.*,p.title as product_title,s.*,s.title as store_title from ".TABLE_DEALS." as d join (".TABLE_PRODUCTS." as p, ".TABLE_STORES." as s) on (d.product_id = p.id and d.store_id = s.store_id)"); 
                        while($deal = tep_db_fetch_array($deals)):
                ?>
        			<option value="<?= $deal['deal_id']?>" <?php if ($s_deal == $deal['deal_id']) echo "selected"?>><?= $deal['product_title'].' - '.$deal['store_title'].','.$deal['location_name'].' ('.$deal['start_date'].' ~ '.$deal['end_date'].')'?></option>
		<?php endwhile;?>			
		</select>&nbsp;&nbsp;&nbsp;		
		User: <select name="s_user" onchange="this.form.submit()" style="width: 150px;">
			<option value="">-- All --</option>
		<?php $users = tep_db_query("select * from ".TABLE_USERS.""); while($user = tep_db_fetch_array($users)):?>
			<option value="<?= $user['user_id']?>" <?php if ($s_user == $user['user_id']) echo "selected"?>><?= $user['user_fullname']?></option>
		<?php endwhile;?>			
		</select>&nbsp;&nbsp;&nbsp;	
		Search: <input type="text" name="s_key" value="<?= $s_key?>" />
		<input type="submit" value="Search" />&nbsp;&nbsp;&nbsp;
                <p>
                    <input type="button" value="Add Review" onclick="location.href='review_edit.php'"/>		
                </p>
	</div>
</form>



<form name="dataListForm" method="post" action="reviews.php" style="margin-top: 15px;">
	<input type="hidden" name="s_deal" value="<?= $s_deal?>">
        <input type="hidden" name="s_user" value="<?= $s_user?>">
	<input type="hidden" name="s_key" value="<?= $s_key?>">
	
	With Selected Reviews: <select name="action" onchange="all_action()">
		<option value="">---</option>
		<option value="all_delete">Delete</option>
	</select>
</form>
<table class="contents_list" cellpadding="0" cellspacing="1">
<?php 
	$sort_column = "id";
	$sort_order = "asc";
	if (isset($_REQUEST['sort_column']))	$sort_column = tep_db_prepare_input($_REQUEST['sort_column']);
	if (isset($_REQUEST['sort_order']))		$sort_order = tep_db_prepare_input($_REQUEST['sort_order']);

	$table_headers = array();
	$table_headers[] = array('id'=>'', 'title'=>'<input type="checkbox" onchange="all_checkbox($(this))" />', 'width'=>'50');
	$table_headers[] = array('id'=>'id', 'title'=>'ID', 'width'=>'100');
	$table_headers[] = array('id'=>'deal_id', 'title'=>'Deal', 'width'=>'200');
	$table_headers[] = array('id'=>'user_id', 'title'=>'User', 'width'=>'200');
	$table_headers[] = array('id'=>'mark', 'title'=>'Mark', 'width'=>'100');
        $table_headers[] = array('id'=>'review', 'title'=>'Review', 'width'=>'100');	
	$table_headers[] = array('id'=>'created', 'title'=>'Created at', 'width'=>'150');
	$table_headers[] = array('id'=>'', 'title'=>'Action', 'width'=>'200');
	
	$column_count = count($table_headers);
	
	include DIR_WS_BOX.'table_header.php';
?>

<?
	$sql = "select * from " . TABLE_REVIEWS. " where 1=1";
	if ($s_key != '') {
		$sql.= " and LOWER(review) like '%".strtolower($s_key)."%'";
	}
	
	if ($s_deal != '') {
		$sql.= " and deal_id = '".$s_deal."'";
	}
	
	if ($s_user != '') {
		$sql.= " and user_id = '".$s_user."'";
	}
	$sql .= " order by ".$sort_column." ".$sort_order;
	
	$list_split = new splitPageResults($sql);
	$review_list = tep_db_query($list_split->sql_query);
 
	$row = 0;
	while ($review = tep_db_fetch_array($review_list)) {
		$row ++;
		
		$ext_params = "&id=".$review['id']."&s_deal=".$s_deal."&s_user=".$s_user."&s_key=".$s_key."&sort_column=".$sort_column."&sort_order=".$sort_order."&page=".$page;
		
		
?>	
<tbody>   
	<tr class='dataTableRow'>
		<td align="center">
			<input type="checkbox" name="review_ids[]" value="<?= $review['id']?>" class="all_check" />
		</td>
		<td align="center">
			<a class="link" href="review_edit.php?id=<?= $review['id']?>" title="View Detail"><?=$review['id']?></a>
		</td>		
		<td align="center">
		<?php if ($review['deal_id'] > 0) :?>
			<a class="link" href="deal_edit.php?id=<?= $review['deal_id']?>" title="View Detail">
				<?php 
                                    $deals = tep_db_query("select d.*,p.*,p.title as product_title,s.*,s.title as store_title from ".TABLE_DEALS." as d join (".TABLE_PRODUCTS." as p, ".TABLE_STORES." as s) on (d.product_id = p.id and d.store_id = s.store_id) where d.deal_id = ".$review['deal_id']);
                                    while($deal = tep_db_fetch_array($deals)){
                                        echo $deal['product_title'].' - '.$deal['store_title'].','.$deal['location_name'].' ('.$deal['start_date'].' ~ '.$deal['end_date'].')';
                                        break;
                                    }
                                ?>
			</a>
		<?php endif;?>
		</td>
		<td align="center">
		<?php if ($review['user_id'] > 0) :?>
			<a class="link" href="user_edit.php?user_id=<?= $review['user_id']?>" title="View Detail">
				<?= teb_query("select user_fullname from ".TABLE_USERS." where `user_id`=".$review['user_id'], "user_fullname")?>
			</a>
		<?php endif;?>
		</td>
		<td align="center">
			<?=$review['mark']?>
		</td>	
                <td align="center">
			<?=$review['review']?>
		</td>	                
		<td align="center">
			<?=$review['created']?>
		</td>	
		<td align="center">
        	<a class="button" href="review_edit.php?id=<?= $review['id']?>" title="Edit">Edit</a>
        	<a class="button" href="javascript:delete_new(<?= $review['id']?>, '')" title="Delete">Delete</a>        
        </td>
	</tr>
<?php
	}
?>
</tbody>
<?php 
	$data_message = TEXT_DISPLAY_NUMBER_OF_REVIEWS;
	$empty_message = "No Product Reviews";
	include DIR_WS_BOX.'table_footer.php';
?>
</table>



<?php require(DIR_WS_INCLUDES . 'body_footer.php'); ?>
<?php
require('../includes/admin_application_top.php');

$titlex = "Create Favorite Item";

$id = 0;
if (isset($_GET['id'])) {
    $titlex = "Favorite Item Edit";
    $id = tep_get_value_get("id");
}

require(DIR_WS_INCLUDES . 'body_header.php');

$product_id = 0;
$user_id = 0;

if (isset($_POST["product_id"])) {
    $product_id = tep_get_value_post('product_id');
    $user_id = tep_get_value_post('user_id');

    if ($message_cls->is_empty_error()) {

        $favorite = array(
            'product_id' => $product_id,
            'user_id' => $user_id,
            'created' => tep_now_datetime(),
        );

        $favorite_db = teb_one_query(TABLE_FAVORITES, array("product_id"=>$product_id, "user_id"=>$user_id)); 
        if ($favorite_db) {
            $result = tep_db_perform(TABLE_FAVORITES, $favorite, 'update', "user_id='" . $user_id . "' and product_id='".$product_id."'");            
        } else {
            $result = tep_db_perform(TABLE_FAVORITES, $favorite, 'insert');
        }

        if ($result > 0) {
            //tep_success_redirect("Success saved product information!", "product_edit.php?id=".$id);
            tep_success_redirect("Success saved product review!", "favorites.php");
        } else {
            $error_db = "Faild register review.";
        }
    }
} elseif ($id > 0) {
    $favorite_info = teb_one_query(TABLE_FAVORITES, array("id" => $id));

    $product_id = $favorite_info['product_id'];
    $user_id = $favorite_info['user_id'];
    $mark = $favorite_info['mark'];
    $created = $favorite_info['created'];
}
?>

<form name="ad_form" encType="multipart/form-data" method="post" autocomplete="off" class="edit_form" id="ADForm">
    <?php if (isset($error_db)): ?>
        <p class="error"><?= $error_db ?></p>
    <?php endif; ?>

    <table class="contents_edit" id="ad_basic">
        <tr>
            <td class="label" width="120px">User *</td>
            <td class="edit">
                <select name="user_id" style="width: 300px;">
                    <option value="0" <?php if ($user_id == 0) echo "selected" ?>>-- Select User --</option>
                    <?php $users = tep_db_query("select * from " . TABLE_USERS . " order by user_fullname");
                    while ($user = tep_db_fetch_array($users)): ?>
                        <option value="<?= $user['user_id'] ?>" <?php if ($user_id == $user['user_id']) echo "selected" ?>><?= $user['user_fullname'] ?></option>
                    <?php endwhile; ?>
                </select>
            </td>
        </tr>
        <tr>
            <td class="label" width="120px">Product *</td>
            <td class="edit">
                <select name="product_id" style="width: 300px;">
                    <option value="0" <?php if ($product_id == 0) echo "selected" ?>>-- Select Product --</option>
                    <?php $products = tep_db_query("select * from " . TABLE_PRODUCTS . " order by title");
                    while ($product = tep_db_fetch_array($products)): ?>
                        <option value="<?= $product['id'] ?>" <?php if ($product_id == $product['id']) echo "selected" ?>><?= $product['title'] ?></option>
                    <?php endwhile; ?>
                </select>
            </td>
        </tr>
        <?php if($id!=0) { ?>
        <tr>
            <td class="label">Created </td>
            <td class="edit">
                <?= $created ?>				
            </td>
        </tr>
        <?php } ?>
        <tr height="35px">
            <td class="label"></td>
            <td class="edit">
                <input type="submit" value="  Save " name="action" style="width:80px;"/>

                <input type="button" value="  Cancel " style="width:80px;" onclick="location.href = 'favorites.php'" />
            </td>
        </tr>	
    </table>
</form>

<?php require(DIR_WS_INCLUDES . 'body_footer.php'); ?>
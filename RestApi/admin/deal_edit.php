<?php
require('../includes/admin_application_top.php');

$titlex = "Create Deal";

$id = 0;
if (isset($_GET['id'])) {
    $titlex = "Deal Edit";
    $id = tep_get_value_get("id");
}

require(DIR_WS_INCLUDES . 'body_header.php');

$product_id = 0;
$store_id = 0;
$regular_price = 0;
$sale_price = 0;
$savings = "";
$start_date = "";
$end_date = "";
$featured = "";
$card_required = "";


if (isset($_POST["product_id"])) {
    $product_id = tep_get_value_post('product_id', 'Product', 'require;');    
    $store_id = tep_get_value_post('store_id','Store', 'require;');
    $regular_price = tep_get_value_post('regular_price', 'Regular Price', '');
    $sale_price = tep_get_value_post('sale_price', 'Sale Price', '');
    $regular_price = tep_get_value_post('regular_price', 'Regular Price', '');
    $savings = tep_get_value_post('savings', 'Savings', '');
    $start_date = tep_get_value_post('start_date', 'Start Date', '');
    $end_date = tep_get_value_post('end_date', 'End Date', '');
    $featured = tep_get_value_post('featured', 'Featured', '');
    $card_required = tep_get_value_post('card_required', 'Card Required', '');

    if ($message_cls->is_empty_error()) {

        $deal = array(
            'product_id' => $product_id,
            'store_id' => $store_id,
            'regular_price' => $regular_price,
            'sale_price'    => $sale_price,
            'savings'       => $savings,
            'start_date'    => $start_date,
            'end_date'      => $end_date,
            'featured'      => $featured,
            'card_required' => $card_required
        );

        if ($id == 0) {
            $deal['created'] = tep_now_datetime();
            $result = tep_db_perform(TABLE_DEALS, $deal, 'insert');
            if ($result > 0) {
                $id = tep_db_insert_id();
            }
        } else {            
            $result = tep_db_perform(TABLE_DEALS, $deal, 'update', "deal_id='" . $id . "'");
        }

        if ($result > 0) {
            tep_success_redirect("Success saved deal information!", "deals.php");
        } else {
            $error_db = "Faild register deal.";
        }
    }
} elseif ($id > 0) {
    $deal_info = teb_one_query(TABLE_DEALS, array("deal_id" => $id));

    $product_id     = $deal_info['product_id'];
    $store_id       = $deal_info['store_id'];
    $regular_price  = $deal_info['regular_price'];
    $sale_price     = $deal_info['sale_price'];
    $savings        = $deal_info['savings'];
    $start_date     = $deal_info['start_date'];
    $end_date       = $deal_info['end_date'];
    $featured       = $deal_info['featured'];
    $card_required  = $deal_info['card_required'];
    $created = $deal_info['created'];

}
?>

<form name="ad_form" encType="multipart/form-data" method="post" autocomplete="off" class="edit_form" id="ADForm">
    <?php if (isset($error_db)): ?>
        <p class="error"><?= $error_db ?></p>
    <?php endif; ?>

    <table class="contents_edit" id="ad_basic">
        <tr>
            <td class="label" width="120px">Product *</td>
            <td class="edit">
                <select name="product_id" style="width: 300px;">
                    <option value="0" <?php if ($product_id == 0) echo "selected" ?>>-- Select Product --</option>
                    <?php $products = tep_db_query("select * from " . TABLE_PRODUCTS . " order by title");
                    while ($product = tep_db_fetch_array($products)): ?>
                        <option value="<?= $product['id'] ?>" <?php if ($product_id == $product['id']) echo "selected" ?>><?= $product['title'] ?></option>
                    <?php endwhile; ?>
                </select>
            </td>
        </tr>
        <tr>
            <td class="label" width="120px">Store *</td>
            <td class="edit">
                <select name="store_id" style="width: 300px;">
                    <option value="0" <?php if ($store_id == 0) echo "selected" ?>>-- Select Store --</option>
                    <?php $stores = tep_db_query("select * from " . TABLE_STORES . " where deleted <> 'Y' order by title");
                    while ($store = tep_db_fetch_array($stores)): ?>
                        <option value="<?= $store['store_id'] ?>" <?php if ($store_id == $store['store_id']) echo "selected" ?>><?= $store['title'].' - '.$store['location_name'] ?></option>
                    <?php endwhile; ?>
                </select>
            </td>
        </tr>
        <tr>
            <td class="label" width="120px">Regular Price</td>
            <td class="edit">
                <input type="text" name="regular_price" id="regular_price" value="<?= $regular_price ?>" style="width: 400px;" class="" />
                <?php $message_cls->show_error('regular_price') ?>
            </td>
        </tr>
        <tr>
            <td class="label" width="120px">Sale Price</td>
            <td class="edit">
                <input type="text" name="sale_price" id="sale_price" value="<?= $sale_price ?>" style="width: 400px;" class="" />
                <?php $message_cls->show_error('sale_price') ?>
            </td>
        </tr>
        <tr>
            <td class="label" width="120px">Savings</td>
            <td class="edit">
                <input type="text" name="savings" id="regular_price" value="<?= $savings ?>" style="width: 400px;" class="" />
                <?php $message_cls->show_error('savings') ?>
            </td>
        </tr>
        <tr>
                <td class="label">Start Date</td>
                <td class="edit">
                        <input type="text" name="start_date" id="start_date" value="<?= $start_date?>" style="width: 100px;" class="input_birthday" />
                        <?php $message_cls->show_error('start_date')?>
                </td>
        </tr>		
        <tr>
                <td class="label">End Date</td>
                <td class="edit">
                        <input type="text" name="end_date" id="end_date" value="<?= $end_date?>" style="width: 100px;" class="input_birthday" />
                        <?php $message_cls->show_error('end_date')?>
                </td>
        </tr>		
        <tr>
            <td class="label">Featured </td>
            <td class="edit">
                <input type="checkbox" name="featured" value="Y" <?php if($featured=='Y') echo 'checked'; ?>>                    
            </td>
        </tr>
        <tr>
            <td class="label">Card Required </td>
            <td class="edit">
                <input type="checkbox" name="card_required" value="Y" <?php if($card_required=='Y') echo 'checked'; ?>>                    
            </td>
        </tr>
        <?php if($id!=0) { ?>
        <tr>
            <td class="label">Created </td>
            <td class="edit">
                <?= $created ?>				
            </td>
        </tr>
        <?php } ?>
        <tr height="35px">
            <td class="label"></td>
            <td class="edit">
                <input type="submit" value="  Save " name="action" style="width:80px;"/>

                <input type="button" value="  Cancel " style="width:80px;" onclick="location.href = 'deals.php'" />
            </td>
        </tr>	
    </table>
</form>

<?php require(DIR_WS_INCLUDES . 'body_footer.php'); ?>

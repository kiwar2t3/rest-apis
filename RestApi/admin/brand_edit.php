<?php
require('../includes/admin_application_top.php');

$titlex = "Create Brand";

$id = 0;
if (isset($_GET['id'])) {
	$titlex = "Brand Edit";
	$id = tep_get_value_get("id");
}

require(DIR_WS_INCLUDES . 'body_header.php');

$title			= "";
$manufacturer           = "";
$description	= "";
$image_original	= "";
$image_web		= "";
$image_web_thumb= "";
$created		= "";
$actived		= "Y";

if (isset($_POST["title"]))
{
	$title		= tep_get_value_post('title', 'Title', 'require;length[5,200];');
        $manufacturer	= tep_get_value_post('manufacturer', 'Manufacturer', '');
	$description	= tep_get_value_post('description', 'Description', '');
	
	$image_mode     = tep_get_value_post('image_mode', 'Image Mode', 'require;');
        $image_url      = tep_get_value_post('image_url', 'Image URL', '');        
        if($image_mode=='upload'){
            $image_original = upload_file($title, 'image', FALSE);
        }else if($image_mode=='url'){
            $image_original = download_file($title, $image_url, FALSE);
        }        
	if ($image_original != '' && $upload_img_path != '') {
		$image_thumb	= formated_image($image_original, $upload_img_path, 100, 100);
	}

	if ($message_cls->is_empty_error()) {
		$brand = array(
			'title'			=> $title,
                        'manufacturer'		=> $manufacturer,
			"description"           => $description,
		);
		if ($id == 0) {
			$brand['created']			= tep_now_datetime();
			$brand['actived']			= $actived;
			$brand['image_original']		= getUploadFileRelativePath($image_original);
			$brand['image_thumb']                   = getUploadFileRelativePath($image_thumb);
			
			$result = tep_db_perform(TABLE_BRANDS, $brand, 'insert');
			if ($result > 0) {
				$id = tep_db_insert_id();
				//tep_success_redirect("Success new registed brand!", "brand_edit.php?id=".$id);
			}
		} else {
			if ($image_original != '') {
				$brand['image_original']		= getUploadFileRelativePath($image_original);
				$brand['image_thumb']                   = getUploadFileRelativePath($image_thumb);
			}			
			$result = tep_db_perform(TABLE_BRANDS, $brand, 'update', "brand_id='".$id."'");
		}
		
		if ($result > 0) {
			//tep_success_redirect("Success saved brand information!", "brand_edit.php?id=".$id);
			tep_success_redirect("Success saved brand information!", "brands.php");
		} else {
			$error_db = "Faild register brand.";
		}
	}
} elseif($id > 0) {
	$brand_info = teb_one_query(TABLE_BRANDS, array("brand_id"=>$id));
	
	$title			= $brand_info['title'];
        $manufacturer		= $brand_info['manufacturer'];
	$description	= $brand_info['description'];
	$image_original	= getUploadFileAbsolutePath($brand_info['image_original']);
	$image_thumb    = getUploadFileAbsolutePath($brand_info['image_thumb']);
	$created		= $brand_info['created'];	
}
?>

<form name="ad_form" encType="multipart/form-data" method="post" autocomplete="off" class="edit_form" id="ADForm">
	<?php if (isset($error_db)): ?>
	<p class="error"><?= $error_db?></p>
	<?php endif; ?>
	
    <table class="contents_edit" id="ad_basic">
    	<tr>
                <td class="label" width="120px">Title *</td>
			<td class="edit">
				<input type="text" name="title" id="title" value="<?= $title?>" style="width: 400px;" class="validate[required,length[5-200]]" />
				<?php $message_cls->show_error('title')?>
			</td>
		</tr>
                <td class="label" width="120px">Manufacturer</td>
			<td class="edit">
				<input type="text" name="manufacturer" id="manufacturer" value="<?= $manufacturer?>" style="width: 400px;" class="validate[]" />
				<?php $message_cls->show_error('title')?>
			</td>
		</tr>
		<tr>
			<td class="label">Image</td>
			<td class="edit">
				<?php if ($image_thumb != ''):?>
					<a href="<?= $image_thumb?>" target="_image"><img src="<?= $image_thumb?>" /></a><br>
				<?php endif;?>
                                <input type="radio" name="image_mode" value="upload" checked onclick="document.getElementById('image').style.display='block'; document.getElementById('image_url').style.display='none';" />Upload
                                <input type="radio" name="image_mode" value="url"  onclick="document.getElementById('image').style.display='none'; document.getElementById('image_url').style.display='block';" />URL<br>
				<input type="file" name="image" id="image" style="width:400px;">
                                <input type="text" name="image_url" id="image_url" style="width:400px; " hidden>
				<?php $message_cls->show_error('image')?>
			</td>
		</tr>
		<tr>
			<td class="label">Description</td>
			<td class="edit">
				<textarea name="description" id="description" style="width: 400px; height: 150px;" class="validate[required,length[5-2000]]"><?= $description?></textarea>
				<?php $message_cls->show_error('description')?>
			</td>
		</tr>
		<tr height="35px">
			<td class="label"></td>
			<td class="edit">
				<input type="submit" value="  Save " name="action" style="width:80px;"/>
			
				<a href="brands.php" class="button">&nbsp;&nbsp;&nbsp;Cancel&nbsp;&nbsp;&nbsp;</a>
			</td>
		</tr>	
	</table>
</form>

<?php require(DIR_WS_INCLUDES . 'body_footer.php'); ?>
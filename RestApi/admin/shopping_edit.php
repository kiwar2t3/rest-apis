<?php
require('../includes/admin_application_top.php');

$titlex = "Create Shopping Item";

$id = 0;
if (isset($_GET['id'])) {
    $titlex = "Shopping Item Edit";
    $id = tep_get_value_get("id");
}

require(DIR_WS_INCLUDES . 'body_header.php');

$deal_id = 0;
$user_id = 0;
$status = "InUse";

if (isset($_POST["deal_id"])) {
    $deal_id = tep_get_value_post('deal_id', 'Deal', 'require');
    $user_id    = tep_get_value_post('user_id', 'User', 'require');
    $status     = "InUse";//tep_get_value_post('status');

    if ($message_cls->is_empty_error()) {

        $shopping = array(
            'deal_id' => $deal_id,
            'user_id'   => $user_id,
            'created'   => tep_now_datetime(),
            'status'    => $status,
        );

        $shopping_db = teb_one_query(TABLE_SHOPPINGS, array("deal_id"=>$deal_id, "user_id"=>$user_id));        
        if ($shopping_db) {
            $result = tep_db_perform(TABLE_SHOPPINGS, $shopping, 'update', "user_id='" . $user_id . "' and deal_id='".$deal_id."'");            
        } else {
            $result = tep_db_perform(TABLE_SHOPPINGS, $shopping, 'insert');
        }

        if ($result > 0) {
            //tep_success_redirect("Success saved deal information!", "deal_edit.php?id=".$id);
            tep_success_redirect("Success saved deal review!", "shoppings.php");
        } else {
            $error_db = "Faild register review.";
        }
    }
} elseif ($id > 0) {
    $shopping_info = teb_one_query(TABLE_SHOPPINGS, array("id" => $id));

    $deal_id    = $shopping_info['deal_id'];
    $user_id    = $shopping_info['user_id'];    
    $created    = $shopping_info['created'];
    $status     = $shopping_info['status'];
}
?>

<form name="ad_form" encType="multipart/form-data" method="post" autocomplete="off" class="edit_form" id="ADForm">
    <?php if (isset($error_db)): ?>
        <p class="error"><?= $error_db ?></p>
    <?php endif; ?>

    <table class="contents_edit" id="ad_basic">
        
        <tr>
            <td class="label" width="120px">User *</td>
            <td class="edit">
                <select name="user_id" style="width: 300px;">
                    <option value="" <?php if ($user_id == 0) echo "selected" ?>>-- Select User --</option>
                    <?php 
                        $users = tep_db_query("select * from " . TABLE_USERS . " order by user_fullname");
                        while ($user = tep_db_fetch_array($users)): 
                    ?>
                            <option value="<?= $user['user_id'] ?>" <?php if ($user_id == $user['user_id']) echo "selected" ?>>
                                <?= $user['user_fullname'] ?>
                            </option>
                    <?php 
                        endwhile; 
                    ?>
                </select>
                <?php $message_cls->show_error('user_id')?>
            </td>
        </tr>
                
        <tr>
            <td class="label" width="120px">Deal *</td>
            <td class="edit">
                <select name="deal_id" style="width: 300px;">
                    <option value="" <?php if ($deal_id == 0) echo "selected" ?>>-- Select Deal --</option>
                    <?php $deals = tep_db_query("select d.*,p.*,p.title as product_title,s.*,s.title as store_title from ".TABLE_DEALS." as d join (".TABLE_PRODUCTS." as p, ".TABLE_STORES." as s) on (d.product_id = p.id and d.store_id = s.store_id)"); 
                            while($deal = tep_db_fetch_array($deals)):
                    ?>
                                    <option value="<?= $deal['deal_id']?>" <?php if ($deal_id == $deal['deal_id']) echo "selected"?>><?= $deal['product_title'].' - '.$deal['store_title'].','.$deal['location_name'].' ('.$deal['start_date'].' ~ '.$deal['end_date'].')'?></option>
                    <?php endwhile;?>			
                </select>
                <?php $message_cls->show_error('deal_id')?>
            </td>
        </tr>
        
        <!--<tr>
            <td class="label" width="120px">Status *</td>
            <td class="edit">
                <select name="status" style="width: 300px;">
                    <option value="InUse" selected>InUse</option>
                    <option value="Completed">Completed</option>
                    <option value="Deleted">Deleted</option>
                </select>
            </td>
        </tr>-->
        
        <?php if($id!=0) { ?>
        <tr>
            <td class="label">Created </td>
            <td class="edit">
                <?= $created ?>				
            </td>
        </tr>
        <?php } ?>
        <tr height="35px">
            <td class="label"></td>
            <td class="edit">
                <input type="submit" value="  Save " name="action" style="width:80px;"/>

                <input type="button" value="  Cancel " style="width:80px;" onclick="location.href = 'shoppings.php'" />
            </td>
        </tr>	
    </table>
</form>

<?php require(DIR_WS_INCLUDES . 'body_footer.php'); ?>
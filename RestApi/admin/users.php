<?php
require('../includes/admin_application_top.php');

$titlex = "Users";

$user_type = "user";

require(DIR_WS_INCLUDES . 'body_header.php');

$action = tep_get_value_require('action');
if ($action == 'delete_users') {
	$user_ids = tep_get_value_post("user_ids");
	
	for ($i = 0; $i < count($user_ids); $i ++) {
		teb_delete_query(TABLE_USERS, array('user_id'=>$user_ids[$i]));		
	}
	
	$s_process = "Deleted user counts: ".count($user_ids);
}


$s_key = tep_get_value_get("s_key");
?>

<form class="search_form" name="search_form" method="get">
	<div>		
		<input type="text" name="s_key" value="<?= $s_key?>" placeholder="Enter keyword"/>
		<input type="submit" value="Search"/>&nbsp;&nbsp;&nbsp;
                <p>
                    <input type="button" value="Add New User" onclick="location.href='user_edit.php'"/>&nbsp;
                    <input type="button" value="Delete Users" onclick="delete_users()"/>
                </p>
	</div>
</form>

<?php if ($errors['db'] != ""): ?>
<p class="error"><?= $errors['db']?></p>
<?php endif; ?>

<?php if (isset($s_process)):?>
<div class="message"><?= $s_process?></div>
<?php endif;?>

<script type="text/javascript">

function delete_users() {
	if ($('.all_check:checked').length == 0) {
		alert("Please select user.");
		return;
	}

	if (confirm("Are you sure delete selected users?")) {
		document.users_form.submit();
	}
}

</script>


<form name="users_form" method="post">
<input type="hidden" name="action" value="delete_users" />
<table class="contents_list" cellpadding="0" cellspacing="1">
<?php 
	$sort_column = "user_created";
	$sort_order = "DESC";
	if (isset($_REQUEST['sort_column']))	$sort_column = tep_db_prepare_input($_REQUEST['sort_column']);
	if (isset($_REQUEST['sort_order']))		$sort_order = tep_db_prepare_input($_REQUEST['sort_order']);

	$table_headers = array();
	$table_headers[] = array('id'=>'', 'title'=>'<input type="checkbox" onclick="all_checkbox($(this))" title="all select" />', 'width'=>'20');
	$table_headers[] = array('id'=>'user_id', 'title'=>'User ID #', 'width'=>'100');
	$table_headers[] = array('id'=>'', 'title'=>'Avatar', 'width'=>'50');
        $table_headers[] = array('id'=>'user_email', 'title'=>'Email', 'width'=>'180');	
	$table_headers[] = array('id'=>'', 'title'=>'Name', 'width'=>'120');
	$table_headers[] = array('id'=>'user_address_zip', 'title'=>'Zipcode', 'width'=>'80');	
        $table_headers[] = array('id'=>'user_sex', 'title'=>'Sex', 'width'=>'80');	
        $table_headers[] = array('id'=>'user_birthday', 'title'=>'Birthday', 'width'=>'100');	
	$table_headers[] = array('id'=>'user_created', 'title'=>'Registration Date', 'width'=>'100');
	$table_headers[] = array('id'=>'user_last_logined', 'title'=>'Last Logged In', 'width'=>'100');
	$table_headers[] = array('id'=>'', 'title'=>'Action', 'width'=>'100');
	
	$column_count = count($table_headers);
	
	include DIR_WS_BOX.'table_header.php';
?>

<?
	$sql = "select * from " . TABLE_USERS . " where user_type='user'";
	if ($s_key != '') {
		$sql.= " and (user_name like '%".$s_key."%'";
		$sql.= " or user_phone like '%".$s_key."%'";
		$sql.= " or user_email like '%".$s_key."%'";
                $sql.= " or user_firstname like '%".$s_key."%'";
                $sql.= " or user_lastname like '%".$s_key."%'";
                $sql.= " or user_fullname like '%".$s_key."%')";
	}	
	$sql .= " order by ".$sort_column." ".$sort_order;
	
	$list_split = new splitPageResults($sql);
	$users = tep_db_query($list_split->sql_query);
 
	
	
	$row = 0;
	while ($user = tep_db_fetch_array($users)) {
		$row ++;
?>	
<tbody>   
	<tr class='dataTableRow'>
		<td align="center"><input type="checkbox" name="user_ids[]" value="<?= $user['user_id']?>" class="all_check"/></td>
		<td align="center"><?=$user['user_id']?></td>
		<td align="center">
			<a class="link" href="user_edit.php?user_id=<?= $user['user_id']?>" title="View Detail">
			<?php if ($user['user_avatar'] != ''):?>
				<img src="<?= getUploadFileAbsolutePath($user['user_avatar'])?>" width="50px"/>
			<?php endif;?>
			</a>
		</td>
                <td align="center"><a class="link" href="mailto:<?=$user['user_email']?>" title="Please send emil"><?=$user['user_email']?></a></td>				
		<td align="center">
			<a class="link" href="user_edit.php?user_id=<?= $user['user_id']?>" title="View Detail"><b><?=$user['user_fullname'];?></b></a>
		</td>				
		<td align="center"><?=$user['user_address_zip']?></td>
                <td align="center"><?=$user['user_sex']?></td>
                <td align="center"><?=$user['user_birthday']?></td>
		<td align="center"><?=$user['user_created']?></td>
		<td align="center"><?=$user['user_last_logined']?></td>
		<td align="center">
                    <a class="button" href="user_edit.php?user_id=<?= $user['user_id'];?>" title="Edit">Edit</a>        	
                </td>
	</tr>
<?php
	}
?>
</tbody>
<?php 
	$data_message = TEXT_DISPLAY_NUMBER_OF_USERS;
	$empty_message = "Empty users";
	include DIR_WS_BOX.'table_footer.php';
?>
</table>
</form>

<?php require(DIR_WS_INCLUDES . 'body_footer.php'); ?>
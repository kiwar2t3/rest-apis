<?php
require('../includes/admin_application_top.php');

$titlex = "Categories";

require(DIR_WS_INCLUDES . 'body_header.php');

$action = tep_db_prepare_input($_REQUEST['action']);
if ($action == 'delete') {
	$id = tep_get_value_get('id');
	
	//teb_delete_query(TABLE_CATEGORIES, array("category_id"=>$id));
	tep_db_query("update ".TABLE_CATEGORIES." set deleted='Y' where `category_id`='".$id."'");
	
	tep_success_redirect("Successfully Deleted Category.", "categories.php");
} elseif ($action == 'actived') {
	$id = tep_get_value_get('id');
	$actived = tep_get_value_get('actived');
	
	tep_db_query("update ".TABLE_CATEGORIES." set actived='".$actived."' where `category_id`='".$id."'");
} elseif ($action == 'all_actived') {
	$category_ids = tep_get_value_post("category_ids");
	
	for ($i = 0; $i < count($category_ids); $i ++) {
		tep_db_query("update ".TABLE_CATEGORIES." set actived='Y' where `category_id`='".$category_ids[$i]."'");
	}
} elseif ($action == 'all_disabled') {
	$category_ids = tep_get_value_post("category_ids");
	
	for ($i = 0; $i < count($category_ids); $i ++) {
		tep_db_query("update ".TABLE_CATEGORIES." set actived='N' where `category_id`='".$category_ids[$i]."'");
	}
} elseif ($action == 'all_delete') {
	$category_ids = tep_get_value_post("category_ids");
	
	for ($i = 0; $i < count($category_ids); $i ++) {
		//teb_delete_query(TABLE_CATEGORIES, array("category_id"=>$category_ids[$i]));
		tep_db_query("update ".TABLE_CATEGORIES." set deleted='Y' where `category_id`='".$category_ids[$i]."'");
	}
	
	tep_success_redirect("Successfully Deleted Categories.", "categories.php");
}

$s_key = tep_db_prepare_input($_REQUEST['s_key']);
$s_active = 'Y';//tep_db_prepare_input($_REQUEST['s_active']);
?>

<?php if ($errors['db'] != ""): ?>
<p class="error"><?= $errors['db']?></p>
<?php endif; ?>

<form class="search_form" name="search_form" method="post">
	<div>
		<!--Active: <select name="s_active" onchange="this.form.submit()">
			<option value="">-- All --</option>
			<option value="Y" <?php if ($s_active == 'Y') echo "selected"?>>Active</option>
			<option value="N" <?php if ($s_active == 'N') echo "selected"?>>InActive</option>
		</select>&nbsp;&nbsp;&nbsp;-->
		Search: <input type="text" name="s_key" value="<?= $s_key?>" />
		<input type="submit" value="Search" />&nbsp;&nbsp;&nbsp;
                <p>
                    <input type="button" value="Add Category" onclick="location.href='category_edit.php'"/>
                </p>
	</div>
</form>

<script type="text/javascript">
<!--
function delete_new(id, title) {
	if (confirm("Are you sure to delete "+title+"?")) {
		location.href = "categories.php?action=delete&id=" + id;
	}
}

function all_action() {
	if (confirm("Are you sure to process?")) {
		document.dataListForm.submit();
	}
}
//-->
</script>

<form name="dataListForm" method="post" action="categories.php" style="margin-top: 15px;">
	<input type="hidden" name="s_active" value="<?= $s_active?>">
	<input type="hidden" name="s_key" value="<?= $s_key?>">
	
	With selected: <select name="action" onchange="all_action()">
		<option value="">---</option>
		<!--<option value="all_actived">Activate</option>
		<option value="all_disabled">Deactivate</option>-->
		<option value="all_delete">Delete</option>
	</select>
	
<table class="contents_list" cellpadding="0" cellspacing="1">
<?php 
	$sort_column = "title";
	$sort_order = "ASC";
	if (isset($_REQUEST['sort_column']))	$sort_column = tep_db_prepare_input($_REQUEST['sort_column']);
	if (isset($_REQUEST['sort_order']))		$sort_order = tep_db_prepare_input($_REQUEST['sort_order']);

	$table_headers = array();
	$table_headers[] = array('id'=>'', 'title'=>'<input type="checkbox" onchange="all_checkbox($(this))" />', 'width'=>'50');
	$table_headers[] = array('id'=>'category_id', 'title'=>'ID', 'width'=>'100');
	$table_headers[] = array('id'=>'', 'title'=>'Photo', 'width'=>'60');
	$table_headers[] = array('id'=>'title', 'title'=>'Title', 'width'=>'');
        $table_headers[] = array('id'=>'parent', 'title'=>'Parent Category', 'width'=>'200');
	$table_headers[] = array('id'=>'created', 'title'=>'Created at', 'width'=>'170');
	//$table_headers[] = array('id'=>'actived', 'title'=>'Active', 'width'=>'100');
	$table_headers[] = array('id'=>'', 'title'=>'Action', 'width'=>'200');
	
	$column_count = count($table_headers);
	
	include DIR_WS_BOX.'table_header.php';
?>

<?
	$sql = "select * from " . TABLE_CATEGORIES. " where deleted<>'Y'";
	if ($s_key != '') {
		$sql.= " and LOWER(title) like '%".strtolower($s_key)."%'";
	}
	if ($s_active != '') {
		$sql.= " and actived = '".$s_active."'";
	}
	$sql .= " order by ".$sort_column." ".$sort_order;
	
	$list_split = new splitPageResults($sql);
	$categories = tep_db_query($list_split->sql_query);
 
	$row = 0;
	while ($category = tep_db_fetch_array($categories)) {
		$row ++;
		
		$ext_params = "&id=".$category['category_id']."&s_active=".$s_active."&s_key=".$s_key."&sort_column=".$sort_column."&sourt_order=".$sort_order."&page=".$page;
?>	
<tbody>   
	<tr class='dataTableRow'>
		<td align="center">
			<input type="checkbox" name="category_ids[]" value="<?= $category['category_id']?>" class="all_check" />
		</td>
		<td align="center">
			<a class="link" href="category_edit.php?id=<?= $category['category_id']?>" title="View Detail"><?=$category['category_id']?></a>
		</td>
		<td align="center">
			<img src="<?=getUploadFileAbsolutePath($category['image_thumb'])?>" width="50"/>
		</td>
		<td align="left">
			<a class="link" href="category_edit.php?id=<?= $category['category_id']?>" title="View Detail"><?=$category['title']?></a>
		</td>		
                <td align="center">
			<?=$category['parent']?>
		</td>		
		<td align="center"><?=$category['created']?></td>
		<!--<td align="center"><?=$category['actived']?></td>-->
		<td align="center">
                    <a class="button" href="category_edit.php?id=<?= $category['category_id']?>" title="Edit">Edit</a>
                    <a class="button" href="javascript:delete_new(<?= $category['category_id']?>, '<?=$category['title']?>')" title="Edit">Delete</a>
                    <!--<?php if ($category['actived'] == 'Y') : ?>
                            <a class="button" href="categories.php?action=actived&actived=N<?= $ext_params?>" title="Edit">DeActivate</a>
                    <?php else : ?>
                            <a class="button" href="categories.php?action=actived&actived=Y<?= $ext_params?>" title="Edit">Activate</a>
                    <?php endif;?>-->
                </td>
	</tr>
<?php
	}
?>
</tbody>
<?php 
	$data_message = TEXT_DISPLAY_NUMBER_OF_CATEGORIES;
	$empty_message = "No Categories";
	include DIR_WS_BOX.'table_footer.php';
?>
</table>

</form>

<?php require(DIR_WS_INCLUDES . 'body_footer.php'); ?>
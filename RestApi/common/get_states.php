<?php
include '../includes/application_top.php';

$country_code = tep_get_value_require("country_code");
$state_filed = tep_get_value_require("filed_name");
$default_value = urldecode(tep_get_value_require("default_value"));
$city_code = urldecode(tep_get_value_require("city_code"));

$state_count = teb_one_query(TABLE_STATES, array("country_code"=>$country_code), "count(*) as data_count");
if ($state_count->data_count > 0) {
	$states = teb_multi_query(TABLE_STATES, array("country_code"=>$country_code), "*", "cd");
	
	echo "\n".'<select name="'.$state_filed.'" id="'.$state_filed.'" style="width: 200px;"'.' onchange="get_cities(\''.$country_code.'\', this.value, \'user_address_city\', \''.$city_code.'\')">'."\n";
	echo "\t".'<option value=""> -- select state -- </option>'."\n";
	foreach ($states as $state) {
		echo "\t".'<option value="'.$state->cd.'" '.($default_value == $state->cd ? 'selected' : '').'>'.$state->name."</option>\n";
	}
	echo '';
} else {
	echo '<input type="text" value="'.$default_value.'" name="'.$state_filed.'" id="'.$state_filed.'" style="width: 200px;"  />';
}
<?php include 'header.php'; ?>

		<div class="container content-md">
			<div class="margin-bottom-60 head">
				<h1>Login or Register an Account</h1>
				<p>Lorem ipsum dolor sit amet, contetur adipisicing elit, sed do eiusmod tempor incididunt labore. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.</p>
			</div>

			<div class="row space-xlg-hor equal-height-columns">
				<!--login Block-->
				<div class ='col-md-3'></div>
				<div class="form-block login-block col-md-6 col-sm-12 rounded-left equal-height-column">
					<div class="form-block-header">
						<h2 class="margin-bottom-15">Log In</h2>
					</div>

					<div class="input-group margin-bottom-20">
						<span class="input-group-addon rounded-left"><i class="icon-user color-white"></i></span>
						<input type="text" class="form-control rounded-right" placeholder="Username">
					</div>

					<div class="input-group margin-bottom-20">
						<span class="input-group-addon rounded-left"><i class="icon-lock color-white"></i></span>
						<input type="password" class="form-control rounded-right" placeholder="Password">
					</div>

					<div class="row margin-bottom-70">
						<div class="col-md-12">
							<button type="submit" class="btn-u btn-block rounded">Lign In</button>
						</div>
					</div>
					<div class="social-login">
						<div class="or rounded-x">Or</div>
						<ul class="list-unstyled">
							<li>
								<button class="btn rounded btn-block btn-lg btn-facebook-inversed margin-bottom-20">
									<i class="fa fa-facebook"></i> Sign in with Facebook
								</button>
							</li>
							<li>
								<button class="btn rounded btn-block btn-lg btn-twitter-inversed">
									<i class="fa fa-twitter"></i> Sign in with Twitter
								</button>
							</li>
						</ul>
					</div>
				</div>
				<!--End login Block-->

				<!--Reg Block-->
				<!-- <div class="form-block reg-block col-md-6 col-sm-12 rounded-right equal-height-column">
					<div class="form-block-header">
						<h2 class="margin-bottom-10">Sign Up</h2>
						<p class="margin-bottom-20">Lorem ipsum dolor sit amet, conur sicing elit, seddoet dolore magna. aliqua.</p>
					</div>

					<div class="input-group margin-bottom-20">
						<span class="input-group-addon rounded-left"><i class="icon-pencil"></i></span>
						<input type="text" class="form-control rounded-right" placeholder="Your name">
					</div>

					<div class="input-group margin-bottom-20">
						<span class="input-group-addon rounded-left"><i class="icon-user"></i></span>
						<input type="text" class="form-control rounded-right" placeholder="Username">
					</div>

					<div class="input-group margin-bottom-20">
						<span class="input-group-addon rounded-left"><i class="icon-envelope"></i></span>
						<input type="email" class="form-control rounded-right" placeholder="Your email">
					</div>

					<div class="input-group margin-bottom-20">
						<span class="input-group-addon rounded-left"><i class="icon-lock"></i></span>
						<input type="password" class="form-control rounded-right" placeholder="Password">
					</div>

					<div class="checkbox margin-bottom-15">
						<label>
							<input type="checkbox">
							<p>I agree to terms &amp; conditions</p>
						</label>

						<label>
							<input type="checkbox">
							<p>Subscribe to our newsletter</p>
						</label>
					</div>

					<div class="row">
						<div class="col-md-12">
							<button type="submit" class="btn-u btn-block rounded">Continue</button>
						</div>
					</div>
				</div> -->
				<!--End Reg Block-->
			</div>
		</div><!--/container-->
	</div>
	<!--=== End Content Part ===-->

  <?php include 'footer.php'; ?>

	<!--=== Style Switcher ===-->
	<i class="style-switcher-btn fa fa-cogs hidden-xs"></i>
	<div class="style-switcher animated fadeInRight">
		<div class="style-swticher-header">
			<div class="style-switcher-heading">Style Switcher</div>
			<div class="theme-close"><i class="icon-close"></i></div>
		</div>

		<div class="style-swticher-body">
			<!-- Theme Colors -->
			<div class="style-switcher-heading">Theme Colors</div>
			<ul class="list-unstyled">
				<li class="theme-default theme-active" data-style="default" data-header="light"></li>
				<li class="theme-blue" data-style="blue" data-header="light"></li>
				<li class="theme-orange" data-style="orange" data-header="light"></li>
				<li class="theme-red" data-style="red" data-header="light"></li>
				<li class="theme-light" data-style="light" data-header="light"></li>
				<li class="theme-purple last" data-style="purple" data-header="light"></li>
				<li class="theme-aqua" data-style="aqua" data-header="light"></li>
				<li class="theme-brown" data-style="brown" data-header="light"></li>
				<li class="theme-dark-blue" data-style="dark-blue" data-header="light"></li>
				<li class="theme-light-green" data-style="light-green" data-header="light"></li>
				<li class="theme-dark-red" data-style="dark-red" data-header="light"></li>
				<li class="theme-teal last" data-style="teal" data-header="light"></li>
			</ul>

			<!-- Theme Skins -->
			<div class="style-switcher-heading">Theme Skins</div>
			<div class="row no-col-space margin-bottom-20 skins-section">
				<div class="col-xs-6">
					<button data-skins="default" class="btn-u btn-u-xs btn-u-dark btn-block active-switcher-btn handle-skins-btn">Light</button>
				</div>
				<div class="col-xs-6">
					<button data-skins="dark" class="btn-u btn-u-xs btn-u-dark btn-block skins-btn">Dark</button>
				</div>
			</div>

			<hr>

			<!-- Layout Styles -->
			<div class="style-switcher-heading">Layout Styles</div>
			<div class="row no-col-space margin-bottom-20">
				<div class="col-xs-6">
					<a href="javascript:void(0);" class="btn-u btn-u-xs btn-u-dark btn-block active-switcher-btn wide-layout-btn">Wide</a>
				</div>
				<div class="col-xs-6">
					<a href="javascript:void(0);" class="btn-u btn-u-xs btn-u-dark btn-block boxed-layout-btn">Boxed</a>
				</div>
			</div>

			<hr>

			<!-- Theme Type -->
			<div class="style-switcher-heading">Theme Types and Versions</div>
			<div class="row no-col-space margin-bottom-10">
				<div class="col-xs-6">
					<a href="E-Commerce/index.html" class="btn-u btn-u-xs btn-u-dark btn-block">Shop UI <small class="dp-block">Template</small></a>
				</div>
				<div class="col-xs-6">
					<a href="One-Pages/Classic/index.html" class="btn-u btn-u-xs btn-u-dark btn-block">One Page <small class="dp-block">Template</small></a>
				</div>
			</div>

			<div class="row no-col-space">
				<div class="col-xs-6">
					<a href="Blog-Magazine/index.html" class="btn-u btn-u-xs btn-u-dark btn-block">Blog <small class="dp-block">Template</small></a>
				</div>
				<div class="col-xs-6">
					<a href="RTL/index.html" class="btn-u btn-u-xs btn-u-dark btn-block">RTL <small class="dp-block">Version</small></a>
				</div>
			</div>
		</div>
	</div><!--/style-switcher-->
	<!--=== End Style Switcher ===-->

	<!-- JS Global Compulsory -->
	<script src="assets/plugins/jquery/jquery.min.js"></script>
	<script src="assets/plugins/jquery/jquery-migrate.min.js"></script>
	<script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>

	<!-- JS Implementing Plugins -->
	<script src="assets/plugins/back-to-top.js"></script>
	<script src="assets/plugins/backstretch/jquery.backstretch.min.js"></script>
	<script type="text/javascript" src="assets/js/plugins/style-switcher.js"></script>

	<!-- JS Customization -->
	<script src="assets/js/custom.js"></script>

	<!-- JS Page Level -->
	<script src="assets/js/app.js"></script>
	<script>
		jQuery(document).ready(function() {
			App.init();
			StyleSwitcher.initStyleSwitcher();
		});
	</script>
	<script>
		$(".forms-wrapper").backstretch([
			"assets/img/bg/6.jpg",
			"assets/img/bg/5.jpg",
			"assets/img/bg/7.jpg",
			], {
				fade: 1000,
				duration: 7000
			});
	</script>
	<!--[if lt IE 9]>
  <script src="assets/plugins/respond.js"></script>
  <script src="assets/plugins/html5shiv.js"></script>
  <script src="assets/plugins/placeholder-IE-fixes.js"></script>
  <![endif]-->
</body>
</html>

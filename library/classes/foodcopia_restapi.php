<?php
class FoodCopia_RestApi {
	var $action = "";
	var $auth_token = FALSE;
	
	var $status = "";
	var $result = "";

	function __construct($action, $auth_token = FALSE) {
		$this -> action = $action;
		
		$this->auth_token = $auth_token;
	}

	function has_successed() {
		return $this -> status == 'success';
	}

	function has_failed() {
		return $this -> status == 'error';
	}

	function call_api($params = array(), $upload_files = FALSE) {
		global $message_cls;
		$fp = fsockopen(HTTP_REST_API_HOST, 80, $errno, $errstr, 30);

		if (!$fp) {
			$message_cls -> set_error("api_call", $errno . " : " . $errstr);
			return FALSE;
		}

		$params['action'] = $this -> action;
		if ($this->auth_token) {
			$params['auth_token'] = $this -> auth_token;
		}

		fwrite($fp, "POST " . HTTP_REST_API_URL . " HTTP/1.1\r\n");
		fwrite($fp, "Host: " . HTTP_REST_API_HOST . "\r\n");
		$content = "";
		if ($upload_files) {
			srand((double)microtime() * 1000000);
			$boundary = "----WebKitFormBoundary" . substr(md5(rand(0, 32000)), 0, 16);

			fwrite($fp, "Content-Type: multipart/form-data; boundary=" . $boundary . "\r\n");

			// attach params
			foreach ($params AS $index => $value) {
				$content .= "--" . $boundary . "\r\n";
				$content .= "Content-Disposition: form-data; name=\"" . $index . "\"\r\n\r\n";
				$content .= $value . "\r\n";
			}
			// and attach the file
			foreach ($upload_files as $index => $upload_file) {
				$content .= "--" . $boundary . "\r\n";
				//$file_contents = join("", file($upload_file['tmp_name']));
				
				$file_contents = file_get_contents($upload_file['tmp_name']);
				
				$content .= "Content-Disposition: form-data; name=\"" . $index . "\"; filename=\"" . basename($upload_file['name']) . "\"\r\n";
				$content .= "Content-Type: " . $upload_file['type'] . "\r\n";
				$content .= "Content-Length: " . strlen($file_contents) . "\r\n";
				$content .= "Content-Type: application/octet-stream\r\n\r\n";
				$content .= $file_contents . "\r\n";
			}
			$content .= "--" . $boundary . "--";
		} else {
			$content = http_build_query($params);
			fwrite($fp, "Content-Type: application/x-www-form-urlencoded\r\n");
		}

		fwrite($fp, "Content-Length: " . strlen($content) . "\r\n");
		fwrite($fp, "Connection: Close\r\n");
		fwrite($fp, "\r\n");

		fwrite($fp, $content);

		$body = false;
		$response = "";
		while (!feof($fp)) {
			$s = fgets($fp, 1024);
			if ($body){
				$response .= $s;
			}
			
			if ($s == "\r\n"){
				$body = true;
			}
		}
		fclose($fp);
	/* 	
		$pattern = '
				/
				\{              # { character
				    (?:         # non-capturing group
				        [^{}]   # anything that is not a { or }
				        |       # OR
				        (?R)    # recurses the entire pattern
				    )*          # previous group zero or more times
				\}              # } character
				/x
				';
		
		preg_match_all($pattern, $response, $matches); */
		
		$data = json_decode(substr($response, 0, strrpos($response,'}') + 1));
		//$data = 
		//$data=preg_replace('/\s+/', '',$response);
		
		if ($data && isset($data->status)) {
			$this -> status = $data -> status;
			$this -> result = $data -> result;
		} else {
			$this -> status = 'error';
			$this -> result = array("RestAPI have error. Please try again.");
		}
	}

}
